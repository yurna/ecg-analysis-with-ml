import pickle
import itertools
from tqdm import tqdm
import numpy as np
from matplotlib import pyplot as plt
from pathlib import Path
FS = 250

# %%
path=Path('data')

stride = 5
window_length = 10

file_name = '{}-{}_seq_stp.pkl'.format(window_length, stride)

with open(path / file_name, "rb") as f:
    filtered = pickle.load(f)


#%%

w = 128
shift = 50
all_sig = []
for signal in tqdm(filtered):
    all_lead_mat = []
    # use the first 2
    signal = signal.T[:2]
    pairs = list(itertools.combinations(signal, 2))
    for s1,s2 in pairs:
        sig_mat = []
        for i in range(w, len(s1), shift):
            sig_mat.append(np.outer(s1[i - w:i], s2[i - w:i]))
        sig_mat = np.array(sig_mat) / w
        all_lead_mat.append(sig_mat)

    all_sig.append(np.swapaxes(np.array(all_lead_mat, dtype='float32'), 0, 1))

del filtered, all_lead_mat, sig_mat

#%%


# %%
for index, ten_sec in enumerate(all_sig):
    all_sig[index] /= np.max(np.abs(ten_sec))

all_sig = np.array(all_sig)

def trim_axs(axs, N):
    """little helper to massage the axs list to have correct length..."""
    axs = axs.flat
    for ax in axs[N:]:
        ax.remove()
    return axs[:N]

index = np.random.randint(0,all_sig[0].shape[0],10)
fig, axs = plt.subplots(3,4)
axs = trim_axs(axs, 10)
for i, ax in zip(index, axs):
    ax.imshow(all_sig[0][i,0,:,:])

plt.show()



#%%

from pytorch_utils import VAE, Conv_VAE
from pytorch_utils import SimpleImageDataset
from pytorch_utils import get_loaders, train, EarlyStopping
import torch
import torch.backends.cudnn as cudnn

if torch.cuda.is_available():
        device = torch.device("cuda")
        cudnn.enabled = True
        cudnn.benchmark = True

data = SimpleImageDataset(all_sig)
base = 16
net = VAE(data[0].shape[0], 10, device, base=base, debug=False)
if torch.cuda.device_count() > 1:
    net = torch.nn.DataParallel(net)

net.to(device)
train_loader, val_loader, test_loader = get_loaders(data, batch_size=1024, pin_memory=True)

lr = 1e-3
optim = torch.optim.Adam(net.parameters(), lr=lr)

losses = train(net = net,  train_loader = train_loader,
                           val_loader = val_loader, device = device,
                           optim = optim)
#%%
torch.save(net.state_dict(), 'simpleConvNet_base={}.weights'.format(base))
#%%
n_row = 5
index = np.random.randint(0,len(data),n_row)
fig, axs = plt.subplots(n_row,3)
for count, i in enumerate(index):
    axs[count,0].imshow(data[i, 0].numpy())
    with torch.no_grad():
        output = net(data[i].unsqueeze(0).to(device))[0][0,0].cpu().numpy()
    axs[count,1].imshow(output)
    axs[count,2].imshow(np.abs(data[i, 0].numpy() - output))

plt.show()

#%%
#allconv
conv_net = Conv_VAE(data[0].shape[0], 10, device, base=base, debug=False)
if torch.cuda.device_count() > 1:
    conv_net = torch.nn.DataParallel(conv_net)

conv_net.to(device)
train_loader, val_loader, test_loader = get_loaders(data, batch_size=1024, pin_memory=True)

lr = 1e-3
optim = torch.optim.Adam(conv_net.parameters(), lr=lr)

losses = train(net = conv_net,  train_loader = train_loader,
                           val_loader = val_loader, device = device,
                           optim = optim)

#%%
torch.save(net.state_dict(), 'AllConvNet_base={}.weights'.format(base))
#%%
n_row = 5
index = np.random.randint(0,len(data),n_row)
fig, axs = plt.subplots(n_row,3)
for count, i in enumerate(index):
    axs[count,0].imshow(data[i, 0].numpy())
    with torch.no_grad():
        output = net(data[i].unsqueeze(0).to(device))[0][0,0].cpu().numpy()
    axs[count,1].imshow(output)
    axs[count,2].imshow(np.abs(data[i, 0].numpy() - output))

plt.show()
