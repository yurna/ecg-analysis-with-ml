from pytorch_utils import get_loaders, train
from pytorch_utils import RNNDataset_arrhythmia, VRAE, VRAE_conv, TemporalConvNet


import pickle
import torch
import torch.backends.cudnn as cudnn
from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

if torch.cuda.is_available():
    device = torch.device("cuda")
    cudnn.enabled = True
    cudnn.benchmark = True
else:
    device = torch.device("cpu")

weights_folder = Path('weights')
weights = list(weights_folder.glob('*.weights'))
# %%

path = Path('data')

# with open(path / file_name, "rb") as f:
#     _, arrhythmic = pickle.load(f)

file_name = 'seq_stp_unsupervised.pkl'

with open(path / file_name, "rb") as f:
    data = pickle.load(f)
n_leads = 1
formatted = []
for signal, (places, annotations) in zip(data[0], data[1]):
    formatted.append((signal[:,:n_leads], places, annotations))

del data

dataset = RNNDataset_arrhythmia(formatted)
dataset.segment()
dataset.set_training(True)

bs = 256
train_loader, val_loader, _ = get_loaders(dataset, batch_size=bs, pin_memory=True)
###################################################################################
net = VRAE(input_size = n_leads, embedded_size = 20, n_layer=3, device = device, seq_len=175)
net.to(device)

#b = net(torch.stack(dataset[:2]).to(device))

# dim is the dimension along which the tensor is splitted
#net = torch.nn.DataParallel(net, dim=0)

name = 'unsupervised_vRNN'

found = np.array([file.stem == name for file in weights])

if found.any():
    print('##### - Loading weights - #####')
    net.load_state_dict(torch.load(weights[found.argmax()]))
else:
    print('##### - Training net - #####')
    lr = 1e-3
    optims = [torch.optim.Adam(net.parameters(), lr=lr)]  # , torch.optim.SGD(net.parameters(), lr=1e-4)]
    losses = []
    for optim in optims:
        print('Using {} as optimizer'.format(str(optim.__class__).split('.')[3]))
        losses.append(train(net=net, train_loader=train_loader,
                            val_loader=val_loader, device=device,
                            optim=optim, net_type='lstm_vae'))

    plt.figure()
    plt.plot(losses[0][0])
    plt.plot(losses[0][1])
    plt.show()

    torch.save(net.state_dict(), weights_folder / '{}.weights'.format(name))



#########################################################################

net = VRAE_conv(input_size = n_leads, n_layer=3, device = device)
net.to(device)

#b = net(torch.stack(dataset[:2]).to(device))

# dim is the dimension along which the tensor is splitted
#net = torch.nn.DataParallel(net, dim=0)

name = 'unsupervised_vRNN_conv'

found = np.array([file.stem == name for file in weights])

if found.any():
    print('##### - Loading weights - #####')
    net.load_state_dict(torch.load(weights[found.argmax()]))
else:
    print('##### - Training net - #####')
    lr = 1e-3
    optims = [torch.optim.Adam(net.parameters(), lr=lr)]  # , torch.optim.SGD(net.parameters(), lr=1e-4)]
    losses = []
    for optim in optims:
        print('Using {} as optimizer'.format(str(optim.__class__).split('.')[3]))
        losses.append(train(net=net, train_loader=train_loader,
                            val_loader=val_loader, device=device,
                            optim=optim, net_type='lstm_vae'))

    plt.figure()
    plt.plot(losses[0][0])
    plt.plot(losses[0][1])
    plt.show()

    torch.save(net.state_dict(), weights_folder / '{}.weights'.format(name))
#########################################################################
n_inputs = 175
n_channels = [n_inputs] * 4

net = TemporalConvNet(175, n_channels)
net.to(device)

name = 'unsupervised_TemporalConvNet'

found = np.array([file.stem == name for file in weights])

if found.any():
    print('##### - Loading weights - #####')
    net.load_state_dict(torch.load(weights[found.argmax()]))
else:
    print('##### - Training net - #####')
    lr = 1e-3
    optims = [torch.optim.Adam(net.parameters(), lr=lr)]  # , torch.optim.SGD(net.parameters(), lr=1e-4)]
    losses = []
    for optim in optims:
        print('Using {} as optimizer'.format(str(optim.__class__).split('.')[3]))
        losses.append(train(net=net, train_loader=train_loader,
                            val_loader=val_loader, device=device,
                            optim=optim, net_type='tcn'))

    plt.figure()
    plt.plot(losses[0][0])
    plt.plot(losses[0][1])
    plt.show()

    torch.save(net.state_dict(), weights_folder / '{}.weights'.format(name))



#########################################################################
# Testing
# seed is set to one so it divide in the same way the
dataset.set_training(False)
_, _, test_loader = get_loaders(dataset, batch_size=bs, pin_memory=True)

net.eval()
annotations = []
z = []
for batch in test_loader:
    signals = batch[0]
    annotations += batch[2]
    with torch.no_grad():
        reconstructed, mu, logvar = net(signals.to(device))
        reconstructed = reconstructed.cpu().numpy()
        signals = signals.numpy()
        diff = np.mean(np.abs(reconstructed - signals), axis=1)

        z_last = net.reparametrize(mu, logvar).cpu().numpy()
        z.append(np.concatenate((np.mean(diff, axis = 1)[np.newaxis].T,z_last), axis = 1))

z = np.concatenate(z)

from sklearn.decomposition import PCA
import pandas as pd

ann = list(set(annotations))
cluster = np.array([ann.index(i) for i in annotations])


to_plot = PCA(n_components=3).fit_transform(z)
df = pd.DataFrame(np.hstack((to_plot, cluster[np.newaxis].T)), columns = ['x', 'y', 'z', 'cluster'])

# z_pca = PCA(n_components=10).fit_transform(z)
# tsne = TSNE(n_components=3, verbose=1, perplexity=40, n_iter=1000)
# tsne_results = tsne.fit_transform(z_pca)
# df_tsne = pd.DataFrame(np.hstack((tsne_results, cluster[np.newaxis].T)), columns = ['x', 'y', 'z', 'cluster'])
#
# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(df_tsne['x'].values,df_tsne['y'].values,df_tsne['z'].values, marker="s", c=df_tsne["cluster"], s=40, cmap="RdBu")
# plt.show()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(df['x'].values,df['y'].values,df['z'].values, marker="s", c=df["cluster"], s=40, cmap="RdBu")
plt.show()


from sklearn.mixture import GaussianMixture as GMM
n_clusters = 2
gmm = GMM(n_components=n_clusters, max_iter=1000, verbose=1).fit(z)

clustered = gmm.predict(z)
annotations = np.array(annotations)

clustered_annotations = []
for i in range(n_clusters):
    clustered_annotations.append(annotations[clustered == i])

dictionaries = []
for n in range(n_clusters):
    a = {}
    for i in set(cluster):
        a[ann[i]] = list(clustered_annotations[n]).count(ann[i])
    dictionaries.append(a)

fig, axs = plt.subplots(1,len(dictionaries))
for d, ax in zip(dictionaries, axs):
    ax.bar(list(d.keys()), d.values(), color='g')
plt.show()







