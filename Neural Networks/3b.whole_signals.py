from pytorch_utils import Time_Conv_VAE
from pytorch_utils import SimpleDataset, get_loaders, train
import pickle
import torch
import torch.backends.cudnn as cudnn
from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np

if torch.cuda.is_available():
    device = torch.device("cuda")
    cudnn.enabled = True
    cudnn.benchmark = True
else:
    device = torch.device("cpu")

weights = list(Path('weights').glob('*.weights'))
# %%

path=Path('data')
stride = 4
window_length = 10
file_name = '{}-{}_seq_stp.pkl'.format(window_length, stride)

with open(path / file_name, "rb") as f:
    data, arrhythmic = pickle.load(f)

##########################################################################################
# vae

# loader creations
dataset = SimpleDataset(data)
train_loader, val_loader, test_loader = get_loaders(dataset, batch_size=256, pin_memory=True, test_split_percentage=0)

base = 8
net = Time_Conv_VAE(1, 20, device, base=base, debug=True)
if torch.cuda.device_count() > 1:
    net = torch.nn.DataParallel(net)

net.to(device)

a = net(dataset[0].unsqueeze(0).to(device))


lr = 1e-3
optim = torch.optim.Adam(net.parameters(), lr=lr)

losses = train(net = net,  train_loader = train_loader,
                           val_loader = val_loader, device = device,
                           optim = optim)


n_row = 5
index = np.random.randint(0,len(dataset),n_row)
fig, axs = plt.subplots(n_row,3)
for count, i in enumerate(index):
    axs[count,0].plot(dataset[i, 0, : ,0].numpy())
    with torch.no_grad():
        output = net(dataset[i].unsqueeze(0).to(device))[0][0,0].cpu().numpy()
    axs[count,1].plot(output[:,0])
    axs[count,2].plot(np.abs(dataset[i, 0, :, 0].numpy() - output[:,0]))

plt.show()
