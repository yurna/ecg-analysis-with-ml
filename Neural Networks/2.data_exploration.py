import pickle
from pathlib import Path
from matplotlib import pyplot as plt
from general_utils import trim_axs

path=Path('data')

stride = 5
window_length = 10

file_name = '{}-{}_seq_stp.pkl'.format(window_length, stride)

with open(path / file_name, "rb") as f:
    st_petersburg = pickle.load(f)

first_record = st_petersburg[10]

fig, axs = plt.subplots(2,4)
axs = trim_axs(axs, 8)
for ax, signal in zip(axs, first_record.T):
    ax.plot(signal)

plt.show()

