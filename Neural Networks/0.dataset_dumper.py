# %%
from general_utils import download
import pandas as pd
import wfdb
from tqdm import tqdm
from scipy import signal
import pickle
from path import Path
from matplotlib import pyplot as plt
import numpy as np

#

CHS = ['v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'i', 'ii']
FS = 250


# %%
def ptb_dataset_extractor(save_to='ptb_dataframe.pkl', plot=False, fs_target=FS, chs=CHS, path=Path('datasets')):
    print('\n#### - PTB dataset - ####')
    print('#### - Downloading - ####')
    # PTB dataset processing
    dest_directory = download(
        'https://physionet.org/static/published-projects/ptbdb/ptb-diagnostic-ecg-database-1.0.0.zip',
        dest_directory=path)
    print('#### - Reading - ####')
    healthy_records = []
    for patient in tqdm(dest_directory.dirs(), desc='Patient elaboration', position=0):
        for file in patient.files():
            if '.dat' in file:
                record = wfdb.io.rdsamp(file[:-4])
                reason = record[1]['comments'][4]
                if "Healthy" in reason:
                    fs = record[1]['fs']
                    data = record[0]
                    if fs != fs_target:
                        to_samples = int(data.shape[0] / fs * fs_target)
                        # print('{} -> {}'.format(data.shape[0],to_samples))
                        data = signal.resample(data, to_samples)
                    df = pd.DataFrame(data, columns=record[1]['sig_name'], dtype='float32')
                    df = df[chs]
                    healthy_records.append(df)

    if plot:
        plt.figure()
        plt.plot(healthy_records[0][chs[0]][:5000])
        plt.show()

    print('#### - Saving - ####')
    with open(save_to, "wb") as f:
        pickle.dump(healthy_records, f)

    print('#### - Done - ####\n')


# %%

# st petersburg
def st_petersburg_extractor(save_to='st-petersburg_dict.pkl', plot=False, fs_target=FS, chs=CHS, path=Path('datasets')):
    print('\n#### - St. Petersburg dataset - ####')
    print('#### - Downloading - ####')
    dest_directory = download(
        'https://physionet.org/static/published-projects/incartdb/st-petersburg-incart-12-lead-arrhythmia-database-1.0.0.zip',
        dest_directory=path)

    print('#### - Reading - ####')
    records = []
    for file in tqdm(list(dest_directory.files())):
        resume = {}
        if '.dat' in file:
            record = wfdb.io.rdsamp(file[:-4])
            ann = wfdb.io.rdann(file[:-4], 'atr')
            fs = record[1]['fs']
            data = record[0]
            if fs != fs_target:
                to_samples = int(data.shape[0] / fs * fs_target)
                data = signal.resample(data, to_samples)

            df = pd.DataFrame(data, columns=[ch.lower() for ch in record[1]['sig_name']], dtype='float32')
            df = df[chs]

            resume['data'] = df
            resume['symbols'] = ann.symbol
            # annotation on r_peak -> hamilton algorithm after resampling but rescaling index
            resume['places'] = np.array(ann.sample / fs * fs_target, dtype='int')
            records.append(resume)

    if plot:
        plt.figure()
        plt.plot(records[0]['data'][chs[1]][:5000])
        ann = records[0]['places'][records[0]['places'] < 5000]
        for pos in ann:
            plt.axvline(pos, color='green')
        plt.show()

    print('#### - Saving - ####')
    with open(save_to, "wb") as f:
        pickle.dump(records, f)

    print('#### - Done - ####\n')


def mit_bih_extractor(save_to='mit_bih.pkl', plot=False, fs_target=FS, chs=CHS, path=Path('datasets')):
    print('\n#### - MIT-BIH dataset - ####')
    # most frequent leads = II and V1
    print('#### - Downloading - ####')
    dest_directory = download(
        'https://storage.googleapis.com/mitdb-1.0.0.physionet.org/mit-bih-arrhythmia-database-1.0.0.zip',
        dest_directory=path)

    print('#### - Reading - ####')
    records = []
    for file in tqdm(list(dest_directory.files())):
        resume = {}
        if '.dat' in file:
            record = wfdb.io.rdsamp(file[:-4])
            if record[1]['sig_name'] == ['MLII', 'V1']:
                ann = wfdb.io.rdann(file[:-4], 'atr')
                fs = record[1]['fs']
                data = record[0]
                if fs != fs_target:
                    to_samples = int(data.shape[0] / fs * fs_target)
                    data = signal.resample(data, to_samples)

                df = pd.DataFrame(data, columns=[ch.lower() for ch in record[1]['sig_name']], dtype='float32')

                resume['data'] = df
                resume['symbols'] = ann.symbol
                # annotation on r_peak -> hamilton algorithm after resampling but rescaling index
                resume['places'] = np.array(ann.sample / fs * fs_target, dtype='int')
                records.append(resume)

    if plot:
        plt.figure()
        plt.plot(records[0]['data'][chs[1]][:5000])
        ann = records[0]['places'][records[0]['places'] < 5000]
        for pos in ann:
            plt.axvline(pos, color='green')
        plt.show()

    print('#### - Saving - ####')
    with open(save_to, "wb") as f:
        pickle.dump(records, f)

    print('#### - Done - ####\n')


if __name__ == '__main__':
    ptb_dataset_extractor()
    st_petersburg_extractor()
    mit_bih_extractor()
