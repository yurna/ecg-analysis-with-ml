from general_utils import denoise, trim_axs
from tqdm import tqdm
import pickle
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import time

# %%

# selected fs
FS = 250


# %% load
def ptb_dataset(file='ptb_dataframe.pkl', plot=False, fs=FS, stride=4, window_length=10, path=Path('data')):
    print('\n#### - PTB dataset - ####')
    path.mkdir(exist_ok=True, parents=True)
    with open(file, "rb") as f:
        healthy_sequences = pickle.load(f)

    # %%
    if plot:
        print('#### - Plotting - ####')
        # show denoising
        first_record = healthy_sequences[0]
        denoised = []
        time_step = 1 / fs
        scale = 0.1
        for column in list(first_record):
            fig, axs = plt.subplots(2, 2, num=column)
            axs = trim_axs(axs, 4)
            for ax in axs[:2]:
                ax.set_ylim(-1, len(first_record[column]))
                ax.set_xlim(-100, 100)

            for ax in axs[2:]:
                ax.set_xlim(10000, 12000)

            data = denoise(first_record[column], scale=scale, type='sym5', level = 3, discarded=0, range_freq=[1, 60], fs = fs)
            denoised.append(data)
            ps_denoised = np.abs(np.fft.fft(data)) ** 2
            axs[3].plot(data)

            data = first_record[column]
            ps_raw = np.abs(np.fft.fft(data)) ** 2
            axs[2].plot(data)

            freqs = np.fft.fftfreq(data.size, time_step)
            idx = np.argsort(freqs)
            axs[0].plot(freqs[idx], ps_raw[idx])
            axs[1].plot(freqs[idx], ps_denoised[idx])

        plt.show()

    # %%
    print('#### - Denoising - ####')
    time.sleep(0.5)
    # denoising all data
    for count, record in enumerate(tqdm(healthy_sequences)):
        denoised = []
        scale = 0
        for column in list(record):
            data = denoise(record[column], type='sym5', level = 3, discarded=0, scale=scale, range_freq=[1, 50], fs = fs)
            denoised.append(data)
        healthy_sequences[count] = np.array(denoised).T
    time.sleep(0.5)
    print('#### - Extracting fixed length sequences - ####')
    # seconds
    ten_sec_sequences = []
    for seq in healthy_sequences:
        start_from = np.arange(0, len(seq), stride * fs)
        start_from = [start for start in start_from if len(seq) - start > window_length * fs]
        ten_sec_sequences += [seq[start:start + window_length * FS] / np.max(np.abs(seq[start:start + window_length * FS])) for start in start_from]

    ten_sec_sequences = np.array(ten_sec_sequences)

    print('#### - Saving - ####')

    # %%
    with open(path / '{}-{}_seq_ptb.pkl'.format(window_length, stride), "wb") as f:
        pickle.dump(ten_sec_sequences, f)

    print('#### - Done - ####\n')


# #%%
#
# from biosppy.signals.ecg import hamilton_segmenter
#
# r_peaks = hamilton_segmenter(denoised[0], FS)[0]
# plt.figure()
# plt.plot(denoised[0])
# for pos in r_peaks:
#     plt.axvline(pos, color = 'green')
# plt.show()
#
# peak_peak = [denoised[0][from_i:to_i] for from_i, to_i in zip(r_peaks[:-1], r_peaks[1:])]
#
# plt.figure()
# plt.plot(peak_peak[0])
# plt.show()

# %%
###################################################################################
def st_petersburg_dataset(file='st-petersburg_dict.pkl', plot=False, fs=FS, stride=4, window_length=10,
                          path=Path('data')):
    print('\n#### - St. Petersburg dataset - ####')
    path.mkdir(exist_ok=True, parents=True)
    # St petersburg
    with open(file, "rb") as f:
        st_petersburg = pickle.load(f)

    # todo denoise before or after?
    if plot:
        print('#### - Plotting - ####')
        # show denoising
        first_record = st_petersburg[0]['data']
        denoised = []
        time_step = 1 / fs
        scale = 0.1
        for column in list(first_record):
            fig, axs = plt.subplots(2, 2, num=column)
            axs = trim_axs(axs, 4)
            for ax in axs[:2]:
                ax.set_ylim(-1, len(first_record[column]))
                ax.set_xlim(-100, 100)

            for ax in axs[2:]:
                ax.set_xlim(10000, 12000)

            data = denoise(first_record[column], scale=scale, level = 3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
            ps_denoised = np.abs(np.fft.fft(data)) ** 2
            axs[3].plot(data)

            data = first_record[column]
            ps_raw = np.abs(np.fft.fft(data)) ** 2
            axs[2].plot(data)

            freqs = np.fft.fftfreq(data.size, time_step)
            idx = np.argsort(freqs)
            axs[0].plot(freqs[idx], ps_raw[idx])
            axs[1].plot(freqs[idx], ps_denoised[idx])

        plt.show()

    # %%
    # Filter all data
    print('#### - Denoising - ####')
    time.sleep(0.5)
    for count, record in enumerate(tqdm(st_petersburg)):
        denoised = []
        scale = 0.1
        for column in list(record['data']):
            data = denoise(record['data'][column], scale=scale, level=3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
        st_petersburg[count]['data'] = np.array(denoised).T

    if plot:
        plt.figure()
        plt.plot(st_petersburg[0]['data'][:, 0])
        for i, x in enumerate(st_petersburg[0]['places']):
            if st_petersburg[0]['symbols'][i] != 'N':
                plt.axvline(x, color='green')
        plt.show()

    # %%
    # extract healthy sequences
    time.sleep(0.5)
    print('#### - Extracting healthy sequences - ####')
    healthy_sequences = []
    total_length = 0
    for record in tqdm(st_petersburg):
        places = record['places']
        diagnosis = record['symbols']

        indices = [i for i, x in enumerate(diagnosis) if x != "N"]
        from_to = [(f + 1, t - 1) for f, t in zip(indices[:-1], indices[1:])]

        for f, t in from_to:
            start = places[f]
            stop = places[t]
            length = (stop - start) / fs
            if length > 10:
                total_length += length
                healthy_sequences.append(record['data'][start:stop])

    # extract arrhythmic sequences
    # one 10 seq for each event
    print('#### - Extracting arrhythmic sequences - ####')
    arrhythmic_sequences = []
    for record in tqdm(st_petersburg):
        places = record['places']
        diagnosis = record['symbols']

        indices = [i for i, x in enumerate(diagnosis) if x != "N"]
        for i in places[indices]:
            # control that the interval is inside the record
            if i - (fs * 5) > 0 and i + (fs * 5) < len(record['data']):
                # 5 sec before and 5 sec after
                # todo dynamic window
                current = (i - (fs * 5), i + (fs * 5))
                # index of diagnosis
                place_interval = []
                # diagnosis
                diagnosis_interval = []
                for count, p in enumerate(places):
                    if current[0] < p < current[1]:
                        place_interval.append(p - current[0])
                        diagnosis_interval.append(diagnosis[count])
                    elif current[1] < p:
                        break
                # sequence, index, diagnosis
                arrhythmic_sequences.append((record['data'][current[0]:current[1]],
                                             place_interval, diagnosis_interval))

    if plot:
        plt.figure()
        plt.plot(arrhythmic_sequences[0][0][:, 0])
        for p, d in zip(arrhythmic_sequences[0][1],arrhythmic_sequences[0][2]):
            if d == 'N':
                plt.axvline(p, color='green')
            else:
                plt.axvline(p, color='red')
        plt.show()

        for f, t in from_to:
            start = places[f]
            stop = places[t]
            length = (stop - start) / fs
            if length > 10:
                total_length += length
                healthy_sequences.append(record['data'][start:stop])


    print('#### - Extracting fixed length sequences - ####')
    # seconds
    ten_sec_sequences = []
    for seq in healthy_sequences:
        start_from = np.arange(0, len(seq), stride * fs)
        start_from = [start for start in start_from if len(seq) - start > window_length * fs]
        ten_sec_sequences += [seq[start:start + window_length * FS] / np.max(np.abs(seq[start:start + window_length * FS])) for start in start_from]
    ten_sec_sequences = np.array(ten_sec_sequences)

    print('#### - Saving - ####')

    with open(path / '{}-{}_seq_stp.pkl'.format(window_length, stride), "wb") as f:
        pickle.dump((ten_sec_sequences, arrhythmic_sequences), f)

    # TODO extract sequences with arrhythmia

    print('#### - Done - ####\n')

def st_petersburg_dataset_unsupervised(file='st-petersburg_dict.pkl', plot=False, fs=FS, path=Path('data')):
    print('\n#### - St. Petersburg dataset unsupervised sequences - ####')
    path.mkdir(exist_ok=True, parents=True)
    # St petersburg
    with open(file, "rb") as f:
        st_petersburg = pickle.load(f)

    # todo denoise before or after?
    if plot:
        print('#### - Plotting - ####')
        # show denoising
        first_record = st_petersburg[0]['data']
        denoised = []
        time_step = 1 / fs
        scale = 0.1
        for column in list(first_record):
            fig, axs = plt.subplots(2, 2, num=column)
            axs = trim_axs(axs, 4)
            for ax in axs[:2]:
                ax.set_ylim(-1, len(first_record[column]))
                ax.set_xlim(-100, 100)

            for ax in axs[2:]:
                ax.set_xlim(10000, 12000)

            data = denoise(first_record[column], scale=scale, level = 3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
            ps_denoised = np.abs(np.fft.fft(data)) ** 2
            axs[3].plot(data)

            data = first_record[column]
            ps_raw = np.abs(np.fft.fft(data)) ** 2
            axs[2].plot(data)

            freqs = np.fft.fftfreq(data.size, time_step)
            idx = np.argsort(freqs)
            axs[0].plot(freqs[idx], ps_raw[idx])
            axs[1].plot(freqs[idx], ps_denoised[idx])

        plt.show()

    # %%
    # Filter all data
    print('#### - Denoising - ####')
    time.sleep(0.5)
    for count, record in enumerate(tqdm(st_petersburg)):
        denoised = []
        scale = 0.1
        for column in list(record['data']):
            data = denoise(record['data'][column], scale=scale, level=3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
        st_petersburg[count]['data'] = np.array(denoised).T

    if plot:
        plt.figure()
        plt.plot(st_petersburg[0]['data'][:, 0])
        for i, x in enumerate(st_petersburg[0]['places']):
            if st_petersburg[0]['symbols'][i] != 'N':
                plt.axvline(x, color='green')
        plt.show()
    print('#### - Extracting fixed length sequences with no windowing - ####')
    ten_sec_signals = []
    ten_sec_annotation = []
    for record in tqdm(st_petersburg):
        beats_dataframe = record['places']
        diagnosis_dataframe = record['symbols']
        # signal
        windowed = record['data'].reshape(-1,2500,8)
        ten_sec_signals.append(windowed)
        # annotations

        current_beats, current_diagnosis = [], []
        current_max = 2500
        for place, diagnosis in zip(beats_dataframe, diagnosis_dataframe):
            if place < current_max:
                current_beats.append(place - current_max + 2500)
                current_diagnosis.append(diagnosis)
            else:
                ten_sec_annotation.append((current_beats, current_diagnosis))
                current_beats, current_diagnosis = [], []
                current_max += 2500
        ten_sec_annotation.append((current_beats, current_diagnosis))

    ten_sec_signals = np.concatenate(ten_sec_signals)

    formatted = []
    for signal, (places, annotations) in zip(ten_sec_signals, ten_sec_annotation):
        formatted.append((signal, places, annotations))

    print('#### - Saving - ####')

    with open(path / 'seq_stp_unsupervised.pkl', "wb") as f:
        pickle.dump(formatted, f)

    # TODO extract sequences with arrhythmia

    print('#### - Done - ####\n')


def mit_bih_dataset(file='mit_bih.pkl', plot=False, fs=FS, stride=4, window_length=10,
                          path=Path('data')):
    print('\n#### - MIT-BIH dataset - ####')
    path.mkdir(exist_ok=True, parents=True)
    # St petersburg
    with open(file, "rb") as f:
        mit_bih = pickle.load(f)

    # todo denoise before or after?
    if plot:
        print('#### - Plotting - ####')
        # show denoising
        first_record = mit_bih[0]['data']
        denoised = []
        time_step = 1 / fs
        scale = 0.1
        for column in list(first_record):
            fig, axs = plt.subplots(2, 2, num=column)
            axs = trim_axs(axs, 4)
            for ax in axs[:2]:
                ax.set_ylim(-1, len(first_record[column]))
                ax.set_xlim(-100, 100)

            for ax in axs[2:]:
                ax.set_xlim(10000, 12000)

            data = denoise(first_record[column], scale=scale, level = 3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
            ps_denoised = np.abs(np.fft.fft(data)) ** 2
            axs[3].plot(data)

            data = first_record[column]
            ps_raw = np.abs(np.fft.fft(data)) ** 2
            axs[2].plot(data)

            freqs = np.fft.fftfreq(data.size, time_step)
            idx = np.argsort(freqs)
            axs[0].plot(freqs[idx], ps_raw[idx])
            axs[1].plot(freqs[idx], ps_denoised[idx])

        plt.show()

    # %%
    # Filter all data
    print('#### - Denoising - ####')
    time.sleep(0.5)
    for count, record in enumerate(tqdm(mit_bih)):
        denoised = []
        scale = 0.1
        for column in list(record['data']):
            data = denoise(record['data'][column], scale=scale, level=3, discarded=0, type='sym5', range_freq=[1, 50], fs = fs)
            denoised.append(data)
        mit_bih[count]['data'] = np.array(denoised).T

    if plot:
        plt.figure()
        plt.plot(mit_bih[0]['data'][:, 0])
        for i, x in enumerate(mit_bih[0]['places']):
            if mit_bih[0]['symbols'][i] != 'N':
                plt.axvline(x, color='green')
        plt.show()

    # %%

    # extract healthy sequences
    time.sleep(0.5)
    print('#### - Extracting healthy sequences - ####')
    healthy_sequences = []
    total_length = 0
    for record in tqdm(mit_bih):
        places = record['places']
        diagnosis = record['symbols']

        indices = [i for i, x in enumerate(diagnosis) if x != "N"]
        from_to = [(f + 1, t - 1) for f, t in zip(indices[:-1], indices[1:])]

        for f, t in from_to:
            start = places[f]
            stop = places[t]
            length = (stop - start) / fs
            if length > 10:
                total_length += length
                healthy_sequences.append(record['data'][start:stop])

    # extract arrhythmic sequences
    # one 10 seq for each event
    print('#### - Extracting arrhythmic sequences - ####')
    arrhythmic_sequences = []
    for record in tqdm(mit_bih):
        places = record['places']
        diagnosis = record['symbols']

        indices = [i for i, x in enumerate(diagnosis) if x != "N"]
        for i in places[indices]:
            # control that the interval is inside the record
            if i - (fs * 5) > 0 and i + (fs * 5) < len(record['data']):
                # 5 sec before and 5 sec after
                # todo dynamic window
                current = (i - (fs * 5), i + (fs * 5))
                # index of diagnosis
                place_interval = []
                # diagnosis
                diagnosis_interval = []
                for count, p in enumerate(places):
                    if current[0] < p < current[1]:
                        place_interval.append(p - current[0])
                        diagnosis_interval.append(diagnosis[count])
                    elif current[1] < p:
                        break
                # sequence, index, diagnosis
                arrhythmic_sequences.append((record['data'][current[0]:current[1]],
                                             place_interval, diagnosis_interval))

    if plot:
        plt.figure()
        plt.plot(arrhythmic_sequences[0][0][:, 0])
        for p, d in zip(arrhythmic_sequences[0][1],arrhythmic_sequences[0][2]):
            if d == 'N':
                plt.axvline(p, color='green')
            else:
                plt.axvline(p, color='red')
        plt.show()

        for f, t in from_to:
            start = places[f]
            stop = places[t]
            length = (stop - start) / fs
            if length > 10:
                total_length += length
                healthy_sequences.append(record['data'][start:stop])


    print('#### - Extracting fixed length sequences - ####')
    # seconds
    ten_sec_sequences = []
    for seq in healthy_sequences:
        start_from = np.arange(0, len(seq), stride * fs)
        start_from = [start for start in start_from if len(seq) - start > window_length * fs]
        ten_sec_sequences += [seq[start:start + window_length * FS] / np.max(np.abs(seq[start:start + window_length * FS])) for start in start_from]
    ten_sec_sequences = np.array(ten_sec_sequences)

    print('#### - Saving - ####')

    with open(path / '{}-{}_seq_mit.pkl'.format(window_length, stride), "wb") as f:
        pickle.dump((ten_sec_sequences, arrhythmic_sequences), f)

    # TODO extract sequences with arrhythmia

    print('#### - Done - ####\n')


if __name__ == '__main__':
    stride = 4
    window_length = 10
    #ptb_dataset(stride=stride, window_length=window_length)
    #st_petersburg_dataset(stride=stride, window_length=window_length)
    st_petersburg_dataset_unsupervised()
