import torch
import torch.nn as nn

class VRAE(nn.Module):
    def __init__(self, input_size, embedded_size, device, seq_len, n_layer = 3, dropout = .2):
        super(VRAE, self).__init__()
        self.device = device
        self.embedded_size = embedded_size
        self.input_size = input_size
        self.seq_len = seq_len
        self.n_layer = n_layer

        self.encoder = nn.LSTM(self.input_size, 1, self.n_layer, batch_first=True, dropout=dropout)
        self.decoder1 = nn.LSTM(1, self.input_size, self.n_layer, batch_first=True, dropout=dropout)

        self.fc21 = nn.Linear(self.seq_len, self.embedded_size)
        self.fc22 = nn.Linear(self.seq_len, self.embedded_size)

        self.fc3 = nn.Linear(self.embedded_size, self.seq_len)

        self.splus = nn.Softplus()
        self.lrelu = nn.LeakyReLU(negative_slope=0.1)
        self.tanh = nn.Tanh()


    def encode(self, x):
        self.bs = x.shape[0]
        self.encoder.flatten_parameters()
        output, hidden = self.encoder(x)
        output = output.squeeze(2)
        return self.fc21(output), self.splus(self.fc22(output)), hidden

    def decode(self, z):
        h3_i = self.lrelu(self.fc3(z))
        output = self.decoder1(h3_i.unsqueeze(2))
        return output

    def reparametrize(self, mu, logvar):
        std = logvar.mul(0.5).exp_()
        if self.device != 'cpu':
            eps = torch.FloatTensor(std.size()).normal_().cuda()
        else:
            eps = torch.FloatTensor(std.size()).normal_()
        return eps.mul(std).add_(mu)

    def forward(self, x):
        # print("x", x.size())
        mu, logvar, hidden = self.encode(x)
        # print("mu, logvar", mu.size(), logvar.size())
        z = self.reparametrize(mu, logvar)
        # print("z", z.size())
        decoded, hidden = self.decode(z)
        # print("decoded", decoded.size())
        return self.tanh(decoded), mu, logvar


class VRAE_conv(nn.Module):
    def __init__(self, input_size, device, n_layer = 3, dropout = .2):
        super(VRAE_conv, self).__init__()
        self.device = device
        self.input_size = input_size
        self.n_layer = n_layer

        self.encoder = nn.LSTM(self.input_size, 1, self.n_layer, batch_first=True, dropout=dropout)
        self.decoder = nn.LSTM(1, self.input_size, self.n_layer, batch_first=True, dropout=dropout)

        self.fc21 = nn.Conv1d(1, 1, 20, 10, dilation=2, padding=2)
        self.fc22 = nn.Conv1d(1, 1, 20, 10, dilation=2, padding=2)

        self.fc3 = nn.ConvTranspose1d(1, 1, 20, 10, dilation=2, padding=2)

        self.splus = nn.Softplus()
        self.lrelu = nn.LeakyReLU(negative_slope=0.1)
        self.tanh = nn.Tanh()
        self.bn = nn.BatchNorm1d(175)


    def encode(self, x):
        self.bs = x.shape[0]
        self.encoder.flatten_parameters()
        output, hidden = self.encoder(x)
        #output = self.bn(output).permute(0,2,1)
        output = output.permute(0, 2, 1)
        return self.lrelu(self.fc21(output)).squeeze(), self.splus(self.fc22(output)).squeeze()

    def decode(self, z):

        h3_i = self.lrelu(self.fc3(z.unsqueeze(1)))
        #h3_i = self.bn(h3_i.permute(0,2,1))
        h3_i = h3_i.permute(0,2,1)
        output = self.decoder(h3_i)
        return output

    def reparametrize(self, mu, logvar):
        std = torch.exp(logvar / 2)
        if self.device != 'cpu':
            eps = torch.cuda.FloatTensor(std.size()).normal_()
        else:
            eps = torch.FloatTensor(std.size()).normal_()
        return eps.mul(std).add_(mu)

    def forward(self, x):
        # print("x", x.size())
        mu, logvar = self.encode(x)
        # print("mu, logvar", mu.size(), logvar.size())
        z = self.reparametrize(mu, logvar)
        # print("z", z.size())
        decoded, hidden = self.decode(z)
        # print("decoded", decoded.size())
        return self.tanh(decoded), mu, logvar
