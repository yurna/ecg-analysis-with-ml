from .useful_layers import PrintLayer
import torch
import torch.nn as nn

class VAE(nn.Module):
    def __init__(self, n_in, n_z, device, base = 64, debug = False):
        super(VAE, self).__init__()
        self.debug = debug
        self.device = device
        self.nz = n_z
        self.n_in = n_in
        self.min_dense = 128
        self.base = base

        self.encoder = self.encoder_net()

        self.decoder = self.decoder_net()

        self.fc1 = nn.Linear(self.min_dense * 2, self.min_dense)
        self.fc21 = nn.Linear(self.min_dense, n_z)
        self.fc22 = nn.Linear(self.min_dense, n_z)

        self.fc3 = nn.Linear(n_z, self.min_dense)
        self.fc4 = nn.Linear(self.min_dense, self.min_dense * 2)

        self.lrelu = nn.LeakyReLU()
        self.relu = nn.ReLU()
        # self.sigmoid = nn.Sigmoid()

    def encode_block(self, from_size, to_size, batch_norm = True, kernel = 4, stride = 2, padding = 1, debug = False) :
        if batch_norm:
            encode =  nn.Sequential(
                nn.Conv2d(from_size, to_size, kernel, stride, padding, bias=False),
                nn.BatchNorm2d(to_size),
                nn.LeakyReLU(0.2, inplace=True)
            )
        else:
            encode = nn.Sequential(
                nn.Conv2d(from_size, to_size, kernel, stride, padding, bias=False),
                nn.LeakyReLU(0.2, inplace=True)
            )
        if debug:
            encode.add_module('debug', PrintLayer())
        return encode

    def encoder_net(self):
        return nn.Sequential(
            self.encode_block(1, self.base, debug=self.debug),
            self.encode_block(self.base, self.base * 2, debug=self.debug),
            self.encode_block(self.base * 2, self.base * 4, debug=self.debug),
            self.encode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.encode_block(self.base * 4, self.base * 8, debug=self.debug),
            self.encode_block(self.base * 8, self.min_dense * 2, padding=0, stride=1, batch_norm=False, debug=self.debug)

        )

    def decode_block(self, from_size, to_size, batch_norm = True, kernel = 4, stride = 2, padding = 1,debug = False):
        if batch_norm:
            decode = nn.Sequential(
                nn.ConvTranspose2d(from_size, to_size, kernel, stride, padding, bias=False),
                nn.BatchNorm2d(to_size),
                nn.ReLU(True)
            )
        else:
            decode = nn.Sequential(
                nn.ConvTranspose2d(from_size, to_size, kernel, stride, padding, bias=False),
                nn.ReLU(True)
            )
        if debug:
            decode.add_module('debug', PrintLayer())
        return decode

    def decoder_net(self):
        decoder =  nn.Sequential(
            self.decode_block(self.min_dense * 2, self.base * 8, padding=0, stride = 1, debug=self.debug),
            self.decode_block(self.base * 8, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 2, debug=self.debug),

        )
        if self.debug:
            final_section = nn.Sequential(
                nn.ConvTranspose2d(self.base * 2, self.n_in, 4, 2, 1, bias=False),
                PrintLayer(),
                nn.Tanh()
            )
        else:
            final_section = nn.Sequential(
                nn.ConvTranspose2d(self.base * 2, self.n_in, 4, 2, 1, bias=False),
                nn.Tanh()
            )
        decoder.add_module('final', final_section)
        return decoder


    def encode(self, x):
        conv = self.encoder(x);
        # print("encode conv", conv.size())
        h1 = self.fc1(conv.view(-1, self.min_dense * 2))
        # print("encode h1", h1.size())
        return self.fc21(h1), self.fc22(h1)

    def decode(self, z):
        h3 = self.relu(self.fc3(z))
        deconv_input = self.fc4(h3)
        # print("deconv_input", deconv_input.size())
        deconv_input = deconv_input.view(-1,self.min_dense * 2,1,1)
        # print("deconv_input", deconv_input.size())
        return self.decoder(deconv_input)

    def reparametrize(self, mu, logvar):
        std = logvar.mul(0.5).exp_()
        eps = torch.FloatTensor(std.size()).normal_().to(self.device)
        return eps.mul(std).add_(mu)

    def forward(self, x):
        # print("x", x.size())
        mu, logvar = self.encode(x)
        # print("mu, logvar", mu.size(), logvar.size())
        z = self.reparametrize(mu, logvar)
        #print("z", z.size())
        decoded = self.decode(z)
        # print("decoded", decoded.size())
        return decoded, mu, logvar