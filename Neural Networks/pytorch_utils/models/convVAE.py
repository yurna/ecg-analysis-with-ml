from .useful_layers import PrintLayer
import torch
import torch.nn as nn

class Conv_VAE(nn.Module):
    def __init__(self, n_in, n_z, device, base = 64, debug = False):
        super(Conv_VAE, self).__init__()
        self.debug = debug
        self.device = device
        self.nz = n_z
        self.n_in = n_in
        self.min_dense = 128
        self.base = base

        self.encoder = self.encoder_net()

        self.decoder = self.decoder_net()

        self.cn21 = self.encode_block(self.base * 8, self.nz, padding=0, stride=1,
                                      batch_norm=False, activation = False, debug=self.debug)
        self.cn22 = self.encode_block(self.base * 8, self.nz, padding=0, stride=1,
                                      batch_norm=False, activation = False, debug=self.debug)

        self.lrelu = nn.LeakyReLU()
        self.relu = nn.ReLU()
        # self.sigmoid = nn.Sigmoid()

    def encode_block(self, from_size, to_size, batch_norm = True, kernel = 4, stride = 2, padding = 1, debug = False, activation = True) :
        encode = nn.Sequential(nn.Conv2d(from_size, to_size, kernel, stride, padding, bias=False))
        if batch_norm:
            encode.add_module(nn.BatchNorm2d(to_size))
        if activation:
            encode.add_module(nn.LeakyReLU(0.2, inplace=True))
        if debug:
            encode.add_module('debug', PrintLayer())
        return encode

    def encoder_net(self):
        return nn.Sequential(
            self.encode_block(1, self.base, debug=self.debug),
            self.encode_block(self.base, self.base * 2, debug=self.debug),
            self.encode_block(self.base * 2, self.base * 4, debug=self.debug),
            self.encode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.encode_block(self.base * 4, self.base * 8, debug=self.debug)
        )

    def decode_block(self, from_size, to_size, batch_norm = True, kernel = 4, stride = 2, padding = 1,debug = False, activation = True):
        decode = nn.Sequential(nn.ConvTranspose2d(from_size, to_size, kernel, stride, padding, bias=False))
        if batch_norm:
            decode.add_module('batch norm', nn.BatchNorm2d(to_size))
        if activation:
            decode.add_module('activation', nn.ReLU(True))
        if debug:
            decode.add_module('debug', PrintLayer())
        return decode

    def decoder_net(self):
        decoder =  nn.Sequential(
            self.decode_block(self.nz, self.base * 8, padding=0, stride = 1, debug=self.debug),
            self.decode_block(self.base * 8, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 4, debug=self.debug),
            self.decode_block(self.base * 4, self.base * 2, debug=self.debug),

        )
        if self.debug:
            final_section = nn.Sequential(
                nn.ConvTranspose2d(self.base * 2, self.n_in, 4, 2, 1, bias=False),
                PrintLayer(),
                nn.Tanh()
            )
        else:
            final_section = nn.Sequential(
                nn.ConvTranspose2d(self.base * 2, self.n_in, 4, 2, 1, bias=False),
                nn.Tanh()
            )
        decoder.add_module('final', final_section)
        return decoder


    def encode(self, x):
        conv = self.encoder(x);
        left = self.cn21(conv)
        right = self.cn22(conv)
        # print("encode conv", conv.size())
        # print("encode h1", h1.size())
        return left.view(-1, self.nz), right.view(-1, self.nz)

    def decode(self, z):
        # print("deconv_input", deconv_input.size())
        deconv_input = z.view(-1,self.nz,1,1)

        # print("deconv_input", deconv_input.size())
        return self.decoder(deconv_input)

    def reparametrize(self, mu, logvar):
        std = logvar.mul(0.5).exp_()
        eps = torch.FloatTensor(std.size()).normal_().to(self.device)
        return eps.mul(std).add_(mu)

    def forward(self, x):
        # print("x", x.size())
        mu, logvar = self.encode(x)
        # print("mu, logvar", mu.size(), logvar.size())
        z = self.reparametrize(mu, logvar)
        #print("z", z.size())
        decoded = self.decode(z)
        # print("decoded", decoded.size())
        return decoded, mu, logvar