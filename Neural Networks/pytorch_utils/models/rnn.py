# -*- coding: utf-8 -*-
"""
Created on Tue Nov 27 21:28:56 2018

@author: yurin
"""
import torch.nn as nn
import torch


class RNN_conv(nn.ModuleList):
    def __init__(self, input_size, hidden_units, layers_num, output_size, dropout_prob=0):
        # Call the parent init function (required!)
        super().__init__()
        # Define recurrent layer
        self.dropout_prob = dropout_prob
        self.rnn = torch.nn.LSTM(input_size=15,
                                 hidden_size=int(hidden_units),
                                 num_layers=layers_num,
                                 dropout=dropout_prob,
                                 batch_first=True,
                                 bidirectional=False)

        # Define linear convolution layers
        self.first_conv3 = nn.Conv1d(input_size, input_size + 2, 1, padding=1)
        self.conv3 = nn.Conv1d(input_size + 2, 15, 2, padding=0)

        # Define output layer
        self.out = torch.nn.Linear(hidden_units, output_size)

    def forward(self, x, state=None):
        # Conv1D
        x = x.permute(0, 2, 1)
        x = self.first_conv3(x)
        x = self.conv3(x).permute(0, 2, 1)
        x = nn.functional.leaky_relu(x, 0.05)
        # LSTM
        x, rnn_state = self.rnn(x, state)

        if type(x) == torch.nn.utils.rnn.PackedSequence:
            x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
            # Linear layer
        x = self.out(x)
        return x, rnn_state


class RNN_first(torch.nn.Module):

    def __init__(self, input_size, hidden_units, layers_num, output_size, dropout_prob=0):
        # Call the parent init function (required!)
        super().__init__()
        # Define recurrent layer
        self.rnn = torch.nn.LSTM(input_size=input_size,
                                 hidden_size=hidden_units,
                                 num_layers=layers_num,
                                 dropout=dropout_prob,
                                 batch_first=True,
                                 bidirectional=False)
        # Define output layer
        self.out = torch.nn.Linear(hidden_units, output_size)

    def forward(self, x, state=None):
        # LSTM
        self.rnn.flatten_parameters()
        x, rnn_state = self.rnn(x, state)

        if type(x) == torch.nn.utils.rnn.PackedSequence:
            x, _ = torch.nn.utils.rnn.pad_packed_sequence(x, batch_first=True)
            # Linear layer
        x = self.out(x)
        return x, rnn_state

class RNN_all_lstm(torch.nn.Module):

    def __init__(self, input_size, hidden_units, layers_num, output_size, dropout_prob=0):
        # Call the parent init function (required!)
        super().__init__()
        # Define recurrent layer
        self.rnn1 = torch.nn.LSTM(input_size=input_size,
                                 hidden_size=hidden_units,
                                 num_layers=layers_num - 1,
                                 dropout=dropout_prob,
                                 batch_first=True,
                                 bidirectional=False)

        self.rnn2 = torch.nn.LSTM(input_size=hidden_units,
                                 hidden_size=output_size,
                                 num_layers=1,
                                 dropout=0,
                                 batch_first=True,
                                 bidirectional=False)

    def forward(self, x, state=None):
        # LSTM
        self.rnn1.flatten_parameters()
        x, rnn_state = self.rnn1(x, state)
        self.rnn2.flatten_parameters()
        # need (1,5,8) hidden
        x, rnn_state = self.rnn2(x, state)
        return x, rnn_state