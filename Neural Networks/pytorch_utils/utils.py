import numpy as np
import pandas as pd
from tqdm import tqdm
import torch
import torch.nn as nn
from torch.utils.data import DataLoader, SubsetRandomSampler
import random
from time import time

random.seed(1)
np.random.seed(1)
torch.manual_seed(1)
torch.cuda.manual_seed(1)
# from rdp import rdp

pd.options.mode.chained_assignment = None


class Loss(nn.Module):
    def __init__(self):
        super(Loss, self).__init__()
        self.mse_loss = nn.MSELoss(reduction="sum")
        self.l1_loss = nn.L1Loss(reduction='sum')

    def forward(self, recon_x, x, mu, logvar):
        MSE = self.mse_loss(recon_x, x)
        KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
        return MSE, KLD


# Compute the number of parameters
def count_parameters(model):
    return sum(p.numel() for p in model.parameters())


def get_loaders(dataset, batch_size, pin_memory, fn=None, validation_split_percentage=.1, test_split_percentage=.1):
    batch_size = batch_size
    validation_split_percentage = validation_split_percentage
    test_split_percentage = test_split_percentage

    # Creating data indices for training and validation splits:
    dataset_size = len(dataset)
    indices = list(range(dataset_size))
    # Shuffle with fixed seed for reproducibility
    random.Random(1).shuffle(indices)
    val_split = int(np.floor(validation_split_percentage * dataset_size))
    test_split = int(np.floor(test_split_percentage * dataset_size))

    train_indices = indices[0:dataset_size - val_split - test_split]
    val_indices = indices[dataset_size - val_split - test_split: dataset_size - test_split]
    test_indices = indices[dataset_size - test_split: dataset_size]

    torch.manual_seed(1)
    # Creating data samplers and loaders:
    train_sampler = SubsetRandomSampler(train_indices)
    valid_sampler = SubsetRandomSampler(val_indices)
    test_sampler = SubsetRandomSampler(test_indices)

    if fn:
        train_loader = DataLoader(dataset, batch_size=batch_size, sampler=train_sampler, pin_memory=pin_memory,
                                  collate_fn=fn)
        val_loader = DataLoader(dataset, batch_size=batch_size, sampler=valid_sampler, pin_memory=pin_memory,
                                collate_fn=fn)
        test_loader = DataLoader(dataset, batch_size=batch_size, sampler=test_sampler, pin_memory=pin_memory,
                                 collate_fn=fn)
    else:
        train_loader = DataLoader(dataset, batch_size=batch_size, sampler=train_sampler, pin_memory=pin_memory)
        val_loader = DataLoader(dataset, batch_size=batch_size, sampler=valid_sampler, pin_memory=pin_memory)
        test_loader = DataLoader(dataset, batch_size=batch_size, sampler=test_sampler, pin_memory=pin_memory)
    return train_loader, val_loader, test_loader


class EarlyStopping(object):
    '''
    Class used for early stopping computing, it returns a Boolean value: True when
    the training process need to be stopped False otherwise, initializing the object
    it requires a min_delta, in [0,1], which is the minimum delta to update the best
    loss and to not consider the epoch a bad_epoch
    '''

    def __init__(self, min_delta, patient):
        assert patient > 0
        assert min_delta >= 0
        self.min_delta = min_delta
        self.patient = patient
        self.best = None
        self.n_bad_epochs = 0

    def update(self, val_loss):
        '''
        It requires as input the validation loss which will be compare with the best
        value, if there is no best value it will be set to the actual val_loss
        '''
        if not self.best:
            self.best = val_loss
            # Comparing module
        if (val_loss > self.best * (1 - self.min_delta)):
            self.n_bad_epochs += 1
            if self.n_bad_epochs > self.patient:
                return True
        else:
            self.best = val_loss
            self.n_bad_epochs = 1  # the first is always counted

        return False


def train(net, train_loader, optim, device, es_delta=.01, es_patient=5, num_epochs=None,
          val_loader=None, net_type='vae', lookback=0):
    train_loss = []
    val_loss = []
    if net_type == 'vae' or net_type == 'lstm_vae':
        loss_fn = Loss()
    elif net_type == 'lstm' or net_type == 'tcn':
        loss_fn = nn.MSELoss(reduction="sum")
    es = EarlyStopping(es_delta, es_patient)
    num_epoch = 0
    while True:
        print('Epoch', num_epoch + 1)
        # Training
        current_loss = 0
        net.train()  # Training mode (e.g. enable dropout)
        for sample_batch in tqdm(train_loader):
            # Extract data and move tensors to the selected device
            image_batch = sample_batch.to(device)
            # Eventually clear previous recorded gradients
            optim.zero_grad()
            # Forward pass
            if net_type == 'vae' or net_type == 'lstm_vae':
                output, mu, logvar = net(image_batch)
                # Evaluate loss
                mse, kld = loss_fn(output, image_batch, mu, logvar)
                loss = mse + kld
            elif net_type == 'lstm' or net_type == 'tcn':
                if net_type == 'lstm':
                    output, hidden_layer = net(image_batch)
                else:
                    output = net(image_batch)
                # Evaluate loss
                if lookback > 0:
                    loss = loss_fn(output[:, :-lookback, :], image_batch[:, lookback:, :])
                else:
                    loss = loss_fn(output, image_batch)

            current_loss += loss.data.item()

            # Backward pass
            loss.backward()

            # Update
            optim.step()

        # Keep track of the training loss
        # train_loss.append(loss_fn(conc_out, conc_in, mu_history, logvar_history).data)

        # Validation
        loss, _ = test(net, val_loader, device, net_type=net_type, lookback=lookback)
        val_loss.append(loss)
        train_loss.append(current_loss / len(train_loader.dataset))

        # Print loss
        print('\t Validation loss:', float(loss))

        # Exit statement using early stopping
        if es.update(val_loss[-1]):
            break
        # Exit statement using the number of epochs, if it is set
        if num_epochs and num_epoch == num_epochs:
            break
        num_epoch += 1

    return train_loss, val_loss


def test(net, test_loader, device, net_type='vae', lookback=0):
    ### Test
    if net_type == 'vae' or net_type == 'lstm_vae':
        loss_fn = Loss()
    elif net_type == 'lstm':
        loss_fn = nn.MSELoss(reduction="sum")
        # loss_fn = nn.MSELoss(reduction='elementwise_mean')
    net.eval()  # Evaluation mode (e.g. disable dropout)
    infer_time = 0
    val_loss = 0
    with torch.no_grad():  # No need to track the gradients
        for sample_batch in test_loader:
            image_batch = sample_batch.to(device)
            # Forward pass
            #
            start = time()

            if net_type == 'vae' or net_type == 'lstm_vae':
                output, mu, logvar = net(image_batch)
                # Evaluate loss
                mse, kld = loss_fn(output, image_batch, mu, logvar)
                loss = mse + kld
            elif net_type == 'lstm' or net_type == 'tcn':
                if net_type == 'lstm':
                    output, hidden_layer = net(image_batch)
                else:
                    output = net(image_batch)
                # Evaluate loss
                if lookback > 0:
                    loss = loss_fn(output[:, :-lookback, :], image_batch[:, lookback:, :])
                else:
                    loss = loss_fn(output, image_batch)

            infer_time += time() - start
            val_loss += loss.data.item()

    return val_loss / len(test_loader.dataset), infer_time
