from torch.utils.data import Dataset
import torch
from tqdm import tqdm
import numpy as np
from torch.nn.functional import pad



class SimpleImageDataset(Dataset):

    def __init__(self, id_matrix, transform=None, fp16 = False):
        if fp16:
            tensor = torch.HalfTensor
        else:
            tensor = torch.FloatTensor
        self.data = tensor(id_matrix).view(-1, id_matrix.shape[2], id_matrix.shape[3], id_matrix.shape[4])
        for index, image in tqdm(enumerate(self.data)):
            self.data[index] /= torch.max(torch.abs(image))

        self.transform = transform

    def __len__(self):
        return self.data.size(0)

    def __getitem__(self, idx):

        image = self.data[idx]
        # Apply transformation (if defined during initialization)
        if self.transform:
            image = self.transform(image)

        return image

    def to(self, device):
        self.data.to(device)

class SimpleDataset(Dataset):

    def __init__(self, id_matrix, transform=None, fp16 = False):
        if fp16:
            tensor = torch.HalfTensor
        else:
            tensor = torch.FloatTensor
        self.data = tensor(id_matrix).unsqueeze(1)

        for index, image in tqdm(enumerate(self.data)):
            self.data[index] /= torch.max(torch.abs(image))

        self.transform = transform

    def __len__(self):
        return self.data.size(0)

    def __getitem__(self, idx):

        image = self.data[idx]
        # Apply transformation (if defined during initialization)
        if self.transform:
            image = self.transform(image)

        return image

    def to(self, device):
        self.data.to(device)
############################################################################à
class RNNDataset(Dataset):

    def __init__(self, id_matrix, transform=None, fp16 = False):
        if fp16:
            tensor = torch.HalfTensor
        else:
            tensor = torch.FloatTensor
        self.data = tensor(id_matrix)

        for index, image in tqdm(enumerate(self.data)):
            self.data[index] /= torch.max(torch.abs(image))

        self.transform = transform

    def __len__(self):
        return self.data.size(0)

    def __getitem__(self, idx):

        image = self.data[idx]
        # Apply transformation (if defined during initialization)
        if self.transform:
            image = self.transform(image)

        return image

    def to(self, device):
        self.data.to(device)

class RNNDataset_arrhythmia(Dataset):

    def __init__(self, data, transform=None, fp16 = False):
        if fp16:
            self.tensor = torch.HalfTensor
        else:
            self.tensor = torch.FloatTensor
        self.segmented = False

        self.data = []
        self.places = []
        self.labels = []
        for d in tqdm(data):
            self.data.append(d[0])
            self.places.append(d[1])
            self.labels.append(d[2])
        self.data = self.tensor(np.array(self.data))

        for index, image in tqdm(enumerate(self.data)):
            self.data[index] /= torch.max(torch.abs(image))

        self.transform = transform

        self.training = False

    def set_training(self, value):
        self.training = value

    def segment(self, before = 0.3, after = 0.4, fs = 250):

        before *= fs
        after *= fs
        length = int(before+after)

        temp_data = []
        temp_label = []
        temp_place = []
        # for eache 10s segment I extract every single beats using notations
        for ten_sec, places, labels in zip(self.data, self.places, self.labels):
            for place, label in zip(places, labels):
                segment = ten_sec[place - int(before):place + int(after)].numpy()

                if len(segment.shape) < 2:
                    segment = segment.unsqueeze(1)
                if len(segment) < length:
                    to_pad = length - len(segment)
                    offset = 0
                    if to_pad % 2:
                        offset = 1
                    segment = np.pad(segment, [(int(to_pad / 2), int(to_pad / 2) + offset), (0, 0)], 'constant',
                                     constant_values=(0,))
                normalized = segment.T
                for count, lead in enumerate(normalized):
                    normalized[count] = lead / np.max(np.abs(lead))
                segment = self.tensor(normalized.T)
                    # segment = pad(segment, (int(to_pad / 2), int(to_pad / 2) + offset, 0, 0))
                segment[segment != segment] = 0
                temp_data.append(segment)
                temp_label.append(label)
                temp_place.append(before)

        self.segmented = True
        self.data = temp_data
        self.labels = temp_label
        self.places = temp_place


    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):

        image = self.data[idx]
        place = self.places[idx]
        label = self.labels[idx]
        # Apply transformation (if defined during initialization)
        if self.transform:
            image = self.transform(image)
        if self.training:
            return image
        return image, place, label

    def to(self, device):
        self.data.to(device)