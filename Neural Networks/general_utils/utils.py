import numpy as np
from matplotlib import pyplot as plt
from biosppy.signals.ecg import hamilton_segmenter

from sklearn.manifold import TSNE
from sklearn.decomposition import TruncatedSVD
from random import randint


def rolling_window(a, window):
    shape = a.shape[:-1] + (a.shape[-1] - window + 1, window)
    strides = a.strides + (a.strides[-1],)
    return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def make_batch(l, group_size):
    """
    :param l:           list
    :param group_size:  size of each group
    :return:            Yields successive group-sized lists from l.
    """
    for i in np.arange(0, len(l), group_size):
        yield l[i:i + group_size]

def get_sequences(seq, min_length = 0):
    if len(seq) > 0:
        # return continuous sequences of a array with "hole"
        sequences = []
        sequence = []
        for couple in rolling_window(seq, 2):
            if couple[1] - couple[0] > 1:
                if len(sequence) > min_length:
                    sequences.append(np.array(sequence))
                sequence = []
            else:
                sequence.append(couple[0])
        if len(sequence) > min_length:
            sequences.append(np.array(sequence))
        return sequences
    else:
        return seq


def f_beta_score(beta, true_positive, false_negative, false_positive):
    score = (1 + beta * beta) * true_positive
    score /= (1 + beta * beta) * true_positive + beta * beta * false_negative + false_positive
    return score

def segmenter(ten_sec, fs = 250, plot = False, before = 0.3, after = 0.4):
    single_lead = ten_sec[:,0]
    length = int((before + after) * fs)
    r_peaks = hamilton_segmenter(single_lead, fs)[0]
    if plot:
        plt.figure()
        plt.plot(single_lead)
        for r_peak in r_peaks:
            plt.axvline(r_peak)
        plt.show()

    # beat length
    before *= fs
    after *= fs

    template = []
    for r_peak in r_peaks:
        segment = ten_sec[r_peak - int(before):r_peak + int(after)]
        if len(segment) < length:
            to_pad = length - len(segment)
            offset = 0
            if to_pad % 2:
                offset = 1
            segment = np.pad(segment, [(int(to_pad / 2), int(to_pad / 2) + offset), (0, 0)], 'constant', constant_values=(0,))
        template.append(segment)

    if plot:
        plt.figure()
        for t in template:
            plt.plot(t)
        plt.show()

    return np.array(template)


def plot_clustering_matplotlib(z_run, labels):

    labels = labels[:z_run.shape[0]] # because of weird batch_size

    hex_colors = []
    for _ in np.unique(labels):
        hex_colors.append('#%06X' % randint(0, 0xFFFFFF))
    color = set(labels)
    color = list(range(len(color)))
    colors = [hex_colors[int(i)] for i in color]

    z_run_pca = TruncatedSVD(n_components=3).fit_transform(z_run)
    z_run_tsne = TSNE(perplexity=80, min_grad_norm=1E-12, n_iter=3000).fit_transform(z_run)

    plt.scatter(z_run_pca[:, 0], z_run_pca[:, 1], c=colors, marker='*', linewidths=0)
    plt.title('PCA on z_run')

    plt.show()

    plt.scatter(z_run_tsne[:, 0], z_run_tsne[:, 1], c=colors, marker='*', linewidths=0)
    plt.title('tSNE on z_run')
    plt.show()
