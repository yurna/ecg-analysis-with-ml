import numpy as np
import pywt
from scipy.signal import butter, filtfilt
from matplotlib import pyplot as plt


def butter_bandpass(lowcut, highcut, fs, order=5):
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# %% md

## Denoising function
### Bandpass + Wavelet denoising
#### RigSURE soft thresholding (risk minimization)

# %%

def trim_axs(axs, N):
    """little helper to massage the axs list to have correct length..."""
    axs = axs.flat
    for ax in axs[N:]:
        ax.remove()
    return axs[:N]


# thresholding
def get_th(coeff, raw, scale=0.5):
    # squared
    squared = coeff ** 2
    # sort from little to big
    squared.sort()
    # sum value
    s = np.sum(squared)
    risk = []
    # risk computation
    for count, value in enumerate(squared):
        r = len(squared) - 2 * (count + 1) + (len(squared) - (count + 1)) * value + s
        r /= len(squared)
        risk.append(r)
    # standard deviation
    std = np.std(raw)
    # select the coefficient with less risk
    selected_coeff = np.sqrt(squared[np.argmin(risk)])
    # threshold
    th = scale * std * selected_coeff
    return th

def heursure(x):
    l = len(x)
    hth = np.sqrt(2 * np.log(l))

    # get the norm of x
    normsqr = np.dot(x, x)
    eta = 1.0 * (normsqr - l) / l
    crit = (np.math.log(l, 2) ** 1.5) / np.sqrt(l)

    if eta < crit:
        th = hth
    else:
        sx2 = x ** 2
        sx2.sort()
        cumsumsx2 = np.sum(sx2)
        risks = []
        for i in np.xrange(0, l):
            risks.append((l - 2 * (i + 1) + (cumsumsx2[i] + (l - 1 - i) * sx2[i])) / l)
        mini = np.argmin(risks)

        rth = np.sqrt(sx2[mini])
        th = min(hth, rth)

    return th


def denoise(raw, type='sym6', level=6, scale=1, plot=False, range_freq=[1, 60], fs=500, heur = False, discarded = 2):
    w = pywt.Wavelet(type)
    filtered = butter_bandpass_filter(raw, range_freq[0], range_freq[1], fs, order=6)
    coeffs = pywt.wavedec(filtered, w, level=level)

    if plot:
        fig, axs = plt.subplots(len(coeffs), 2)
        axs = trim_axs(axs, len(coeffs) * 2)

    th_coeffs = []
    for count, coeff in enumerate(coeffs):
        if scale > 0:
            if heur:
                th = heursure(coeff)
            else:
                th = get_th(coeff, raw, scale=scale)
            # thresholded = pywt.threshold(coeff, th, 'hard')
            # thresholded = pywt.threshold(coeff, th, 'soft')
            thresholded = pywt.threshold_firm(coeff, th, 2*th)
        else:
            thresholded = coeff
        th_coeffs.append(thresholded)
        if plot:
            axs[2 * count].stem(coeff, use_line_collection=True)
            axs[2 * count + 1].stem(thresholded, use_line_collection=True)

    if plot:
        plt.draw()
    # drop last two wavelet: 500:250, 250:125
    if discarded > 0:
        th_coeffs = th_coeffs[:-discarded] + discarded * [None]

    denoised = pywt.waverec(th_coeffs, w)

    return np.array(denoised , dtype = 'float32')
