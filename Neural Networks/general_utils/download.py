import urllib.request
import os
import sys
from zipfile import ZipFile
from path import Path


def download(data_url, dest_directory = Path('dataset')):
    # make dataset directory if not present

    database_name = data_url.split('/')[-2]
    dest_directory = dest_directory / database_name

    if not os.path.exists(dest_directory):
        os.makedirs(dest_directory)

    # select the last part of the dataurl (the file name)
    filename = data_url.split('/')[-1]
    filepath = dest_directory / filename

    # program the download and extraction if the file doesn't exists
    if not filepath.exists():
        def progress(count, block_size, total_size):
            sys.stdout.write(
                '\r>> Downloading %s %.1f%%' %
                (filename, float(count * block_size) / float(total_size) * 100.0))
            sys.stdout.flush()

        try:
            myProxy = urllib.request.ProxyHandler({'http': 'proxy.reply.it:8080'})
            openProxy = urllib.request.build_opener(myProxy)
            filepath, _ = urllib.request.urlretrieve(data_url, filepath, progress)
        except Exception as e:
            print(e)

    if not dest_directory.dirs():
        with ZipFile(filepath, 'r') as zip:
            # extracting all the files
            print('\nExtracting all the files now...')
            zip.extractall(path=dest_directory)
            print('Done!')
    else:
        print('Already downloaded')

    return dest_directory.dirs()[0]