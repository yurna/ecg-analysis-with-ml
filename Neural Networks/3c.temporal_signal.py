from pytorch_utils import get_loaders, train
from pytorch_utils import RNN_first, RNNDataset, RNN_all_lstm
from general_utils import get_th, rolling_window, get_sequences, make_batch, f_beta_score
from general_utils import pretty_plot_confusion_matrix, trim_axs
import pickle
import torch
import torch.backends.cudnn as cudnn
from pathlib import Path
from matplotlib import pyplot as plt
import numpy as np
from tqdm import tqdm
import pandas as pd

if torch.cuda.is_available():
    device = torch.device("cuda")
    cudnn.enabled = True
    cudnn.benchmark = True
else:
    device = torch.device("cpu")

weights_folder = Path('weights')
if not weights_folder.exists():
    weights_folder.mkdir()
weights = list(weights_folder.glob('*.weights'))
# %%

path = Path('data')
stride = 4
window_length = 10
file_name = '{}-{}_seq_stp.pkl'.format(window_length, stride)

with open(path / file_name, "rb") as f:
    _, arrhythmic = pickle.load(f)

arrhythmic = np.array(arrhythmic)

data_test = []
places_test = []
labels_test = []
for d in tqdm(arrhythmic):
    data_test.append(d[0] / np.max(d[0]))
    places_test.append(d[1])
    labels_test.append(d[2])
data_test = torch.FloatTensor(np.array(data_test))

file_name = '{}-{}_seq_ptb.pkl'.format(window_length, stride)

with open(path / file_name, "rb") as f:
    data = pickle.load(f)

rnn_dataset = RNNDataset(data)
train_loader, val_loader, _ = get_loaders(rnn_dataset, batch_size=256, pin_memory=True,
                                          test_split_percentage=0)

####################################################
# LSTM
in_out = 8
middle_layer_size = 64
n_layers = 4

lookback = 5
print('Predict {} seconds'.format(lookback / 250))

all_lstm = True

if all_lstm:
    name = 'lstm'
else:
    name = 'lstm_dense'

name += '_in{}_mid{}x{}_lb{}'.format(in_out, middle_layer_size, n_layers, lookback)
if all_lstm:
    net = RNN_all_lstm(in_out, middle_layer_size, n_layers, in_out, dropout_prob=0.3)
else:
    net = RNN_first(in_out, middle_layer_size, n_layers, in_out, dropout_prob=0.3)

net.to(device)
# if torch.cuda.device_count() > 1:
net = torch.nn.DataParallel(net, dim=1)

found = np.array([file.stem == name for file in weights])

if found.any():
    print('##### - Loading weights - #####')
    net.load_state_dict(torch.load(weights[found.argmax()]))
else:
    print('##### - Training net - #####')
    lr = 1e-3
    optims = [torch.optim.Adam(net.parameters(), lr=lr)]  # , torch.optim.SGD(net.parameters(), lr=1e-4)]
    losses = []
    for optim in optims:
        print('Using {} as optimizer'.format(str(optim.__class__).split('.')[3]))
        losses.append(train(net=net, train_loader=train_loader,
                            val_loader=val_loader, device=device,
                            optim=optim, net_type='lstm', lookback=lookback))

    plt.figure()
    plt.plot(losses[0][0])
    plt.plot(losses[0][1])
    plt.show()

    torch.save(net.state_dict(), weights_folder / '{}.weights'.format(name))

################################################################à
# some results of healthy prediction
net.eval()
n_row = 10
for batch in val_loader:
    signals = batch[:n_row]
    with torch.no_grad():
        predicted = net(signals.to(device))[0].cpu()

    signals = signals[:, lookback:, :].numpy()
    predicted = predicted[:, :-lookback, :].numpy()
    break

fig, axs = plt.subplots(n_row, 2)
for count in range(n_row):
    axs[count, 0].plot(signals[count, :, 0], label='real')
    axs[count, 0].plot(predicted[count, :, 0], label='rnn')
    axs[count, 0].legend(loc="upper right")
    axs[count, 1].plot(np.abs(signals[count, :, 0] - predicted[count, :, 0]))
plt.show()

##########################################àà
fig, axs = plt.subplots(5,2)
axs = trim_axs(axs, 10)
for ax in axs:
    r_i = np.random.randint(len(data_test))

    signal, place, label = data_test[r_i], places_test[r_i], labels_test[r_i]

    with torch.no_grad():
        predicted = net(signal.unsqueeze(0).to(device))[0].cpu().numpy().squeeze()[:-lookback]

    signal = signal.numpy()

    '''
    fig, axs = plt.subplots(8, 1)
    for ax, s, pre in zip(axs, signal.T, predicted.T):
        ax.plot(s[lookback:])
        ax.plot(pre)

        for p, d in zip(place, label):
            if d == 'N':
                ax.axvline(p, color='green')
            else:
                ax.axvline(p, color='red')
    plt.show()
    '''
    diff_mae = np.abs(signal[lookback:] - predicted).sum(axis = 1)
    diff_mse = ((signal[lookback:] - predicted)**2).mean(axis = 1)
    '''
    plt.figure()
    plt.plot(diff_mse / np.max(diff_mse))
    plt.plot(diff_mae/np.max(diff_mae))
    plt.show()
    '''
    w = 80
    sums = [np.trapz(window) for window in rolling_window(diff_mse, w)]
    result = np.pad(sums, (int(w / 2), 0), 'constant', constant_values=(0,)) / np.max(sums)
    result[result < get_th(result, result, 2)] = 0

    ax.plot(result, color='red', label='error')
    ax.plot(signal[lookback:, 0], label='real')
    ax.plot(predicted[:, 0], label='predicted')
    for p, d in zip(place, label):
        if d == 'N':
            ax.axvline(p, color='green')
        else:
            ax.axvline(p, color='red')
    ax.legend()
plt.show()

###########
# Testing


net.eval()

batch_size = 512

true_positive = 0
false_positive = 0
true_negative = 0
false_negative = 0

window = 60
scale = 2
dynamic_threshold = True
fixed_threshold = 0.5
plot = False
confidence = 10

count = 0
for batch in zip(make_batch(data_test, batch_size), make_batch(places_test, batch_size),
                 make_batch(labels_test, batch_size)):
    count += 1
    print('{}/{}'.format(count, int(len(data_test) / batch_size) + 1))
    samples, places, labels = batch
    samples.to(device)
    with torch.no_grad():
        predicted = net(samples)[0]

    predicted = predicted.cpu().numpy()[:, :-lookback, :]
    samples = samples.cpu().numpy()[:, lookback:, :]

    diff_mse = ((samples - predicted)**2).mean(axis = 2)
    diff_mse /= np.max(diff_mse)

    sums = [np.trapz(window) for window in rolling_window(diff_mse, window)]
    results = np.pad(sums, [(0, 0), (int(window / 2), int(window / 2))], 'constant', constant_values=(0,))

    for index, result in enumerate(results):
        results[index] /= np.max(result)

    if dynamic_threshold:
        for index, result in enumerate(tqdm(results)):
            result[result < get_th(result, result, scale)] = 0
            results[index] = result
    else:
        results[results < fixed_threshold] = 0

    # find where the error is greater then 0
    x, y = np.where(results)

    for i in range(len(samples)):
        values = y[x == i]
        place = places[i]
        label = labels[i]
        sequences = get_sequences(values, min_length=30)
        # control each continuous subsequence
        counted = []
        fp = 0
        tp = 0
        for seq in sequences:
            # control the type of beat
            for p, l in zip(place, label):
                # optimization through element removal
                # "pad" the sequence to add confidence interval -> maybe the peak is not in the anomaly
                findings = np.where(np.arange(seq[0]-confidence, seq[-1]+confidence) == p)[0]
                # if index found controls if it's a true positive or a false positive
                if findings.any():
                    if l == 'N':
                        fp += 1
                    else:
                        tp += 1
                    counted.append(l)
                    # break means only one beat for each anomaly, but if heartbeat is too high this hp doesn't hold
                    #break

        # true negative are all the N labels not counted as false positive
        tn = len(np.where(np.array(label) == 'N')[0]) - fp
        # false negative are all the not N labels not counted as true positive
        fn = len(np.where(np.array(label) != 'N')[0]) - tp

        if plot and ((fn > 1 and fp > 1) or (fn > 2) or (fp > 2)):
            plt.figure()
            plt.plot(samples[i, :, 1], label='true')
            plt.plot(predicted[i, :, 1], label='predicted')
            plt.plot(results[i], label='error')
            for p, d in zip(place, label):
                if d == 'N':
                    plt.axvline(p, color='green')
                else:
                    plt.axvline(p, color='red')
            plt.legend()
            plt.draw()

        true_positive += tp
        false_positive += fp
        true_negative += tn
        false_negative += fn

    print('true positive: {}, false positive : {}\nfalse negative: {}, true negative : {}'.format(true_positive,
                                                                                                  false_positive,
                                                                                                  false_negative,
                                                                                                  true_negative))


tot = sum([true_positive, false_positive, true_negative, false_negative])
assert tot == sum([len(i) for i in labels_test])


type_1 = false_positive / (false_positive + true_positive)  # alfa
type_2 = false_negative / (false_negative + true_negative)  # beta

precision = 1 - type_1
recall = 1-type_2

f_1 = f_beta_score(1, true_positive, false_negative, false_positive)  # armonic mean of precision and recall
f_2 = f_beta_score(2, true_positive, false_negative, false_positive)  # placing more emphasis on false negatives

print('precision: {:2.2f}%\nrecall: {:2.2f}%\n'
      'f_1: {:.4f}\nf_2: {:.4f}\n'.format(precision * 100, recall * 100, f_1, f_2))

cm = np.array([[true_positive, false_negative],[false_positive, true_negative]])
df_cm = pd.DataFrame(cm, index = ['anomaly', 'normal'], columns = ['anomaly', 'normal'])
pretty_plot_confusion_matrix(df_cm)


