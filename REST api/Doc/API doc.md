
# API token server



## Indices

* [Resources](#resources)

  * [Send scp](#1-send-scp)
  * [Get anomaly interval](#2-get-anomaly-interval)

* [TEST](#test)

  * [[TEST] Get resource](#1-[test]-get-resource)
  * [[TEST] User registration](#2-[test]-user-registration)

* [Token management](#token-management)

  * [Logout](#1-logout)
  * [Login](#2-login)
  * [Refresh token](#3-refresh-token)
  * [Renew service tokens and keys](#4-renew-service-tokens-and-keys)


--------


## Resources
API endpoints per l'accesso alle funzionalità



### 1. Send scp


Endpoint protetto accessibile con il token di accesso, richiede un json con i campi payload e id. Il primo deve contenere l'scp in base64 mentre il secondo l'id associato alla misura.


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://localhost:5000/send_scp
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Body:***

```js        
{
	"payload" : "S78eiQAAUKoAAJIAAAANDVNDUEVDRwAAkgAAAAcAAAABAPIBAACZAAAAAgASAAAAiwIAAAMAWgAAAJ0CAAAEABYAAAD3AgAABQCqBwAADQMAAAYAvncAALcKAAAHADIAAAB1ggAACAC0AAAAp4IAAAkAAAAAAAAAAAAKAPQEAABbgwAACwAAAAAAAAAAAIAA0AAAAE+IAAAOYAEA8gEAAA0NAAAAAAAAAAkAUEFaSUVOVEUAAQsAQ0FSRElPTElORQACBAA2MDMABAMAGAABBQQAywcEGwYDAAAAAAcDAAAAAAgBAAIJAQAADmcAAAAAAAAAAABIRHBsAAANIAfwAQAAAAAAAAAAAAAAAAAAAAAHMjguNi41ADAwNjYxOTA1AHRvdWNoRUNHLDMuNDMuNzc3OS4AU0NQMjAwMCA0LjUuMC4wAENhcmRpb2xpbmUgU3BBAA9OAAAAAAAAAAAAAAAAAAAADSAH8AAAAAAAAAAAAAAAAAAAAAAAAioAMDY2OGU4YmJhYWNhNGNmYTg3NwAqAFNDUDIwMDAgNC41LjAuMAAqABIRAENhcmRpb2xpbmUgRGVwdC4AFhIAbS5kdWV8TUVESUNPX1RFU1QAGAEAABkEAOMHBx0aAwAMMxsbAgBDABwCAJYAHQEACtKXAFJpdG1vIHNpbnVzYWxlCnJTcicoVjEpIC0gcHJvYmFiaWxlIHZhcmlhbnRlIG5lbGxhIG5vcm1hCkVDRyBuZWxsYSBub3JtYQoKKioqIEFUVEVOWklPTkU6IExBIFFVQUxJVMOAIERFTEwnRUNHIFBVTycgSU5GTFVFTlpBUkUgTCdJTlRFUlBSRVRBWklPTkUgKioqCgD/AAAZdgIAEgAAAA0NAAAAAAAAH05LMwMAWgAAAA0NAAAAAAAACEQBAAAAiBMAAAEBAAAAiBMAAAIBAAAAiBMAAAMBAAAAiBMAAAQBAAAAiBMAAAUBAAAAiBMAAAYBAAAAiBMAAAcBAAAAiBMAAAgAPAQAFgAAAA0NAAAAAAAA6AO3AAAAib8FAKoHAAANDQAAAAAAAMQJ0AcCACABIAHKAN4A5ADkAOgA7ADznn75O/n1rN4ldzPXmptxv57nqPnsffM9me+c3fZ+W+5lh7l8u+P9n/BO97l5zud7559/fnM7ual83J9jfzftzzPt5m+5Xl57u15zu5x3ufN9eLPcye3Oa8/cVknNk/Pz75fvnfvJ/3h/3t/wVnz9/u/3z7/3T9/4L8/4O/4P/4J5v8/Obz+/969/PffL1v5uahs9zxti7ib5dObLr57vM93yTPovk3snn6+PU7n83++Ze3/N/fOX7nJ/Z5z/b815Nv7z595d3N8b9vJ3nu882z9n8778u5LXIos/Pd3M+f7ufl27+b3yfuHnvfmdZvueXs5rNt+b7znd4vebxPcm87NmVZm49vMnd3PLPZMvrJ5vdnHWZ3JXUzn7l35t38//g7/g3z/ecc7bKyyryfZFZrlqbcltxl98WTpcx9nl7mdlkvm/Xnn2ybnl+z8n974zatzM95ne5Le9k47zn3vHnb8vV5l97z5p3k1JbLFxn3JubI2e9/kmc/4Pf8Gf8Ff55P+E/P+9P+8/+Cp/2v/2T/3n+/9o/97/8H/8Ff8Ff8Jf8H/Pz/g3/gn+fLv57/d5+vzvfzZebNYfvnmtzO1ecfsk+f2En3PJ3uSTtw52zvOX2fN9rPx/cmb3/O6BnZzP3uZEd7+P7z533k2Xtyd56vlm9nm7yfb+W5n15LbOO7mOs5uay95ayzt549s6/Pb5LrWTrxuXbeS78vu/Ob3XnP7yb755PszdqTmfZXOozfc8d7nJ3G5bLlue8nr/P///gn/gmaY1LMu4thK3JrFWAqSdZS4tZrj2ZS5NNyN41zqeXqbm2bL8ffK1Vjci5dYjvJTzt28GbZYEtiJ7gRc5nJ739fP37+/Pzf95f9/4J/J8/4J/n3n/Bt/7159/7x/7z/Z8595+fnmzU240QGyJrlt3kahYCwsuOzNSy2TPb5aQuzLYKE2JsCNnN7wI3jeS5V5RYsdkbJ1i65VS3LZcXLubADNqRublsvNUkWaSd5N7zLpJuOuKTY3CbCKrO5l7GTrKmstctpn//4Q/4QmsOxLk7lFktzZWNiVOwyXYua46k2+Xb5pcKrmpOzGzl7lLZNklk7Wbc7fNi5uzlncLmO2uJPZl3AVMXYXLxP5OfMu+fnf7/3rP+Dv+8v9+d/7wz/hP3/u7/vf/u7/vHfe+f8H/8Q/8H/8FzP5z3bf77e7nslIuTsu5lLlazHuWKiWpnUSdY2WS2Z7PNtyya3y7LLmxbNyXZNTbzN7OduTYlhmr5TbJWbcbGzLOuG4sbM2IbmxNxsk3cSaFzcnSWQ2zNYuzl7CXG3CyxRJ2JKVKx1jPcE24E0vn/4M/4NdwzqzDcWxLZZOzjsLFlkWwlsjuTb49xNlyaAx1x3OPYLc2JneW25e87JNvLa4dyXeZs7ZJJ7hWbE25MvZZcSMPPnc/z8c75/wj/3r+f8Gb/3i/4R/70/7g/7k/7a/7g/7x/7z/v/B//Cv/GX/DP/Bn+eeed9b7+t2Vy1zUsqzKt42uJ7lSxl2zGyWG82xFuNkzqTctvmlRZNLJqNvLs5fvJbmglvNtyWzedknXFNyLvljYlmzKJbh3lLk1WG5VZY1KjLtc1k9ZzvcRvl3ci5bLYk24Jtybcm3I1EdYGdqc/4P/4QtSWiSooLLjZGnOyasyXUlI3eS3l2TSY02edc2s1i1I6kd8bxvZzdluZ1jVyO5GvHs2SRqTqJmtzDqESrvv85fn+fmf8G/P+Cf+De/8Hf8I7/3i/4P/7p/7Q/7b/7Q/7T/7v/7u7/wv/wr/xx/w1/wX/wb/M5tt99urZ+W3NSubcmm8lrJfcSbhdl5awm3FSq5dnj1M2WyKS5sK3Mu3m3l7nN7Jbcs2Gxt3iW2GLvM7FkTXFqSy3KjLZRcu5Je4lsAS0iG1gl75NtZNk3Zyy2LZczplRqa8u3E1CmKm3K5//4M/4MVGdmy8UJbJsTs5dlys6syrJc6xrHXj3ItSLYlZak2cvUs1BiydvHU2yLlV5t3lL5tqMLvLrA3El1EXNt7984/P+CfP5/wb/wd/n/BV/4V/4Q/f+9Ofz/uD/sn/uD/t3/tH/uz/u+f8Of8J/8Tf8L/5/wh/wTnyavvWlnhYbGbJnW5mxWXYk2I2zNzc3G42S3nWNvxtys0y2WWTebmouyVL3mXWbWbS2W1cqtyuVGVbkjsy5rN5bcEWxUncm8buGe5K3GW0Rmzbiub3yXsM2KZalktSbTyncm5XccuyWbcqZWif//+C/+CpUk6S8rc2Fllxsy6SWVuJbCXeVcm75fZ5VsjbjNsmyTuY7BsF4sl7mds7JKubZku8t7OS23iLc0Ms7Ix7lzcl2e9/fmZ8/z5/P+EP+Df5/M/4V/4L/3+85/29/2V/3P/3P/2z/3Z/3dn/Dn/B3/Dv/B/+f8Hf8F58zK93bc18VJak1k1ZI28PZEVJaktzcz1msqaye553WGWpO5YsEbkdk7i2c3b524t3PebZuuKu4ZpmdkuSzs5uakO4SE1Zsli8dMs7gslCyybCWzPWZeyXNxqTWWxYlWySzszuZeyS2Cbmwud5rn//F/IGAL53AAANDQAAAAAAAMQJ0AcCAL4WABRWDHoMFg0ODfAM9gz/k//kX+f8F/9183z/grfsfz93Jvfnx/3l8/4Q/702eT+/8Ez/f7/wk/7s/P+D+f93bn+Nnn3ue/8I/95e38fP+Cf+9P3/gr7655fZm7+f758f3+dv34/7y/4S/4L/7z/71/zm/3P+EP+9pLP+9fzd/4K+f723+f5/3hvE8/nev+9/5+f97/f+DZ/3r58/71+cz3c3/grz/u384mf97s+f8Hf91/91/8ST/vH/Pfcfz3/vT/gz/gv/vL5d/4J/7t/f+Cf+Ffv/e3u8/zz9z/vT+b+fP+9fPvf+Ds/72/7y/z/gvn7f+C/+8f3nz/g7/vf+9/4Ln/ev8/4L/73/7z8/4J/c75f+C5/3t9/N7z/gr/u9nz7n/BN/v8n/ex549u+/nPJ/3nv/Bd/737/n/Bt/72+/8Hf7+5/wb/3rfn93znz/fv5/Oe/97efn/dvz/hLf7Z+P+7v+Cr/Nfuf8Gf96c/P+8t/4O/P+88+/8I/f+9M/L+/3/hH+88/71v+Xffl/z/vHznv/BN/Z/Pn/c/n/Cft7++f7/wf/wT/3dl8/73x/wdn2d3+r/wS/71/4O/z3++f8KZ/3l/v57+f95/n8/7j8/4Q+773z/vT++f929/4N/7y/4X/4O/4P+5/OfnfP+9P+6f8/4R9/73/z39/X/BM+fvyf97zn8/7wvP+Dfvr/gv/vX/vP/gz+eP+7/8755fP+9/z/vJ/wZc7/fn5d/s/4M7/u5/wn+/94X/gv8/713ybM9/fz/gn8/70nz/eX3/vf/gr/hH+/39fn/Bv/d9/4M/P+9f+7/+Dfn5/f+7/+Df+D/+8fv83N3v/Bjn/eW9+f8E+/94f5nvP8z/f+8/n/Bn/BP/efZ+f97+f5/3hP+Dd7PnfO39+P+DO/92f8Gf8Ff91fz/gv9n3z3/gr9nM97zn/evM/4L5/3v/3p/m/97f8Jc/7xz8l3zPvcv8977/wXP98/bybn/BPv7J/wV/v/eXv/Cf/B3/c//BPO+f3/P32zz1/P+C/9/7yz/hX/vb/uj/g75/3v/J5Pnfvds+VPP3Pdz/gv/fn/e/PPv8v/duf8L/8Gf9x3/gyfrfP7+b3n/BfP+8Zz7uff+FN/fvfP9v/DPz/un+/8Fb5+f967587f7v/CV/71+/zx+zzn+/7/wV/Pb/l/7wn8/4Kn/dm5/wd/P+8O/8F3tIRRYAAAAAAAAPv+bNPf+9/+Df+Cuf979/70+fP+Dv9/7zn/Be/97/n/CX/d3+/zz/O/vv9/4Of96/lszE2d5Y/71/P8vn/eL5/3v/P+DP7/fn/BX/ebz7/wj/3r/3rz/gx/Zz/u3/hItlQmfnfu/nM/3/P57/wZ+/95z/gn/f5/3t/3b/wTz9/z/vH893v/d/998/4J/4V/4O+fz7n/Bvn/eXe/e/8J/95/95f8E3/vH/g7/grf+9eZ/v+P+Cef93+/8GT95N+f3/vX/PPn9/nz/vf9fnv/BN/P+7P8/n/evM/4L/713PN/vOZz3fn/env/B//Cf/cMz/g377/wj/3p/efn99/4K/P+8n+b6/P+DP+8E+8+P+DP+5/n/Bn/evLP+Cff+7+f8G7/nP+6ef8F5+/96f8Ez6/4J/28/Zv/BL7d/4L/3+f97p5vPP7zvn8+/3mv+Dfu/3f83u/P+Fv+7d5/f+Cf7/v/DX/e//d8/nz/gr3/vH5cn7/wXP+8u/8FfLzv7/P+8vz/gznv/ev/e3/Bs9/z1L8fe/v/Bn/Bv/dLnnz3/vff+DL/wZ9/729/JfP+8+f8GP9n8/72+feb/PP3/vR/wf9+dyf97e/8Ffs/N58/73/73n/C/7/3f+f5P7+/8E53+/HnP+7Of8Ff8IT/ut8d/4K/73/f+Cvv/BPn/e3/eu/nz/g3d/vv5/3j/wh7ni/96+T/g7/u3+/8L/93v+E/+Df+5dc7z+f7n/BX/et/z3/vZz/hN/3b+f5/v/en/Bm/f8557/3rz/gz/Zfvz/vf+f8FT+/95v+Dv+Dv+7/+9f+DP53/vT/P5/3j4+Y/7w/n8/7x/4Zn/eT/vD/hP/hD/vD/vP+cv/Bv/e1/3/gn/gn/vZ63/PO794fn3+/8E/P+93PP7Hzf+9/+Ct/735/wTf3+fPf+7/+C/85/3r/v8k/nP+8/ff+F/+Df+5P+8P+D/+Dv+9r3/vf/g//hf/ub/vfn/BWZ/3t+e+/cn/Bmz+98/4J7/3rn/BX3+55v8+f97/n375e+f96/8KT/vXv8/v9/4K/4L/n/duO/8MfP+8/z/ve/8Fff75+f90f96/z/hGf93/97/8Hfvv/d3/dvn/B37/wV/wv/wd8/vz/gz/gv/d/7x9+3/P9/nfv7P+G/v/dub/nn38/71+fX7/wf/wV/3X/3v/wd/P3l/72/4L+f97f97+f8Fe/H3/L/3r7+N/n3P+Efv/dX58357nff+Df2/P+8s/4a/73/7r/4J/4Q/7s3/gnI+/3/hD8/f+7t/4J/ybfP7l/c/4Jv/eu/8J/n/eGfvf5vJnf+8s/4P7+/3n8/f+C9/7u8/4K97/wV/n9/7w3/gyc/389v7/wT8/Xn98+/D+fs/z/vP5/n/e+Pd/4K/c427/f58z9/f+Cv+C/+9v+7P+FM3/vT3/hbP+6f55Pz/vX7/nN+58+c/7y8/P7/3n/wjz99/z+7zyd5++c/vz/b/wZ+/8Fft/7y+f8JT/u+8/Pc++e5z/g7/uz7/wl/3l88zn/df+f8F/f3/vf/hr7+99yf8If97397zx9vy/8Ez/vX+/8E7k5P7Z58+93fnf+Dp/3U/n+5/wp3/vf5/O/96f3P8/z/fP5/3r/3f/wu77/ef8HZ/3v/vz8f95/3/hH/gv76v/e/zxuWZ/3t9/4Jzy6/vP89k/70/4K4/71/mfv/BP7v57v5fff+Dn+v3P8/72+f5/3f/nPz7X3j/gv3/vX7/PPzz/vHk/fl8/v7/P+C/+Dv+7/9n/e/+fz5v/e3f+C8/73/4J/f+8P+Efn2fc+b/3v/wp/f+7f+C87/wX/Xmf7/3vx/wT+fn/ev/e/zv/B3f+8/+9v8/4T8/7y/71/4Lnfv+b95/wj7/3fqfN87y/v/BX/Bf/e//ek7/wV/nf7+/zPv/BPP+7c8efPt8e79/4K/4Q/7z/73/n8/4Lv/eP/Bv/ev8/4Q/7rT7/f+8/+D/sz3/vX/vf/g33/uX/vD/g3/hf/gyf8Ff5zPzf+C5/3h/3pn+f94/P8/7w/4Nf96/z5/3jP+Fv3+tzZ5vfF9/c/nz/uz/g//g3/vf78+8nf958vx8/71/7x/4M/z35Pd/n++/zfyf969/P+D+f9zfP5/u/8E/P3v5nzn/dHnmf8IP+9v79/Oe/5vP+8uc+O/L/3Z/wZ/wjv/e3v955/n/efnH93/hD/P+4PP+Cfsv/Bs/7v78/P6/z/P7338/3P+CfO7v+/zef5/3oz/g//vK/8Gfv/eH/Bnzdn/Bd/7x/v+b/wX7/v/Bt9/v85/f5P+97+ef96Pzz9/Pz33PvvzP9/ieffZM/4J/37k+ff7/wXf2f4+/8F/96X7f+DHv75/N/efvn/BP+8z/XPMz/va/O/cz/gu/7/3t/wTx/wd/3r/3n/P+Cf97z/vfzz/PP+9uduf955/w3v/d3/e3/BPHnv8z/vdd+ez/gnu5/3l3/g7zfz+/r5vs/4Q/v/e357f4/7y/4KmS79/4J378+f7/vz/gr5/3b9/4P/4J/7x/Pz/u/8/4J/vj+z/Nz/vf35/Pfb/wb/f+7f+D/8/n9j/ff+C/+9u/J+Z7+/f+Ct+f3v/Bnvn/e+TP3/hD9/3fnf7/Pn7/n5/3jk3+/P+CvzP+6X/B/v7/n/BP/e3u/b/P+Cc/3/vb/gzfvv/CPO9/72/fz/hDv/ef8fv/Bvv+3+fefe+fP+DP+9v1/wS/7m/4N/4a/7r/v/BXXJ398+fv+d/71z/P5/3v/m/38/73+z/g7v/en/Bnn7d/n/e8837/wpv/en/emf8H3/vP/gn/hT/vL9+v5z3/vf/P5+/96/8Lfv+e/z7/3f/w7/wX/3H/wVz/vX87/3n/3r/O/Pz/vef953+f9y/7/wX/wb38/4P/P+Cf+D/25tt/73/728/4Pe/96/93f8K/8F/3739/4Q9/70/4L2fn3fz/vTz8599/7z/4N/4Q/7y/v/B//e35+47/Pzv/dv/Bv/BP/d35/n/evP+Ef+9/7ebv+P778/4N/7v+/8L/3fu3nnz/vX/P3Pn78bt5/Hv753/vT/g7v/e7/h33/u/v/Bu/f7P535z/vK/P+C+f94f75/wV9nvvmX/hL/vH3/vKf8O/79f96f8Ff8Ff97e/nfPnef96e+c/4P/P+8P+7f+Cv+EO/8H/93f96f8F/8Ff97/uT8f7zk683/ef8E9c/f7n+c/cv/Bf3/vLz8f75d/4N33/vL/gn/gvf7+f97+Xn775/wn/v/eHzv/Bfn33d53+fn9/71/4Q35/vuev+DX/eHz3/gvn9+/f+Cf+Cf+9O7+f8Ff8Gf95f968/3/gv+b9/z/f+98/4J+f95/M/71/4W/7395/f8+v+DP+8N/n/Bm/94/8E/7/u/8E/8LfP+7/7vz+/P+Cv+957zX5/wX/3fOzz5f78/35nefO/97z/hX/vH/vHz/g3/P3/vf/f5/wZ/f+Df+9ft/m/v3/g773/g7/vX2T/hH/f+9f+Df3f7/3t/wd/wf3/vL88z/vN/wX7/3q/4Un/em9/M/73/z/gz/u2/8Hf93f8E/8E/97/8Ff3/vX/g/9u/M5z/vS7z/NL9/70/4L/4Jv+5/j/uf/hf/hb/uP/vL/hnP+9vu/8Fd/3/P83n/eXj/vaf8HZ8/v9/4L/70/7z/4Q+R3Ofv9/4J/u/8I/93ff+DfL/3v5/wj/f+7+5/wXv/B/7/3Y8/f+Fv+Cv+3f+Dp85/3j/j988n/e//B9/7uv/Bnufx/3n5/nP+8fn/Bn/e//ev5z4/3/g/ff+DP77/l/nP+8f+9/8/nv/ev/d3/BfP+FP+8/+7v+9/zv5/23n8/4N/4f/4J/7078/4L/4M9+/P8/7y/7zny68/2/8Hf94f5/P3/vXzPv+fd/4Q9/7z3/g6b/3v8v+f7snc/4K377/3t/wh/n/endnz/vf8/4Kn2zf+87/ww/7y5/wX/v93+c/f7/wZ/3tv/CH/B//d39s43/Pvyc+/94f8FfzP+7/P+Dv7/3t/wb5/3pv/BPz9/ufn3fzz7f+9+/z/gv+/s33/P57/3n/wT/wT/3d9/4Tnvv77/wo/7tf8GfP3/vb/hH5/3f/3r/wf/wjf+8u+c/7z+/8Hf8E8/7s+f5783n/ev3+ef8En/e1/3+T/vL/huf97/357P+EP+8f+9P+Dfk/u/n5/3bf+E7/3b/wZ/wh/3R+f8Ff957z53/gn77OfP9x7nv/BP28v/e3/Bnn7/3t/wdzt/d/3/gv89/z/vL8/n/e2/5/wV/3t/3r/Pn/en7/wl/wT/3l/3t/wT+duef2/8F/v/eH/BWPj/vX+N9/yff+CZ/3l/j/vWf8Ev+DP+8vV+f8Kf94/3nvP+9P+Cf+Ef3/vL+R8/7v8/4Kc9/3z5H33n53J/3tf+Cv+Cv+8P+9P5/wn/3vs+s/f+EP+Cv+9P+8M/4Nvf+9/Pj3/gr/vf7/n+f3/f55/3u3zmf7/wr/3t/3r/nP+8f5/wT/3p/3v/wb5y/97f8FXdzu7v/BXn/en/e//Bn/B/f+9fs+Zm/97/xff+Dvf935z+/8G8/7of8Iz/c9v/BWf93/8J+9fTzPn99r58/fz/fyf96/M/fz/gv/vH/vbn/Bn/Bv3/vKbv5/wd/3nvP+9Z/wT/wV3/vH7/ws/7s/4Km4+7/wh7/3b/we3K/n/enP+CX/d3N/4Q/4U/7a5/wb9/nf+9v+Fe/96b/wZnz8/73v/e3/BvN/7z+f97/93/8F+f8F/7/3ff9/4O/z/tf/vP/hf8z/hH/hb3/f+DOSf8Ff3/u7v/Bvv/eH/Bfu/8Hd/7s/4L+fv/BX3++dnl+d/u/8Ge/7/wl7/3p/3t/wtv/ev8Zf+9fz/grf+98/72/4J/4Of92c8z5v+/PL73z5++/l/n/e0/4K/7zn/Bf/ef/BO/v5Y/73/n/B3/BP/dv/ev+bzzO/97fz8/ubn/eX/Bn8/P+9/39/4K/4M/7r3/gqnfI992TL/L7/3X/wt8/7un/BP/BP/e/ds5/wd/3v2b/wT9/70/4J+eX/f5f+9f8l8/735+XP+8/58/fl+fu/f8/z/f7f5/wd/3b7/P+C/7/X/Bv3/vH/gn5J+/3/grzd+P9Tf5P+8Z/wTuf8F7/3l7n/B373n8/71z+/8Gaz+f94+/n/BP/e3/e//Bf+/n/Cf/e3/dv/CH/CP/eP/dk/4NfP+Cv+9/+8d/4N+b/3r/m8z/vT+fL373/hN/3nf7/wTz75Jz/vT/Ofu5/ef5/wZ/3r/3p/wX/3p/nv/Bfn/ev3+/8IfP3/gn/u/f+EN/7v/l8/M/3/f+DeP+8/5frz/gn+97n85/3t3+Pb87z+ffn/env/DX/eX/dv/Bzvf+DN/3/gv77P8/70/4N/72/v/BX/BP/en897P+Ct/727/wf8/7vrn3J/nz/uj/hLnP+Cv+5Xz/f8847n3Zv+ef92f8Pf97f339/4L+fs+w/4M/3Hf+8f+Dv+Dv91/3lP+Db9v59/4Nf73+3/hLn9/729/H/B/v/dP8/4Lz/vP/P+C/7/3h/wp/wX/3Lf5nnP99z8/4K/7z/t/4Kv3PLPn9/v/Bf/e/7/wU9n/Bff+9vf+Cf+DM/7z/738/O+v+Cv57/3d5n/B3f+8v5+/55z/vbzl/v8eX/gq9/x/v3/vX/g7/gv/uv/uv/g7iff+9+/8Hf90/93P+Cf+9f+Ev+If+Cc/71/zmc/n7/3t/3r+b5/3X/wV5fn/et/4Z/71/71/z8/738v/ef/BU/4K7/3j/nUPmf7/f+Cf873s/fzn1/wV6/35/m/96eefv3/gmbv/Br/uT/g7/g//vfv5/3t55773/h3/vf/uP/gz/g77/3t9/4e/73/7r/4M/n/eXnz+c/7y/72/4P/P9v8/7v7/wl8/71/P+9f4fP+8P8nfP9/4Mnv7/3Z/wd/n5+/93f8F/8J/94/f8/vnP7/3v/n/BX7/l7/3r+e/jn/CP/ef/ejz8+P+904/7z3/hL8/739z5Lvfv/Bf335n9/71/n/BP5/wT/3r/3fn/Cfz/uzv+P87/3r+fm3vn/evn+dvuf7/n+cv9/f+9f+Cv+D39/3/Of96/5/fP598/39/k/4J2X/uz8/4J5P+9b5/wTvf+9/+DO/7/wT59/3/Off7/we/733/PP+9/+88/4N7/wVz/vSc3f2f8I3/vH9/4J/zv/ez/hb+/95/fz/gn335/v+ef97efP95/wT7/3r7/H5vz+3n78/n/en5NuPP+9vlv78/4K/v5/r/gz/vf9vnP7zP8/4J+/96f93/8E/8F+/8Ff97b/wf/3p8/70/M3Pf8378/v+R7n7/3t/wb/wZ7/3nz/J/3lz/gvvf3/gvv78/4J/v/d//BXfv/B//Bffv7/c8fj/V8+d/7z/4On3P75/wT++fYz/vf87n/BP/d+fn8+2/P+9vP5335/f+Cv99/4J7/3r/wt5/3T+f8G/949/4K5P3/vb/g/8/f+7/3/hD89/L/3n/n+f397/wT/3v/wVP2/z9/3/hH+/93f8Jf8Ff95+X7+dvfPl8/3/g79/7s8/nP+R/+O/+DZ/3v95/OTvv3/gv/XJPc+be/n+/5nu/l9zfn2/Pm/95zf+E3/eX+ff785z/gr/vG9/z98/z7+zfnPfP77/wd9+rP5+/z9Tf+C/f+8d/4M/z/ve/4/7z35/wn/3r/s3+Z718/P+97fzP3t+/n8fP375vL9+/8GdvJ/3r/wT8/r38f8F/97f95f8F/n978l/fP+E/+6X/CH7/3r5Pze/P7b/wZ/3vPw/70/v+f8Hz/vKv59/nn+/eeXn3z93zn7OZb55/3nvzz+f79z5+/3/g/n79lM5+/8Gc/3+/yv9/4J999/y/Oe+/PZ5/b/Prznv3z57/3r/H8/f75/wZ8/73++Pv7/wf28/vf5/3v/wT5/3q/z8/vPX/eH8/4V/f3yd7f+Cv+9PN3uf8HZ/3v9/fn85733/gzv/eOTnn3ef8Hf96/96/5/P2+/8IffP+9Oc/v579/4N/n/evfzf+9P+Cf5/d3/hH9/70znne8/4P/3/vf+fWd9/l/e/PPD/gz83/g3+fz/g38/4M/1/v/BXf+0/+x/+9L/2d/3t/wj/wZ/wXz/hb/gv+f5/wZ+dv/BX/B3/eX/de/8KZ/3p/wT+/ZvzOR++/M8/7zn8/4J/fr/g//vT/vL+fz839nv583/fiv+9Z/wh8/7yv+P33/Of96c8z/O/96549mfbz+fs8+/8Ffv+/y5z/vX3/g7/P+9vfvPn87/v/BO/Z+/n8/73/v/Bn5/3l4vPfef8F9+/3+T2XX9/zN7d+c/n/eu/mf97/38vz+c/725+fu349/4Jv/enn8/73v5/n76mfJ9257/n338fnPv/enPfnzPf9/4O5/3r7/ns85/v383n+e+/78z8+/efz+3+Wd3/vX/g7/gz/uP/g7/gv/vX38/n373fO/PPz778/2Xz3/f+CuTf597z++z+fN++fc43vv/BPN/73z+fP+7/+9P+Iv+DP+5v7+f5/3vM+f96Z/wT+7/wVn/eH8+ffe5/wcv/en89/vz/hLv/dn3/g7Z/wV+7M/73zHJz97+/8FX/fz+e/96f5/wZ9/P7793/gn/grf+8n3/gn/gz/vS3fwtlQlS2CyKLAAAAAAM5/Z8/Xf+CfH795PJ37v8f97fP+D/+9v+7/5/wd/P+9Ob/3d/wU+/8Eoiy8/72/4J9nz/vZzz+f79+XeP9f8E+ev3/g3v93M9n8/7x/4KhKlsfOfvvv/BP0/O/8F/8G/nzn/Dfv5vPf3nf+y/+0d/7u/7N/n/DX58/4V/4N/P8/4M/PP+Cp73/e/z5/3e/z93/g757/3t8n9+b/wTv7fyf75lP7/wYn/e15n+/z59/n/e38z/vX9/P+D/fn9/71/MXmp+f77+v+Ef+Cf+5/j/hL+/97/8Fe/3yxnn+3/F/728/4O/717+ey/8F8/7t/f+C8k/7y/P8753/vT/g7/hH/uj+/n/B//e3/ev+Z/r/gz++49fJ+7/j9+fz3978znvPs/PZ7/f+Cv+Cv2f7+/8H3/vXv/Bv/Bf/eXfz7/P+95/wj/3t/t/N/4K9/3mZ/vn/Bf/e/+/ncv88/1v3+f8Ff7/cz7/wXz/c/73n/B39/fnl9/72/4Pv3k/7255P+Dvv/def8G8b/u/n3+/8G/8E/3P+9vnvP8v/enf+C0/vf+DH++7/n5Pfzf+9v7/wV/wT/fcTfL7+Z/3vnz/vf+/5/wrv/d/zveef3fz/vf/gzPuX++fnvO53+/8GPZzv+5/P5/3ty/94+f8J/8E/907/wbv+3/P6/4J91m7n89+/z2/v559+X9/zv7+X597P+9v5/wk/71/vfz/g39/735nfv+c/Zfmef94f7/wV8/4K/39P+Cv+Cf+7zufz+zl/Pfn/em/J7n/BOz/vT8/4K/v/en/BjP389uTc2Xc+Ps++cvnu35/b/wjz/ur/vb/gn/hr9/7s7/Pn/e8n/e//Bv/Cn/dv9c/z1n9b/i5f+9p/POd/72/4M37+/nz2v+Cvv/e3z8r/vL/hSf96bvzvn83/vT+/8E/P+Df9f8Lf97/8G/8If8E7z/hPn/en/eP/BX/BP/aX/Y3ff+x/+9/+Df+Cf+Ef8/4P/z/gv/hTzv/Bn/BP93v1v38n/ev8fv/DW/93tv/BV5/3tzZf53vnnv+/mb5e/97/5/wV/Xs7/wV/d/Ln9l/4U/f+6vznI899/n+7/wf/3p/f+GfP+4H/Bnm89ye+/8HX/vf+/8E5fO7ff+Cfc95f35/Pz/vPucd7nOfe/56/70/Pz7Pn/ev88/71f8GT7/3pnz+fn/eD/gnf+9uf8FS/33+X/N/ZfP+Cf+8t8/Nn73/gvZPcva5n68+c/72+b5/3r/3v/wv5/3X+c/M/7078z39+f8E8/n/e//e2dz+c9/73/4Kn/e2T/g3371+ff9/L5d/zP9k5/3j/wT8z/vfPx/3vvz/hD/vL9/z/vf5nflss/f+DN+73f5/wZ/f9/72/P+Dvz/u/3+fP3+d9vnfOef95d/z75/wT/3v+e/97/M/n+++Tr/hL/vf/vb+fPf6/P3n3n/BXfvz2+ff3z/H/e355v3/f+EP5/f97+c98n/e9+ZnP9/f5jy/73+bze/8Hf97Yv2f8Ff7vN5n3vz/P93h/f8n97/P+DPv/e2ez8ef73efP+Cn/efvzs/nfv1/wl/wV/3bf3f+C5fn7/vz+X9+b/f8/P+9f+9/+Dv5/3vn/Bn/e3/evz+dnP+9vPz/fvyX8/4K+/vd+Z/3p/3nn/Cv8f94+Xu+fv4/y++7P+Dv+9/9+f8E/97+/n/BN/7137/n/B/v/efxnff+C/+9uYX3PM7f+9/5/wp/3b/3n/wb4d/4T/73/4b/4a/7zz+/8L/8J/92f95/8F+f9uf9jf93f7/2v/3l/wn8z/iX/g3/vX/gr/hP/hLZ3559/vf8/3cyfP7n5/3r/f+Ffn/dn5y78f2y/nn79+fP+9P+D5/3v89++Od78fuf8F/v+/8Fd/vP508/f+9/n/Bf3357/wTz/vb9/4J77f5f7/wTx33k/fzmz/vfn57v85/3n+/z5nXP+9vP+Dff+9tf8Fezfefsf8HT/u38+e/95/z8/Pv/evz+b/3tf+DM/7z/z/gn3+/ntvJ524/35/wX+/3y/97/8G7/wT7/3t/P5/3g/Kzz5/f7P5vPP+88PP+9vn/Bvf+9vPz9/73+f55/u/Huyvn/e3z/hP/vb/vW8/4J+/35fv/CE/39/n953878f95/nJe/f5/L9879ny+f7XL2fzn3vPu/8Mf97/93/El89y8l/73/P77/wb/wr/f+8P7Oc5v/BPf+9fh99/z/gnv/e/Pc/OfdZ5/3p/wS/71/4L/4J/7z3N/j9t/zz9/fnz/vfv/BOZ/v3/gn8/7uf8K9/7158/72n/BP68fv+d/Y8tzfv5/3r+f8H8/f+9+bf5v37PGfN3/f+C9/v873+PuX/g//vP9z/gn/vf7z5fz/P+7b+fn/e08/z8/7r7/wZPfOfv+/zP7f85n/e353fv+f8FXf+9v2f8Hfz/vT9/n5n/en78/4L/tzv+/5/wl/3r/rP8/7uz/hr/vX/fefnv7vnH7zNuf5+/z7+/z+4/4K/f+8f88f532yZ/v/CP79/71z/g7/vf+/8J/8F/94/3P3/gy/7M+r/wV/f+Ef+9f7/wT/f+F/nv/CX/BX58/4O/Of92f7+f90f9lf90b/2z/3J/wf/wd/wd/wh/wTzP+DP+EP+Fvz/vX2WP+9v+9v+DP+Dff7P358z/va3/gqf3+f97Z/PbHz97eX/hH7/3hP8fnv+x55+/v/Bf/Bn/dm/8G/95+/8I+9/n/e/nP3l78z93/g39+up8/4K/7x/Oz/P9zfPd3/gnP9z83/vb5P9v/C/v/ds/4NzP+9f3/Ij+9/n5/3v7z5/3tP57eP8/v/e3Z/wv/3v779/z/gn9/72+fz1vmd/72z/g//gn/vT/vL8z8/4N/7w/70/z/gyf3/vfn/B3+/97fn5P3zn+u/5NvWb8/fl/4N7/3ru/8E+/3fP+EN/70+/8FfP7/f+Ce8nr+5/wr/f+8nv/Bn+fv7O/ef8GfP+8/eeZn/ev4efqv8z++3z+ff9z+T/P3/vJ+fD7zY557Pv7/wX/3vz/P3OfdmY9+fz/vb/vfX/B//BX/e31e/8E8/73ybfv8/4M/v7/3r8/nOdf978+dvM/72/P+DP+898v579/Ob/3j/wbdz/N/7z3/hD9/v897+Pz39/4O7/3nnf+Cf89/715j/fz+b+/f+D/u8795/wV/3vn9v/BK/f5P73/gt9/f88/v5+9fmb+zzvf+Cr3/f52/7/wx/wf/3D7/Pl/737/wc/72/PPP6nfvz/g//vLsv5/3sz+fzf3/f+Efu/d3537z53++f8GP+7+c/4R/713Pf359/4O/4O/7lmf8Fef95+/L+fn/e3+/8Ie/v8v33/gr/P+9f+9v+Cv+Cf+7vP+C/v98/4J39/fN+Z/3t/wX/P8/4Q/4O/v5/P+C/+GM/7h/70/4T+/9lf9mb/3n/2n/wVz/g3+f8M/8Nf3f+DfP+DP+DN/s/4J/71/f539/4J73zN/71/4L8f97+e7/wV/3rz/hH+/vv85/vvnv+f8Gf3/ev55/dZnz/avn/e3z/gv8/3v7HP8v7Xf+Cr/3tnz/b4/z/g7/vH235+/3/hL3eTv7vv/B/u+Te59v/Bnn+/975879/P8/vsbzz+2ff5+f7+8/4Q/7z/73/4L8/37nnzP+6/5/wZty/rv8/4Qf946ndzybnn3zf5f+953/gr/f8379/j/vL/hfu/P+8e+f8J/97f95/8FS7z0v+T/vDz/hf/u79/k2d8/n9e++fyz7vf+Dv+9t/H+8y9/z8z+/7fe/P+D+f96Xzu5/wV/vc77n/BV/7258/z3/vD/P57nc+2OfP+9fj33/g//P+8f5v+/N/3555va75/wb/vue+f97c/4Lv/eP/BXnZ/Z/wTt99v/BXL79xz5v/ev8+f7Hnz/vfv+fv9/z7/wtv/e//efnz81/3o/Pv/BT+/8E39T/gv/vf/vf/OO/7/wp/3v/3j/wZ53v59+/5v3zn+f77Zkn3f+C/7/3t/wX/wb/3u/2vl7f+C+/3v/Cf7/3t8Xf+EH/cl/4Rz/vf575/wT/b/wT/fd/4Jf977/wT/n/efvHmv53O+v+Cv99+X/f+9/z/g73c9358/vN/72/Pn5z/vL9/4V/P+6ufzf9897/wVn3+dbf+Cuf96f3/Pn/e3P+9/+EP8/7wvy8t5/3lPnPnn/BvZ/wd/n/BX/Bf/eP/Cn/B3/en/d3/B3/B3/an/YH/d38/7Q/7q/4Wz/g7/g//g/+f8Kfx+fN3/gt+1++/57+/8Hv+85/wTefc+5c3n9+fz9+5/wl9/7v7/wfn/ef/BPr/P+8fz/gr+zz/vSfnL7vzy/7nOX0lc5/3p+f8Gf9598m8t/f+DPz/vB/P+FP+7/+8/nPlvvfy7yZ/3v/j8/3/vO/8K/8E/90b/wj/v/ef8/mZ3/crPfcz+f96/8GP+8c/jcn/e1/4N/4J/3+8+/meznn99/M/fJ3+ffv/BOe96/4Jz/bkv/BN/7t/4L/v/BX/B//dv7/nn/evnf+EvP+6H8nvn8+++efu3n7n88/73+/Ob+Z/fb/L75z+/Pts/4N/P+6ufh98/7w/P+Fv9/7v/4R/4L/7w/s/mfvx3fn8/3/vbnk7P573/gn+9/4M+/u7/PL3fz2c31n3nHn/d//CE/7z/P+Dv+88v3/gnf35Oyfvnfnz+/X/B0+/ZvZy3jnzP+8d/nfc/4L+/f5Pz9/73/4M/3/vX/gu/v/Bv5+/97+b+/8Fv+9vM/PuNyd/vzc/zzv+/3j5/n39+/8E587+379/4L+a/fF+5/wZ/n/df/CP7/3n+/8GZ/c/4O/32b/wT/eZ+5t/L+p/L9nL/wT7/vP+9fn/Ct/70/39/z/gn/P+9/+8f+C/v78/4N3+7/wv+/91/5u8+5P78/4Lv/ef7/mZz/Z/jvv/BXv7/n9/r/g3n/e2/z/Pv+yZ9v/BNf5/3rm525/3t/whu5+f7f7/f+Cv8u8/4R/z/gnn/BXvz/iHP+5P7/wn/3t/3F/2j/3Df+z/+9/+EPz/g//hD/g7/gn3/hT/g/8v7/P+Cvvv/evfeefz/vbzz/vO/8O/96/7zZ+f97e/5v/B33/vT/g7/vW+e9/lz/vb+f8Fe7+zv8+bs+528c3fnc/738z8z/u78/4Q+/u38n/e27/wzn/dPzNz7/wl7/3f88/vPPj/vR/wZn3b9+/8G/nf3+v57/Pvz7fn+/8E/3nP+7v+DMP+9f7/wtf93Mz3u/yc9t3J/3v/wn+f95/975nz/gn9/fmfvv5z3ktmzJ+/nP+9XM/72/4J/n9/M/7xn/CH+567/nP9+8/Pvx/3r+eZ9749z/Hf3/f+Cf+CvX/e9/Pn23nc75d/l3v9/Ob5z+/97Z/wT83eb9/f+Db+/P3Pk/vyz37M/P+Dv+8/+9P+DM/73Z9/4Sv/d/v/BP/Bn/e37/wT/Pv+/s/4M9Rz/vf/O6vnn99+X/gr+98ndne/xn3+e+N9N/Gf2/8F/v955z/vZn/B8/7x9v/BP+f94f8Gv99f5LZ/uf8E+/fJ/n31P79/n/Bv+9mZ7yd/O/1fHO9+5/wV/wZ3/f7J/c/4Mz/vXIl/35/wT/b3z/hD/vf97fOdnvNm7n+X/vT/grz7L7X/BXb/wZ/3Z9/4Vz/vLn/Bn/dnf+Ds+8987599+fn+z5/3e/4O7d+dvn+7/wT97/wh/3t/3j/wf/wj/v/e/PZ9z8ez8/flj/vb7/7U/7W8s9YXb5feG3Z5LW1/nr1cyG+7/wXn+5vZzM/uZnbzXZybYzdy8nTsfM/73mZcl/s8/zf+9Xm/L3HztX2cnb8qdlTb5qef3Pz36/Pm/963/g3328X35/P+9vzz/vb38/O3snn3O7+Vd8+5nvmfZzZZsS3ZOX1+TdbPPufP3Xip3fmP3z8/fZOdvL98vmes95n7IjN7+X3zP3HZnz+35nfufO7OP38z3Yv+e3+/P+Cb/Wdfj1O/X/Bfd3xn77jkne+ef37+fn9fM9e/8Fdfcy59eT7+Z+/3+fPrnn7sTO55+75v3/M9x45k/f+9s+b/3r9z/g7f+775/3v/wi78/4N/4Ks/4K8/70v7/3h/3t/c/4L7/f8/4Om7l7z7MtnfnuWZfZnO9n/BPv+8ksL8998c79znnvv8esn+/k7y7lszZr59kvH+z/Pcn95nub53ye/Z5+fbm7mdRO5znfs5Jufm/eL75ym9zz3t/M+t/L9nvPO89v5f+9/PLOzfI9nvEZXc/n393/grez355/3vPLuefdeee2dy+X/fPnt8z3uWfP+9v8z2c77LfidiZ/3rz/g3f+9n5n242Tnvfydb8vZ3JW8zO/7/wX5/3v1+feZd43c5+5j3v8y33+ftz+d+9/NuQsuT/vef8F993+dt3z+5/E97wzu/5/t/yb/bkTiXbWWZZdCQoubEstgsihZKlsLOffPn/e2Scf97+fN5XvfOe383/ve/58/7m/4Xn+83fkNlln9/G+S95uW+b98m9/JO7Jfs/m7vAAfzvs8/M+2/97c8/73/71f8I5/3r/3l/wb/Xn5+fP+DH3/g3/hD/vL/vP8/70/7z/70/f+C/57v5z8zfPvvPZ5d3nPu/5P+95may59yZZrvNyey+effvzPXE2N57fzu9z5n/e1z/gzf3K2yN8W7mzc4+3zN9v52WTPP+9J8/Pu7+Nuzz5205OpPvO/8F+/t53OX+/jd98+X3HfH7zLt83Nzz7PfNzbmTuT9zztt8mzt/N0v831Xl28z+v+Cb+ye+WfH/ennjn3OysnO7e+f5v77mc9fOfvtzzz7fLv9/4Jz7J38/bknb88/72z5tT3nOt38e85/YyZv7zkz1nbJHtz+c/71v47clTvfzn7PnP3tScz2pNt/PZNyT2ez+bb1zPfzdnfm/uZ8/ffzzefXbyPN7Z+fe/z77z52ez/grf+9ecvv5b5vvl9/Oe7Od+/y95/vz+Pv3/J/3v78+e7wtsnOtzLnfOvzf3jtzJn+/55/d+X+yfFuR9k3Jdz4+bNf97v+Cv+8/v7/nnf9+P+9v8/y8/4P/4J/72/P+Dd/71m/92f95938/Lvz/PNl59+/O7/L94vl93LyT7z8n/e/vJzn3eMbs984vefvl/M/715zuSztk8+/f8/n/e/t/4Jt7b8357+z5Ps5rvJOzZ8t/Wf8E/3NllkTvHzbazI3vnePd/Ne+Z39fk5P3fvnju/ldsw6ny7N7ic72Y3+Z/v3+Z6ce+/MZP9mcu89zndv5u5m73km+/xc9t8Wznuyclvb8/fnz/ZN4++R+/5N/Zn5z/vf9/Mezy2/O37/Hd8zX955P38z6nO9vyyfP908TXc8r98Tbzf5f63xuc72P5r/fJM2e1jnPu+d3OWpacduefe/x/3v4/z974T75Z95PnfSXM3b/Pn/e+7mN8+fveX755/3v+f8E/3vzf78+dshf3yd35597+V9Xzf6/z5P+9/l7m2LzJLm/38vZMztszPbmP75yWnebKZ/hnf9289u787/3v/L/3l/wh/LP+C/+Cf6/4X+/94/8Ffv/df/eV3yd/P+CeYnbee5Nq/8E7/3t58utuEx2T7/wT+9+XvJN3y+38veNvv8z3H7zz+/mdnud5Jdi8dz3PP1559tvf87s8vY2Tj1PO98neV81b89s8dnH9Tz7kzZ953Jn1vz3yT6z983/gnfXPvm9mc7vOd9zPvI52Zv3knZ5n73zN3Gzvn+ff9yeOzZyb3vz+fu7+b+/K8v78x9hvJcv2fz7JZX7n/Bc/73szu/Ge785/ub38/J+7bx3494zvZk9nF5f3k8/73+c/Z/wT+9nO755/r5v3+ef3vy1f536/zf3JYOT7fO2TUm/PX476m8mfszme7b4159QzN9zjtTnn9zvL3fz8/exL4/738/n3eY3qyZ5+9+bnnvvP8v93zO95f75/LuznZ57vZcsRe/LPvL8azm777nM8enZOdx7+X3n8/723J5fZ5vv5nz+f7/3vn3zfv+9/4Mj9/4K/73+/P5/wVn8z7/n5/3s37/3p/3l+/n+f97fc/4M8/OfXzf383bm5n2ZOyJ7n5+vvPPz99yM7s5zur8/b+b9/4Jv789uTfH3fzcub7nnefpxXKuzn9vnJ3b8596+Zdqp+e9Tnm7b+XXF7OeX/vePMn33nl64/718+c37N/nP995zO9z+d7U51qZm9uePdmeb9k8n+zMv35/H/e1xzd8nfj77PObvnevjq+Htkm83Od7y2Z2Z3fO8nrmdtmXvxvL9yXN9/zn3+8+T9fLvL2SVeT67888/v3yy5vv5zveX38ffku75L9/fn8ld7fmpNktnPt5l/P33Jdzjubfj37+fn3pz59+8z2eZ/b87PNi7U86lXPbP+Cn/e3ns+bvj6znfm1J7rzfsnzlf7/m3rvv5812Z3n3PnP3+/Pn7k/zP+9fvnzJ/cl33f8fPP+99mH9/mfufPbPz7v8fL/e3Zz3/vX44n3+/8Ff8FT5zzef8F/7953/vf/u7u38/b/n/BN+fPv9+c2N9mS+feT9z87t5G7+/P+Cf6SWdfPbzYd4892+dzt/Hu/5fqbOWc/sS7v8vuZb3PPP97yb89vzP2zl75YrXxvn7n83e/8Fd9nPfeZz9uedeff78zv+fu/Nbav87veT87X3nvzO+8z7/nt/P9efP7vmsu5y7P3P5v3f+CX3vhy/vMqev8/d8qdZkXfqc/J97CS89sct3P59/35zs+/ObsZdue/PP+958bXK757JM7Rc+d/fj3I35nvu+fO7Jo3nn+/5X3c8vee/M97OYv7fJfNXvm/Pezkt+feyTvM787+z8zvXK53s5O8756snv5z9++fzfcrv83/c/M6Z6/O/eZ/Pv7ec9PN+/Mm/3+WX+zyf7P5eT9k7k2TJe+c3+55tbePl/eN/Of7/v5/Nd5bHZPyd/L7vfN9f94+f8G/3/vbz8/7y/z/g7b/wT/wd/3vv/Cz/vCd/71/72/72/v/Bfn2/8E/Oc9zn2e+bpPNdxrz3mf1z5v3m+e/zvvPuZvl7Jfn6yPeG5bjvJ9+fP+9958/eVc8+/s/4L7/c+d6+a98Wex/L/3vn/BW/7nnt8y68/b/wTPZN53nP3fnvPO/vE2/n3zu+5zNsz2zzvufj3s+V7id5z/U/4J37x9Pzn/e88veef2zzt8vvG453J7H3yVyp/uc8vfN3K7f5v9/4Kn751tk5cv0+Xvy+35e/PWvmb/c+d+Tfbb53PP3+T++Z/3v5i9ec+vH7OXt8/O/cnPt3y7nG0+d75+z83nvc57ne/8F2fbWS7PL2bzc3fku87+z/Hud5J335e5k6XzsufP95ez8f975/wT+++TN/eec/2/573nL7m/E97eZ5596vPu+R+/zz9vPnf1u/PZ595Oe+/Nv4+zlPn35/ev5Pucn7fne85z99/lubeX9+c+5mTk593/X5/e/v88+/7zf3/g4/n+f8E/97/8Ff8F3/vT57/3V/3l3n/BO/fP+Cvn+f39zfm98e5+fv+/8Ge7zqX873nufO7LZltvyzZbc57t/nfv857/fmarzn72Yrf55/b98uZrm/s85/r5nPfe/N5OfbfPfLbPz2W42+fH+/Pnv742f8E/7J3c0zv/BXf37nMm9+dsyd3Es2+b9+c2X7zz6/Pt5vzd9zxdlST3Gz8/d+LvvPy9df5+y2cufbyN5uf34dnzN+3k/f5P7PM3n7fz1m/fz/P99/4J77vz3yfe/8FZ/uf5/3n3/OfvzPc2ffl3x++c/b8nb8dl3fluc+vn7lzWNt+M+47v+d+8j5/cj+/nzr2e/k9vyL9ScXc2zyfe/x/czr8e/efmPu/O+5y3m28z95s57/nvvz3fJ7Ml95uS5q+P3md/Of3vkz19nl7/j3uY/WXz7v+bbuOX2PJ/e/n/BfffeNvJvvztn/Bff+9/v//ib/iLw68veb7/L683PY5G0fzrt2eRXff+DZ/3tm2ZzP2zls3z9fLrmy7jFt7zf5378s9859vef8Fb/3o+Xl7njdZ+55bvirzdlvmnPc83938/L/3rz+b7uTP7+fz/fOf9795+Xt8s9+e98K87ezzz9kU2Ys23k7M53fPsz3z5/3u5mmfuZL38573cz3OX7k5Pb53yf2zmzy/fy6+f7J3mefdzntv87sj75zeyP5/X2f8GX+zn142+e/Z/wTvrM5/eszHvvnPfvzz1419/xffLse+c75n/e+/nO7P+Cf99nOPvPn+s3v8zcnnPPz+e/fn57/3p/3n3/hX/vT/vKc/7y/P+DN/71+/92f97f97/95/n8f8Sf8M/8GWf8Fv+9uefv/efvL7z7PPe5fmyud9vzz1v/BP3+8S5lTfv5vl/czne/jdnP3i82xKc9v5++Xv4/2fzsvuc7LWefe3n8/bn1OdzZPb5879cll/PfZzn28nc3fJfXzu834+3f5t877/l/3nidm8jZ7y5sx2fm/973/Oe39/zP9nlvzP7+/M411Pnf75893zL+82fP9/k0dm7nM6kn/e+f8E+f96zM7vjSbll53jueyXnWc79/4Mz/vX2fz2cdnN95z/fJne5+de/nu5+d7Yvm5pfJ/3tP8fd/4J9q3ufzef283Ob789v5PepESWqgSy2CyKLAAAAAF5cz+3OZf98/O861M/vz53938+f95/8Evd3PmdUk5/q5+dbC8z+ufP3fzeexN7nm7cYKK/M7nn+Zv8n98/4J/72/7w/3/hbP+6P+9/+C+/96/P+Ef+9f+8P+9/+7/+9J3L/wz/xB/wZ5fz/gzv33vv+787+jz7l5z6+X/XzPclnuXMNufec9s58/v38873h3ydjvHTzP97n/Bc+5vS/Pryxuo7/nd9/J3++edWv58/7x7/n5/cvPTXPmts51LOs/4J+/vflmPuSX6/4JdsrnvfPPdy/PufO7snnv7/Odsz7fN57POs25bc7/NvW/y/cyfs/4J++8dl887/vk49vzevN49Wf5f9sznr5n77J8nuc773+efY3L9ny6/l/d/4K+9zb5nc9+/zz95z9MJ7OTN9kWSe9z+Z/d5Pec7Uf3/gnn+5+Xe+8y5LsZ3Xzue8kz66/md+s8+zzTeb+55+f973n57mz65vI95v8/vfnd55d9z/gv7/uZz9yZz3uRJu85/Zx3z/efx7fPP+8/fz/gn395ed1zvztm/nvPvDj75t3lv57Pz9v5v++/P+Cfdnme/78neX87mZ8mfk9/z8/72/73/2f8I/92/3/Pv/eN/4a/3/urf+8P+7F+/P+DP+JP+Df85n5/H/e+3+/bM/f35u/8E70n8+/bMTz+z/N/fc+fPvc2c7lveZnU/v87+Z/3u83Lczd7/n97/wT+f97/ufw9MnN/7285z+8me7MzbbzL/Zz57rNuXi7vPO52Ms5995OO75tvJvb5mXfbP5v3P5/ed3zM92PNS9yd+e+84fj/vfvPz6vjf3nl5n+zJ3HZ/O93+d65evneX/vf/gnXz3r4vuZuy/zd5/Z57nz3ZPP3eeb+/yfdnn5v/e8nz7ZkM7e/jvfyX7vPN7+Pa+e9nxZ59+3Pm8v2ZJe87yfZPPP365Fzbeczf2X4r3m535z93M95mxNtybEz738z/fM+f3fnZncs9nJovfPPfd/4K8/2e559vnPWb9/Of97+fn7Pl/3zze8S3vPn++/mdnxv2d+b/Z+fk/72yXUly4r5v+/4/738znP3ck5/d+cv7yc2a3mbtv+f8FL/l9fOdv7/f+Df+9P7/n/en/ePn/DHv/dH3/u//u79/P2f8L/8Of8Ff8G/xnn7M7/3t2d9st5Y35n/e3znsl7MDcb/n9782LzuJ+z5vfFn35nsdzz7nOt+f7543c7wne+c+vM37N3/N6yew2fne2c7ZOdcn2c3Nkm+8+T/ve+fP9yRPsZM+15eM/Zffl/M+3PvG3zm++H7zz9zJtce5m7M59s83sl7fn/BP3/vfJyessx+5PnuyT3lcv3zj9iuS4/ufnbEd1/wTn+643PO77/j957ef8Fb99l8tzPeauz876nN+b9+e+f35z+5/wT+9ebvj95573+fn/em/LNzm+z52yVNkbzbebsc7+evM/fs4zH6nk1t8nu5uSbOb3M6rmT+/PcbM+f243Pl/718/N14vV3zLd53zPtz/L/e/mfbN7z/N+x8/3n5vvTNyxd+dz78+/y/czx3v7zJ8z9y6c3nb+d/f5+f97b87zz+vy+/nn5/wV+9+X55v/ePv/CL/vXM/7v/7y/4L/4P/7u/7w/7q/70/73v/ev/BP/Bn/Dn/C35/nPn/BO/1XfvvfN++V3mzzqP3yWbzn3d5z+fv94zm3Znbmef1533/H9/z7fNk3u/59+S97h59TM+xm53k+++cO7+Z/fc5x65ss3rmfN3rx7nJPtn5v/e88zjv7nGvJ/3u/mb++e/8E/P+8t8/l+++f8Fd/TFrNwfecz3V/z3T5v1nN9n/BV/7334nr5P3472+ed3x9v8vbcfP157O5fMvvy/eB7nO8dmTbZJ2RjvNvN/v887+5+T73x35TZOzl9zv8x/e+RcnrFy+/5fWVufO/ffzg9m/nu+XebObN9ePx/3t5n2M8/YmN+/Pz71fPO/Z59zmfvfLPnXn295k0udz2P+Dff+9/PufL3x7ZmWW5z+68veJ5O/38Wz9vkjskn7f58/f9+c63/gn8/7y3Pnl+l57+/zzmfurk/fnPvM3XyX+X58bPj/gr3/u7z5/uc/7w/70z/hj/vH/vL/uv/u3/X/enf+C/+Fv+JPn/Bf8cNvn39nv7nXnZWWPeX3z/gr+93ktvvn/BP9reXOzztMXvJnbeWe38nu/5f2c735Z59y13v+dZbPb/Pn/e1vm5ffk33Jnbx3J28jn7Pzev+Cv7vHfePL7nHfn99+Tv/BP3vyzrqfz3Uzj2dz3+Pe8X+b35/3vM/O9y1zdY5/c+brzfuednPe5wvZ+f1zNavOL7sr/gmfvZnc5Zvc7+avOd/3zzufvzntxzfeb7/Pn+ubnvme87lteHSvzf389Tnb8m/ryTd89Y3yf7+ee7ucey8zeuSb/Z8Rc7U/z7/vzj7P+Cvv/e/xncuZ73zJvUy5f3zncnvzrsz35577P533G3/L/3vP5tl8938n1v89tzz+yc39+eS/Yx/Z8f7nzfL2XeTdxk3f+Ct/71zy5fd+T533K8n395/lvcl2I/nm/Ocb/Me/933/hX/uz/vX/grv/eXv/Dff+6/+9f+8v+7/9fvz/gv/iH/hb/hDvvz/gru/a/v/e/Mv7nL1eedz941z3P8/71vP5v7l+ffl17nO8zu4zdnJ+5y2yzPc5/f8fp+e5e4+ff2f8F9/ufntQ95ns7l/nf9z/gm/7f593zLOzvPN1L53M/WXk32cNnfJfXJPvmb3PPu55O9z5d7Jb55/vefzvvk+nzn+sz3zP1x7MnZcyzbm5vvkbzLf658z7jcu9z8v9/Of349sklmfe88649v897/Pvucw/s/N8X9OXs/4J+zz/vTzzZO2HPc89F25/L/fJnvdzLwt57y35+9/yz281nff+Cs77V8bZ+b63iacrn72fnPb3MT9/LqZ6ueb748/3l78/n7/c/4L/39+OO3Pn335ffnz73LjnvWc8fvO3J95l98zsn+f37Nef78+f2Ynu8yZ7fHuZefd38565PrJ7f5n97/NnO7j9+c+8fn5J8k/ef8Hf7/v/ev5/wb/3X7nv/euf8I/f+9f+9f+7f+7/Pv9/n/B3/EP/B/Of8E/y/uZfv/ev+n5vv/BPf9z/gn9+/Jc7uX533y5NuV3OX181M3e+c7r+fvv5y/7zzuWzHTHb85vdyd+e3ze8+d/28+efdvPuZ3fLz3y3/fPnfLc9rnL/fn+d/ZV/P3JcvqZ3/grf978ZL78vfOdvXk1r5f35z2Pv5P7zze87+bvs8yvXnXOs2efu/l1ec93m+/51UJvfE3O89nzvuebz3fH38n9Z5fP65dl3z/P7uT/c+e/J/Z8nr+f979/Oe8dZd5sjsz155+75z7J9zjXz7Ofcjt+bbh877js5rLn78l/fPl3ue/J7GY+mZded++fH3fy/7+T7PM79zzzP3vnffPPXz16/zs9vL83ee78e75Ofubyri52MvM/tyeTv93/OvMv2Zezs+bvN3z98741uef77+f8FTvrvNydvm9f8GP+9Pf/+QP+OPzee341fv+bvMr08mdm3Pz1e7nzcT9/f+EOf96Y7JzPtnztJ6+T2S52y589b2X/gn778Wzl/3k/4Jv/eryL3mfPrc+3nOvO8vfLtm8p57Ob938/m/97z/Pf18ne/P8/71+ef77z5nrNz3z2+VWXUzPb5u8bE3Nb5P3nk/ff8O9+ef1Obcnb53X58/vbzPXz3uXzOzPvmf7vybzL9/L95+f1t5PPu/M77Pz33mZ/fnN9vJnc/vP+DO/97z5+5x7nPu3+Op5P7bMjtnPf7+fPXxb9/K98u/ne3zf5P9/fz89ufP99z8mfuef3fN9/4Kz7zvnX/BP+fb/P+CbPs/4X+3/gn5/3lz/Pv/cX/cv/aX/cf/dX/dE/4V+f8ef8T/8G/H8f97z+fv/ev383983HZl5WzHrPL3v/BXv7kz3y2c778vzf9nmdvmW75/3vnm+XsS3M+589jv5P+9L/wXfnv/e/nn3m4nn728/Psnvc87ns573+fn7+98xjnv7nL7yc7N9/M93PLs75nu7/jvPe/8Ev+9vnGb15VlnvnvMbPN+/f+CR/vnz+883v5P3988y3N93/Pv3+fP2+Tf3m5+f3/L9uUnvJn3fPH9n8z/ve859z879zebc457adnz75Ztnf+DX/e3t/4J+5Kc33mfcme7nzc+/n7eeX6lnNrJsn7f+Cn+3/Oy+9fPneeuznO+87Pxv7eJJKpUlS2CyKmpKlsFkULJUtgt5N5P1OZv9+eWN3fk/71/P+Cff93+fz/vX47vX8LGnn9R/Ppz2355/3u+T2/Nzcu9T47UiJbU8mn8r857/P+Cfdf8K8/7x5/wf3/vb5/wb/3P/3B/3H/23/3B/v/ef5/wT/xL/xr/wj5z8/y79977+/nftyfO9zk+7/wTf6553ZLed4jt+f3jcec/v3z+e9zL35dzs5fa+Z/uz/gueyvsc7OWbN3m343vM97OXZZ55/3t75/PvLvZd5k2wO8W3n87/d+XJfc83uz+Z9WTz/Weepj3z8/2e+PPvfyb3me74z9ed5nt5L7m/yfd2/8E77OX7P533mez3883/vfJ5nd832/5vN/3yz/gnf+9955z+vPPvrPJ9zl+9/zm93fzf2f5n+3/gl/f3/g/7T9z43u/mdnP2pzm/ZM53rMs2fe+f8Ez/ve/H3Odm8f38+f7nye9uRG6/J72ee3Mz7+bv2f5n7/vn8ne+Vd+d7efP7vzytfb+bMXc8/e+bc5Z95/nf+9fOTfvPyd/vyMXvM+9+Z75/vz5n+38n9+/P+Cf7sTdS/573c5efeW/nvfN6yZn788/73f4/33z/P3vnOd/fLp+fvzkffzyT/gr5+5zP+Ef+8vP+Eu/94e/8Mf95f9uf92f9t/9uf95f97ff+Dv+Ef+Nf+Fv+Cf+CuefnP9+399l53/vfk8+b7Dl+vJsz3v+c/f3znnf3m8zvHe8iH353+P9nnc2/J9zvz93/Pz/vbs/O86ZOe/97/Py/c5e7MZ2sc/73v8zuubsvD28y59hec9/fJ4978s6+bvvPPnfudn5778+f7lkk33Ocep7yd+b+z8u/x/vfnO153ffPP3/gm/d5Pvye35efYuL2Zeb+/zb/P3b/l9vF+8vybPZz9zzqPPv3nM9+X9yT8v+2fN95Mt4+7/O/eeL3fE9+O1+e9nG5+ff2/JfPuuSLe+Ox5z+2zm5dZueP95Gavn135/n99k75zQ6xsyc/vv/BN/3zn5/e+e8ytzd85207+Z3+7/wZ8/11525zrnffmf78/n/e9/k/73847IOz5/t557Pjfs3k/73v/BPyf97ZKrNSSff+Cb/3v/N/73/PHPdxy/u/Pm/vMy01zdj5/Pu/8Ev38/Mn/BPn9/4O/fn/CP/e3/d8/4Z/70/7Z/7s/7a/7f/7v3/vb+f8N/8U/8Mf8I/8G5vz9zN/70/fj9c+88982/L/3r/wVJ7k/Zjfj2efv8/d/Nee1HP9n83vJe7551PZz3Jc759nJey+Wx3M/Z857d+z/grv1zz+yWvl/ZnN+z4/vzzf98+bvvmXfv8/N/733zO8xZu9+ObUvLzuu/nf4/b8/eVfM99+c79+c/bzL7mW4u35nfvmXd497/n/BX/e//e/j+b6zszz7NmdvM1fLv78ye53lsy5+z+epu/N7v/Bb/bfM9zHvf5z+5f+9/n/B/v7suTcbzY/Z/H25HL9+e57+T/vf5/Pvueb9/J/3v/PP+97/n8/72/c5LMvvOX7xYiOpt8v35m75d859/fF4vXnL1Nc7ctzz77+LJ7MtnP3y8+y+Z3qzP5v/euf53b55927mc9mX3Pz9vP5f+9p/L9ze38/n96859fl7/Y87Hmf977/nZefv+X6mY39+Tvy7eW357j3x+/8Fef96Od5K+Xt3/gn/Pfv/BPz+TN9/4Yd/4M/z/vH/vL/P5/29/3J/23/3D/3Z/f+8P+DP+Cf+Lv+I/8/4J/4K/4M/Pf322977led3fL5PvK/fIzfk/fvv8+fz3/ea+d1zn33883/vbz87+/8E+f78/by8zvs+dcd7nm57cx68nc15+985O68n/e/v8+Z77ML597lnye+5837OS+zzz7/3v54nO++Zu3x+z5x/Z7/wZz/vHf5/k/72/fz/g39/738yLu8uTc3358/3vOc72PL9mee/2f8Gv+9Xid7/l/fmb77+eb3k+9/4Kn63Lzrzc/2P5nt8fvLnvnb5uT2+Ze+8nNRNsrzf38zv1887vH75m6zLZn7O/zL33kncz6/ndzryfvJSc79788yb9186/PUtZzv73/Ofn3/c59k+c+9l5O+/n577fef8E79ipn7Obzn75z32+INSfrn/Bff9z75475f7xJy0599zzf38jxv+/43c77nkz+yZn77/n8+/7zn98v+fn/ee5+Zv0nPf3z8zn7s7jv8fZzU/O987PmT8/j/gn/P+9f5/wT9/4P/4K/7w/7y/P+Ev+6P+4v+4P+0/+6f+9/+6Of8G/8P/8bf8E/8H/8G5Jney/c/v3lnPZy23zudefz7+3PHr+/8Ffz/veVd8jPrLidk57szfP7n4+9/x94938Of3iXff83vk7n9f8FZ/u5LL7+N2TPayw7Jk/s/ntv+fe55/dzPH6+O/z+/v+Xf87+/yrrvPnq8Y2fvnv5PffM7+X35/uP5v3lWU5s9nzuvzf2fOznezE23/P3eTZdvnx93Vn8536y+/knfY/Le+c9/vPLPeS++TL9zd/mft+dy5PuJXZ81a2fl/vjvM65nvbzy9me3lkz75P1jx9X+W/Y+L/ueFzN1f5995nPff+Cvv98ydzN8++58m9kS3vnnbn351s8++c99n+X9zPZ8f965/J7jnd+X7J87u+Z+s83/vf+fF/eXH3f5f2/m8XuVFPL7P5f+9ef5+zN++Ty+4k8/395/lnvDW5Px7meZn+ZP5+5/wr/3h5/wnf+8Pf+GP7/21/3L/21/23/3j/3t/3j/wZ/wn/xl/xF/wn87z/gn2/Zf3/vfnntviu8+dvZmubOf1P837l+fvnbzsz3yeuM7bzPXKu5Lvl9/nP72/z78beef39z/gnf3n57Rvc8r3Pf+Cff95/wU/73Ze+ZXZuefbF8+/J+zP7/PO+uZe59/zc/Xw2c9PN6+Tvs/jeynM/tz83Zz+y/Pn/e8ybJ+3/gm+74+zJLza3z3vPOvNX9c/J7m75ve8/L/v+c/vJ3syZU9++fm98n7Pnt+b6zk79fl/Hv0c7P598f7nNktWeevN1G7z+d/c8z7q2eFubjfn9fned3nZN7/wVO+2zh2ebbvIe889cv+5/HXZ8X753O/HW553vH5/3r/N7z89/72/P+C/9+5OPvHn7fH9/n539s+b5+9mZ8n9jeXs8935z7c/z7/cn3+fvM73nN6k8fuePvOfv573vz57OT9cvt/nn97/O7+bq+/nPU8x358d/P+Dvq/8E/8Hf94f5/wZ3/vOf8Hf97/9zf9xf9tf9u/v/eH/eH/Cv/CX/GH/D3P+Cv+Deb7E/f+9/9meb3/gr39n/Bf/e/flZtjzfuSc7ee4ub2flm57b4t3/gn79/h/3t5/PuZ9nnW5y73z5+595zvnt87r+P+97+Tn3b57nL9zed+bP7n8/fndzsuee/vz83+8y+/z6yVPsx/wV9/e+S+bvJ3OXdvM2deX788+y9+P75873m/m9/eeZ2WXs42Xmft+dzt+c995u/F35+459cvNnvPfOfe8+We7k74/fHm+f97z86bvP8/e8n++ee+X/vfn/BT7f5/fvP59ma892dz4+8vd/M738ex2/m3Pn9v59lm5zfXJeadjcyfeXk79/Mt7nc57E83b3nG3l/fPzfd/H/e/zP3POe/c88fWb7zm74v2fz7z153jZu+Tu/D7zcm4mrLm/PP395zy9/3njeZP3D3N5z7fL3mzuZq5n97/P+CfdTb3nN9+e3P+Cvv7v///5d/5T8tc+b9+X9/N3w1Oed1nWfz2+3b/wTued+9/4Of95+Vs8z93Pnc7F7+W3E73xM7L7zv4/71/JnJz9+x/k/7038z5f1nz1d/fLyT5pvP2djyj7nx33+fzv/eU/4Lv3vMz/vfz/gn/vf8/P+8r/P5v7c0fvzzO653Zxz7edeLUm7O8ne/Plu5757zne35sre/HPvPl++vz3x9cvM97534/ZM6+b75NnP98vd+fnby97Obs85/vJs9+T53775/wT79nPvvnl/H7/fP5f1zk97Z8r3157fzzqZ9sznu8u+dsz3/grv938/n+/vn/BX9leT+8z9vj3+Xebf2/8Fc/fn/Bf+c/4K/nP+FX/BX/Bnn/ek/4J/7s/7S/7X/7L/7Z/7k/7lz/iL+f8e/8Uf8F/8G8k9fnv/e1yfubzze8TsPO+8+Tfs/O/6/kme5c97+d87+58bv8z7vJ/3v5zc1M2ffzm4/Z5+/k/1+b5v+/zLd5vJ9u+fndn3eedz2Z9vPL9T57s+fv9zk/eclr35zvvPntZx7b/L3L7/wTP+9ucj1fPz2W79+TzP3c/n3+7/wVm/v7/wTz3Ju/M/fvnhq9zl9z+ffv+Xfcufz/vb8Xcm7ffk8/vmSf96efyft/M/bJ3OO3JefZsxn2Tc5J/3v/wXz/vb3PP3njvLfv+ef965ye+/OJ9/PvfnH7c7PzubZcfX/BT/d/z7O41/LO7bObfnd/Ofv738SRF2yCKLAJSUWAASkosLee/zP3eZnf3nzZvPd5P78/m/9778/n3fP7XOWUHn195/O31mV/P934fv5sSz1vybZGbKLP4999+L/PL/P+CvPnz/hT57/wf/wf7/3nz/gv/t7/tT/tT/sr/tn9/7r/4T/4N/4n/4y/4R/n/BX/BXzN/13+75fV589t+c/2f5f7Pj3cye+amXdzv8fZfJ9758v2SzzU2+e/d5+ef958z/g6/97ub3vPPrzZzfeO5mfvf5nd9+XZMv8/70vz8/fd8c7qed7phTvk+b/d/iPb5Lfvn5O3Xk7uc93jz7nzt9x597+RvL+5zYvKns5vszPz3t1+b7PnvZ/m+y2d+Z3++ZzPrN65PLfvv8/n3vec3XPPv15mfZzdv5bue8vvPyfu/x9+/8Etr7z87b7/HZN1mZ33J8vuTT7nzuzz+f3Z2M2yX35+f2/53e78WN9+SbXP3JZkr9z/gl/fvPPn7vjax+8/N+p+dns2ebLnY8/e/P7555P3vn89/73znb+Xmf68nZna+d7/Pu+fc+c/73v/BT/vf78/4J/25c7tuf8E+92fO+f3ic93J73znJ98n7fy/738/l/Z/Ob+2LfPvmbf78+Tn/Bf83/gr/gr8/4Q/3/g7/hT3/vLf+FP+6/+zP+4f+zf+zv+8/+8O/8Nf8Hf8df8M/8E/8I+Pn8/32e/bvnv9/yT+b91fybsL5+5+PveZz/ff8vx78337nMzuffO/8E7/3vvk5Lzvue/z93/gn5/3v1+HUmZ9/v8zPvMfbEXcxn37+fnvb5vW/J3TF7bmZfe8v8v6/F2c73v558+9bf5vs/Pc7lkdx5q3Znv597nk78n73zz3szN7eZyfrvEdv827+b2Zu3HN/v82fz+7POftxdZmbP3y+8/n3O+/jUz3y9ZPzf+97P56iWSe+/x/eeN7fM3eT9zPPtebfmfv2eee8/ebk533zebseZ96zz1PcXny/3m8523n7k85/bZvzK1N3JcuO+/8E3/cz/P+9r+di8m+ufL7udc573v/Bnn+3b56+Z28v9+c/3n53v8f7mXeSHr5+7nlfLv3n3j9v/BPk/tJt5dzlt/O/3/N7fJ8/fXk5/bn5f75fnbJ9nJ3Zz8/v7/Pn38/Px/wf/wbz/g33/g7/hT/f+8Of8Kf91f9lf9wf9pf9qf91/7/3r/wn/wz/xf/w7/wh/wh/PsfH7/tbe8vnnrO/z3/vX/grydm9tnnLZ1+fvv89fNm2T7nnbyT3vybLuZ+/M3PvnuZm9m+POveT++c8+9zfv/Bm9vHZbc5ftz537yXb5nrOu3Mz6/H9vmf7z5m89vfyc/1zN5M+n35fOe9+fcbc/Pffjffk9SNzLY2vk+955e6z7zz89/eMvtxz57+xz2fkn9yL3fzl/vLzsTL/efz2yo/c/4Ln/eyzm8be/x/T7/P+Du/v2C+Wyr7n5fVy/HeffPvmf7P+Cfv7z8Nn++fn7v/BXz/vT9/Et8u3877yyU4+lclyvfLfmffcJNt/LOzUnb8tsPPe8T7nGZ/eXO7PM/39/z5B/3v+fn+/vn5z3WzI2zZ89X5n+3zz3ZbPzvsfP1/l99985mep6+ezvnv87/c8/M++/fF/j1zu357+d783v8+f9538+/M1zv235/N9/4L/POfz+c/4V5+f8K/8F/92/94/5v/Z3/b3/av/a3/cX/ef/d//DH/B3/Gn/EP+f8If8F/8Ffn3/dvN3735ff5b3zcn3h9z5c73+dbfn89/28ZPWTdfH9zy78+f3n2efvJ6nz3mX73nnvnt5G4t5uT795/J2/ufGuWd57N5du3nyffb8vt5zer8v7z4N9856nJ/3vPnl/eb+fP+9ff5+b9++f5f2YHZv47a/l/2eWa5c33M2f78/4J3+7Pzzv98z75nu75y7M+v5Pvdx89vntvy/j9nl+5zuyzO8t78n08k9yydjZ579/x398/k/b4/72/yY+vM7s7t/nye/URP75595yf3498t7k4/e/MR3b+VO3Jsy/bP82T9nNS57zt/M+9z5/ZHmd9zfvnPPfvPL8+8z+3vMZub+3+d8vyfZ+5/O+/ney8zPZe/nfvv/BWvP9+c5v752V9vnLNsT/vd/wT/n9/fOb2/z+f95+/PMvbUv9/zkz73nfJ+/j+/nH2fnvvn/e+/8E58/kz/g//hH3/gv/L/w1/wV/3d/3p/wV/wd/2v/2p/25/2b/2x/3t/3N/wT/wj/xH/x1/wX/wZ/wh8it8v3P3/cufnl9zdk3m78/nv7s4vXz8++9TPGZ+zS3nMv2d5fn+85n3v+P2+V89zz2y7fv/BNvm9a/nn/ey/nse/j3eb57KSfckuf3zz73v/Bn97zn33yeb2eXf5/v3857/wV93ncttn+f1cfG3uffzz97yd+L5/uX/Pvvmzctylzl9fy/181nezyy9z599/m3s44nb9Zf5z7qbeTPfuc8beZ/f9/P5r9+ee983y+5e8+f7PlmxuaZ3mW73nZ/O7892SuTepza87ZPfN/73/4J8879fnfV8537M5m/2/lys3cv579zM3v+e/vyNi+d+8/Ovc3yTezLcZ9nXnfnP335/n77yfXmf2/Odzcff4/Y+bt5ntfPf3/Ms+zc8/3PL+vy3ne8sVnfl/c/mf96c+bc/U+eb+sfOf7/vn/BVvvje5vHL/bPnOf8GZz/g+f5/wn/3p/wd/wzP+7+/8K/93/9k/9tf9o/9o/91/95/94/8N/8J/8af8Rf8JfPmf49s3/vf98/ntmJ7c863M657n5/3rM/j9xO8m8/sxi9yeeknvjSuXZn388/2v8/fO9vzzf3v/Bvv7z53Yrc87tm/j/vef8Fb/uZPuZN7nvk3W389ee7necn2cT7zvl57fI2Z3Z+d7PJ71/jeprPP+998/4Kv7zP+9z+c/rm95z7PL9mT7xF8vcl9vM1mzn9s/yfcveX3efz3388/738ts3PNy/dz+fZ5v2c2/nuwT95+X5vfduc2fn3PP+9fPP3M1LOevO1J7vz4/b+PvbyGZrWK2/m89udxu/8EvfZm8XvObpku3Js89+z+Xbs8j982VLPeeX7yZ/fLvz53/vb8/z/e5M7vL8/2/8Fe94XZ+bPfszzk/cq7Z8vvPP28/n7+/jufc5+vku05n147mfZa/k+7zz3vl15z93zd/nffNvM2vL77+ST/P+Db/wX/P+Cf+Dv+9f+E/+EO/96P+Df+7P+1/+2v+0P+z/+9P+7v+8/+HP+Ev+NP+Ifn/Bn/B8z9zJ+/7/XPO7/wb9/Z/wT/ffJvy+5fO7L8l7nZt/O78zs1Z35n23/Pf35zf38zt575z9ZE+3n87v38vfm98vr+T/bzzm/2/P3Px/ebzvl33nz3nu89t8zv9+fzv9825893zbm7nm/57+98nOe98nZy775mdns+b3ybue/J+vz3fnv8737z5Ld3Lybvm43387l+/w++fX43vy7k/ZmNm8+/M99z8s3se/mf7mfNz9cXs3z/P7vnv7zz7/wTP+9n+ef6/n/e98hbyX9+ezy6m755335Pvl9vk3Ofuc9zedY7eRKvs82znv598d+/kbuff5z9s4u1nNb579+fl+3mf97/M+zz5/fvmfN338f3PO3+evvPPZ1j5O2+8zt/Jv787lvJbJ7eM+fffOa3++fm+/lvrk/fy3P2fm/vE3y+44/3fn/BO2X2b4656n/BXf+9+//5f/5W5j7fnZ87fN95jc1/wT7Zd3P+Cfsdt+ezO7f+C/n/embu588/sc9Wefb5nd5ed7w5tuzf5fqY5z99nPyf96OTm7L+W+938+P3czeE1N8vTZ+fe7+f4/7yn8nvvOX9/n5/3tzn+v8/Pf7Oblfb8M75u+zPnczvYRFpk9ued7Lz53fn6cvcdZyfec3Tn7hvO/Gpvj+slzj74sn++Nv5z+s8938ne7s9/4K8vufZ83735/wV39zPt495ndz+b9iTbgn33P57988/X/BWfu9/ne+/k7md8n78z/vbf58+vOf72Z559nn/e7z33/grn3n9n9+/z/gv7P+Cfz/P+D/+Duf8Ief8Ifx/3t+f5/27/2T/25/27/2r/3L/3X5/xP/wT/xJ/xB+f8If5wn8/f93y+8TNk3npObvnj7Z/n97JbzvLy/f87zf2+Z95+Pt8f97/OaGdXi3+fZn38z/e/J8v9mdzv5fHu7z53ee7G8pr8nfvvzPrPP7sy+8TqbzzfX/BPdOZ32/zPvzf3+X98zJ1vFm1Ods83Od/r+O3f+Cvv/euecnx7+5yTXXJvs/P18l/fK+f97/ybTub7+Zz1Xz/vbn8ft/N9z+b9UiTPZ573eST2cy/7/wbn/e3d/O+S95Lvj/vb58nfvG83/P37/nnf1O/GzXL5v9f8E7+8/n989j+5/Mz/dmSbnz73nN/f+9/+DQlEtgsiiwAAJSUWAAN/Oyfvf5Mf78+e52OyfefO+3/gmf78fav5pUS+u/n5f25l6/n93zm78u1lvZ+P3vkiDZZM33fz9uf4/P+C/z/gz/g3/hHyf8H/8Gf97/35P+1v+zP+3/+1f+1/+9/+7v+Ff+EP+H/+Jf+Dv+Cv+Cv58+PZ37vN13ny574/X+T9v5nfc35953zNfZnvzF3nn+9zLOS9ryfZ8n3XzP+9O3/hDnZm/e5nuY5d1y75m7532vKsc8/713/Pn+9zzu93Jz3WJu3flP833vkO7yY+v5OtnM97zz9uZmvnr98vnbzO18/s/Psrzsd35ubO88t938v3n5+9n+d65t7n5f7jk9kazjd7vP+Ct/a5e58e/s58fZx3v5ns57fZyT7f553v+X932+eJ338rfP3Nec975ZLl9iHd+efP+9lhPc5nb+X/e/8F53vvM3zn775Lq+e5z75k/bH53sOay3Lz37mT2eSOz7vl85tvM/3f572fk97/Pzf+9M49835O1ZeZ33+fZJ3z9n8fX83/vb+/8F/8Ff7uM+93k+XV59/n7Mcv1Ns+Sfvzn9fL33Py7vyd+8id/PZev+9P555z/gr/L/wZ/wl5/wZ3/hj/hT/f+9L/wf/27/2P/3J/23/2f/3f/3d3/iD/g3/iz/hP/P+EP+C35/wX/v692bzv+/8E538n982c57+8zO8+5/Hv9y/izZPvh9z+Z375++e/yfb5Wd5f7x8+7/wT5/3t6/y8/cmY+/78+Ts8fe8mXW5x+/f5/n3u8ndzzX3by89K87e6/nN9/nXfjP7fMk72z/gne7+eyuS3q+Nx3k98+3PN3/J/3t+/5+dbx7/c/L+b6c6e8/Nu5ntxtmee/fjfn2X8fbk27PIfvM7nPbxPtz5vs+X938/4K3/vS/Pnv7mSYv3f+Cu+7+N7ON+/zPvN/Pfr+dnM+/uc53n3ztmXv5a9x5n92ZzqLvf8nP9zucT7zryZ/3vObec2lneZaZ9v+d/c8/P378LsSdzJ1V5Pt3/gn5/XeTvvPj35frz9z877/wU/70+cvsxx+5nvfOa/zu9n3x/s/PnP+9vLsqblxXzv9/k9M8v7yTfZPk/738zKtuZfVnz9/73/Pn3/P585/wn/wrf+C9/4V/4U/71/71+f8Ff9yf9hf9yf9uf9o/90/96+/8M/8Lf8P/8Mf8Gf8K/8F/vmeb7+xvXn3Pnc8787/3r/n53cvZufOql877nN86nZn3kt8s7eTU9znXnZ3zeXdfPeYr3j755O9879/4M99vM+82zOX9uc7vzdW+e2st3ztv8v9Zz75yeu+P5s/3L875J1ffnf5n93z3877PPn735qvP7wzZHvx3Z8n73zy+z59335/L/uONx9/Oe/t/Pne9+T98Tzf3JPuMqYfc8/ZObH7P+Cr/sTNzH3fyfsvf5/Pe6G+N87H7zl28x5795vz++c/Z/Pvr/gn9cn9+fP37/nP778Ov5f7P+Cvf2cVXl95XclNfNvzPvWak65yV73Mt81Fmfd5+d+3Geey+e7fPPo/efzn/evPn2Pzf3L95Oc6mufWf5P+9+/PP3svfn/BP3rOf7553vvmbbOV75c7nf+Cf7tzyvu+Zvk9Lczc3v+d9/PP92b5zrzv3Pd5ff+Dv8+fn/BP/Bnz/hLP+Df+Fvn/dv+/5/3f/2V/25/3F/2r/3B/3p/3r/w5/wX/xP/w3/P+E/+DP+Cf537bb39n4+/Ma2c7eT7zLdefZ898/P39kfm7uc3bmb75ff8nZ94/v587O8Xl735n3NuTncl9+dx2+cd+785vX8vvuRnN93cnnbt5vrkvuz5z/vffnzG/fOepJ/vnnnf3nf8/P+9e88an573vLyX3l4t2/Hty/l+x4/ZzPfvP+C3/e/28+PfOfv8v31/NnZLOPbeM6u+H38q/nfb5s5+zzs87vx7vcZkZfWW/N/fk+/vz/M/XHs1mzj3mfdfmc/v3nN3Ied7Od8v7mFnj/e/PMz37N+XvipbnO+s3+T+xsk598X573uf5/ds/M77y9+W9/f5yefrnuszNs2XP2P+Cp/s9z+b98z9ucZtPP3Xl9+fefy3+/Ml333DkRPrzn72c3O/n5/3tvnOd7Pb577/M8z+9X8vuc/efnu587/X6+Of8F+ef8G/8KP5/Pz/hv/gr/u//vb/g3v/aH/Z3/dX/bP/a3/eP/dX/CH/Cf/Dn/Fn5/wX/wl/wTkm9893zv9xnJ1eOz3kc/Pv18X2ufPup5Hsdlzh1nPq4z95nn77/wS+zO35b59yzPe/56mXe8/l/d43cnH2S/P3k92+e5nT78596/z77zz765mvJnvz+/v/BPnv5+9/Gbd3Pz7EmTu7zvnP3uee/52/P+92fz33A2ebXX5vb+T9v83vO+55eT9v597/N7GWTze9uX8nvZhZz0k9vnPn7/vP53rzP33yfH7z3vP8/WLPN+tnGv5uz3O5/n9++LjXOd7chvz7fjvj/vf55ts+X9u/yXrxb/3tz8mS77vz7/wV93kn2/8E+/ZJrL579/Pk37u5zZfyd7z3nsjO/Hv75/wT3ubb8v+5/M73v518z/Znn3d8+f7meX+8Zbl5v7fyf7f+Cupsu4u8eX9z+X/vXz8+zOsme+zy/Of7++fLPfMvvN2Zv+s5zn/BfPn/Cf+fz/g3/f+Fv+Ge/94P+Ef+5f+vv+4P+3/+z/+6P+8/+9f+Iv+Ef+Jv+Gv+Ef58z/grK2/v3nPbPyVs8+u8zZzqfuT+Z/3vz7+b5e+f3MZvd8c7kZ+8zbze5e88+/zP7t/n75Lvyff+93/Bs+5+d9g95za3v/BPf9n/Bb+3ndZzvfn3k99zd+Wc/283z537nJdb+XO75Nuee7zn3s+Tu5/N32Lc+f9738/z998n255z+i+Z9ZeyT3y4k++/zN3vk64y/97zn5dlvl7rx/3v/Pn+/PPbq/Fnbv56/L/c87v53qfm39nx8t/amdn+fvOf97Z8/vObL3Ms8/dcevn5f+9585+32R4jbUyfX5ee6ncvv/BWe++e3lt8y6sy7vM1zv9+fz6nbzJ+8byOy8vvvzzz/fm98zv/e/5/wX/f3xz3vMz2+b988e7n8179yeTnV3vPZ8z7nzv2f8E/37+Xvz9nPbznpEz6+XuN897v8ze/8Ev3vO2/Pz+35641P3+Pcvm/v9/Pm8/4J/4Kn/Bv/CH/Bn+e/8L/8G/f9f5/3F/2h/27/27/2Z/3h/3Z/3v/xL/wl/xL/wxP+Ef+E7HMn9+/7yc6/4M7/Z/wV/vvN85feXnesn57vnWsvvOXGfscndv593/Lv7/H3zP3zz3WF9+f57f78fv/BXqb3P5P+9/fznO+7/P7zzv3jnvzvd8/P1Wfd3mb++fnfcqZ7fDL7Od/4K773ifPvfDOXvWZ94Zd853vPfH9z5rP7/HezzF93kx6y5nt+avvPm/vn7P8e+/zc1rlWN3n5/dv/BNruPvzf3mfzc/t/N6J/wT/dy+5z7+T+7/wTP7z5/d+TYtye89zzdT3J+ffv8n3k7vy1z7k7zeam95mbO4/c/O28s+/nW+eT32e/H3OeTt2zFvnv38+X9eT9meznnv758yfu/Pe589cr9+ebfuTv5nTePffzl+/nrLNm5nbyeZ/3v745H2eXvJfc8v7yzn73/gnv3y8tqzk/737/wT/Na/ZnN2/n9x/wVf+9d///5T/5K8vPr+e7y+/8E3szt43503I/z3095x5ffff+C398uvMz+yc3rGv537vwd5Mnd1zf8f97pN+T7cn8v/eu/MzS/OlMfvfny33lVjZk7n579+/P87/3t5/m97E7+ef35z/vXvz5vYvl8/rIt52xjsyzO0k3vGTryOt3zt+enms675PP1ze5k9nN3Z/j9ub8n7Yvnnfcxfn/e+O+TO1zvc/J7uT98euc3+fvvvP+Du/7mfX8f2ZveZLOXP7ry83255v75nff88/2P+Ce/vuch3Off5f7v8zvv85+93yN85+/v5Pf+Cc9vvf77/wT/m5/P8/P+D/+DPn/BXz/hD+X/fzn/bf/Zn/cn/dv/bP/c//dXn/E/n/DH/Cv5/wd/wV88zzz3/vRzt89nks9+V3Pz97zM3b/nvZJZ2zJ/fzvnP7nk+9/lvfJ/vPN5tLc56c9v59/nn/e+/nfH9+Ts9nOZ/bfz57ptm/nur5Oe/vvObHnf3Ldnx6bPzvZ+epnl/V/Of7/O+/8E3/cmTZfi327n57Mn6+a+/8Ev2+/8G7/3vzK/O998iZdOb9v873/NfuJ8/vxvHfcvfzjtZ5/v3/grn/e+fl++/m8zbbJv59zNt3yvvPJ3+/8Gef969z5sT7nNR+85O7nE/fz7ec31y8y72Jy/3z8v+vz3JXXxnv78Z87X+v55f7vyywllsFxUtgsiiwACUlFgG+ST/vc+Y/vnNlSvn/evzj9fPP95n1iUVJPs++Zyesv9z/gn/vd85+v47YX3v853YRCj87/Z/f8/18/y/n/BP/BX/Bv/Bf/CHPz/g3+ff7O/9r/9k/92/9v/9t/97/92/8L/8Gf8L/8P/8Ff8Ff8Ff4/k3ed9jZ2f8E7n3x/s/nP2+T9mSe/l3LfcbPK9+c3+85NuN5ub2fj7s88/71vP+Dff+9ec57fmfbzebezhmd35d9vlmsv+f967+fP7755297eRvbnLZ+82X/gzvfvyyX75kv3n/BPfubM8/u889s47nzu/eTG8877y+35747hPr+V1vmbevy/X877f+Cfbc3Z5m/snh0d+TO9u/PJ/c78v7Pzn368+T+8zfff+CfN7n2bfOe7v/BO/s/y/vl+8xr7/JLn7FzN7mSe8dDPXOef97Z89t8/d+eX785/r/gl/e7+Xf4795kvr8/cSfNvvs/L3L34vNTT3mX688l2fbxJXN8+/v/BXe3+e73n+P+9vJO5c2W3Iy+/nanfPuc57f5P+8/v8/4M/33zvzvWb57cv53n943zvf8z7+8mZ+/zn/e/Od93n+bu/Oz+5l53598v2/97fnM+f8Ffx/wj/wj/PN/4W/4P+/978/4L/7b/7E/7q/7l/7V/7v/7tn/D//BP/EH/Cc/4R/4Kf5/l7d+t537/k38fTv+Z798q87z83vs5nfcjzXs/v5+Ttd8/f+Cuf96Pzsjl/3mfn/e7/gl/3tO/8Fzvrxc33OPuZm+5n53bcufu/z8/e7ncvPO9u9y57lxnrU/H9eavzl/Zzyu3b/m9v89ue/POz7XzZm9lfn3uct/k/7395+dryff2f53837c89y+8/Pdvzt3Obsmd+/5s+f97ZfM9vi+1+Z2fvPP78z3eeX1fPe/ON/ef8E7/3tvz8/Zcd+cft8vs+T32/M28n7OZfs5vHPfffnLmz1nHebpmcz/fZnnZz1r8zf7J8z2++deZ5/vfx+889ktrmfZxPu/m+3/PP3vzTuM+3nl9TZznfe/8E+f9789fM/s+b78n9z5+5879/4K7/fJn3MknvfO/fnPX/BXtt1P2/z5n/enxreZ7JJu/N/fzPdyfH7Lyfd84/eZFzffmd3XM/3/vf+S/8FfPnP+FP+Fc+Z/wv/wb/3p95/n/b3/Yn/df/c3/bX/c//eN/4Z/4R/4Z/4Q/4J/4T/4N95/mXf7vzv1+feeexPzv/ev8/L783s7zm1G/nv35bkKe+Nv4e783cu859zzb759+SU9yc2d5dnx7vnt/N28z7l7J+P7eZ3vzV153vd8N49n83/veeZ3ny7u53yefuXmzLvl/fHj6/PvN3c+e3kn2ef3lxlm7Obr+P7vyd3PPezn89/73+XG43nnfszzu78z9V8ez4/3KjiT9n8+9xfL9z/grf9d8nfPnu/fzn952/nzf3ZvM75Pcsez5ft+bPN/fzvn/e/5z9z87++Z285/3t583+/8E+f77/NvZ/N9fzv3ObR+d7k+3nPebd+N859947mXt8/Lt718157eNnP1zx71xz23/Pfb8+fvfk7fy/96efls8vXvWec7ee8z3efyf7vOb7zqfO+5nPfMvv78qmW9/Ozv59/zv1PD375l/l/ZPeXl53fl/v58/73Yxt/Pdvu+d+/8G/P8/P+DP+D/P+Dvn/Bv/CL/vL/f5/3V/2Z/3F/3Z/29/3F/3f/3v/w3/wf/w9/wn+f8Hf8E/8E/xqZ77738u8mXn2Z2Zn78u5uc/ZN5+d/uL8duY9yfP33x7/m+895z9nm9fnucvd5nfN7jnWakZ9ucTvvnO9vxdc6md3ZObn6x/ZyPZPJ/qTkvvznbkn/e+fzl+z7/n5/3t98/m6f8E+91cs+/O+TPu85O/WfL0y9ni+5+P3c8n3uT3+Tu340ns85+3eTnZnbc7nL783UJ3POxu/FtWWJy7jvm/35zv3n8z91zcrm7l2Z3X+Tn+/cwzP3Pnfcz7/H3mdtzl/bz8533uvy787fPbMzuznf8v9s7uc5q35N+z+fex8z3ZPvOPu/DFbNeV3O/zufe5/H7hzvvxu582Wrz6fL2e/Pze/eSXn33xM+r559uf5/v2c/T/gn5/e48feK938jz9uX533FzzvfPnv+3+z5vn/BXz5/wd/wnzz5/wX/wz8/7t/3/gz7/2b/2n/3l/3D/29/3j/3b/wh/wh/wv/xBz/gv/hH/PPLWe+53fnvll7nxt25358795mz7O+fz7Z3Ob3yXaiWcz7PfG+5+T3v8z+znrzuc/uc7z93/g3+3kd7n85/vc5rr83Zd82Te8bY6/fnP2v+DP9b597POe1zPfnd8zfnvebDJ750nM26d85+3x+/8Fbvn9j8v+/JbLJvm9fm/e/zPt+dnL9eJfX59v5Ou3fPk+7PZn5n9bxO8ntyY2b/P9/v5+dd/M++8kvfN9/nP+9882XNmfYzt/Ol59n5378mlvxvrOX3J3lzvJ3yb3b5nfP75y+zJH/enz/PuVN7/PfnvvPL3v/BX775J2yfPv758dLztXzn2efuWa898z998/4J/dk9fM/3nM72/Ozk/eX+fvs8v75nl+xy7m/L/Zy/7n+dw981uZrz31/wVf95z7M2zkfeL55/3v/c/4M9nfM99xczv/e25+T8/4Kn8/4T/P+Dfl/4U/4Q+/96c/4M/7j/7D/7l/7s/7V/7n/70/72/4h/4L/4b/4T/4Q/z8z/gx5bfv3yfe+cs82/z3bub5z7zn/e6/8F71fn9zO57w8nvY/PZ2ZuTry7L3M758/fvnz+8svnn3/vef8FX7PzvXk3vkt7Z8f975/wV7+zztyPd8/c53Zd5rn283zN7nnuvO/53P6/muvzrOft38nfef8Ffb7zq+efvZ/n7fx/snk/vMzsdnO95M/eYXN3+fZ9vydyJP+9s+Y7y7ju+fO/7/Of96/zne82cZfuzy3m9nz1533fmTn3eXJfUk+3/gn++fn/e/Od+/nuN5mud+78WZ5v3f55/bd+d87F57y7+fffm+T1zq+/8FTe2b5u1+bbeR3ec9zO/vn893n3PD+/ndT87ffi9zmf749/Ob/3t8/4M/37HNs5Pt+PvJOt/mz375mTz7s75f3PM9nz7M/4J/3/fl/f5+/k/X522zOZ+sbPO5+9z8W/J+0rznv9/4J2/L9c/fzn7Iv3/fzy8+f8G/P+Dv+EP+Cud/4Z/4K/fc5/3F/2h/3B/3F/2t/3j/3b/f+I/+Dv+Hf+EX/B//Cq8/N5v9/2Z5t/4Jv3v/Bn33WfL74b1med7x05u58/vy3Unz37nzv3/glv3k98z/f8n2bz5/b7+f8F/fvvM7+fcy/vn8f96zPPL/u/59v4/fNd8ndmb57c728zv++f5f7Jb89SYbvnf+Cu/blz877juZ899yc+475n++c9yfv/BM/73zk+/z/vf/HfeZmze+Wesucvs5N7vny/vnt/PawTSRNevlu/JW3PeZ73n+fef7nm7k9f8F/97zJ/uZv839n8f2fz7/v/BPPZNrPc3zH2Z7M8778u5PrOVz+5zvPct89t+Z3zT7fGd4v3837v+Zz3s7y7nJdpfnbOff3/P539fL+55/ec5798zx99/4K/fvOd3/L+vk26j49bfOfrx3fNTLXfM7eH5/f34w/Z+XvJPs5P781nffjvPfm5tb5n9ufk2N3kvvf59x/wVv/e3f/jMgHADIAAAANDQAAAAAAAAEAgwSDBKIAHgFSAboB/AI5AFIAHwAAADMAMwCaAQIAAAA7XggAtAAAAA0NAAAAAAAAAOMHBx0MMyUBAZcAUml0bW8gc2ludXNhbGUKclNyJyhWMSkgLSBwcm9iYWJpbGUgdmFyaWFudGUgbmVsbGEgbm9ybWEKRUNHIG5lbGxhIG5vcm1hCgoqKiogQVRURU5aSU9ORTogTEEgUVVBTElUw4AgREVMTCdFQ0cgUFVPJyBJTkZMVUVOWkFSRSBMJ0lOVEVSUFJFVEFaSU9ORSAqKioKAACrTwoA9AQAAA0NAAAAAAAADABoAAEAZAB8ALYAWACkAQAANgAhAAAAAAAAAKMBsP4AAAAADwA/AAAAhQEAABgAAQABAC91L3UeAC91XQBEAFgAL3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91AgBkAHwAtgBWAKQBDABJAAAAAAAAAOL/FAcAAAAAAAA2AIgAAAB8AQAAEgABAAEAL3UvdSgAL3VmAEoAWAAvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3U9AGQAfAC0AGIAnAEXAEoAAAAAAAAAXf+8BQAAAAAAACIASwDo/yUA7P/6/wMABAAvdS91KwAvdQYABQAAAC91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdT4AZAB8ALoATACgATgAEwAAAAAAAADK+5cAAAAAAAAA3f8AAJ7/AACB/uv/AgACAC91L3U+AC91of+5/6j/L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91PwBkAHwAtgBeAKQBAAAcAEEAAAAAAAAAwwDP/QAAAAD5/yoA8v/MAAAADwAEAAEAL3UvdRIAL3UpAB8ALAAvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3VAAGQAfAC2AGAApAERAE4AAAAAAAAAsP9pBgAAAAAAACwAaQAAALsAAAAFAAEAAQAvdS91KQAvdTkAKAAtAC91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdQMAZAB8ALIAVAB+AQAAIAAXABsAAAAAAHYAW/8CAQAAGQBLAAAAAABB//r/AQACAC91L3VBAC91AwAGAAEAL3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91BABkAHwAsABYAKYBAAA8ABsAAAAAAAAAyAJh/gAAAABgAF4AAAAfAQAAFwABAAEAL3UvdSgAL3WrAJIApAAvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UFAGQAfACwAFQAqgEAAEEAEgAAAAAAAABjBi3/AAAAAGUAVAAAAF8BAAAWAAEAAQAvdS91MgAvdZ4AhwCaAC91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdQYAZAB8AL4AUACcAQAANwAIAA8AAAAAAOcIyf+DAAAAWABPAAAApQEAABQAAQABAC91L3UkAC91iABsAIEAL3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91BwBkAHwAtgBiAKQBDwBSAAAAAAAAALn/KgkAAAAAAAA9AEoAAAC4AQAAEQABAAEAL3UvdSsAL3ViAEYAXAAvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UIAGQAfAC0AGQApgETAFAAAAAAAAAAmf93CAAAAAAAADQATwAAAL4BAAASAAEAAQAvdS91LQAvdV0ARQBVAC91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3UvdS91L3Uvdbj9gADQAAAADQ0AAAAAAAAAAQAAAQIAL3UCAgAvdQMCAC91BAIAL3UFAgAvdQYCAAAABwEAAAgBAAAJAQAACgEAAAsCAAAADAIAAAANAgAAAA4BAAARAQAAFAIAAAAXAgAAAA8BAAASAQAAFQIAAAAYAgAAABABAAATAQAAFgIAAAAZAgAAABoCAAAAGwIAL3UcAgCJAR0CAJQBHhUAAWQABAARAQEAAAAAAAYAHAAGAUoCHwEAACACAAEAIQEAASIBAAAwAgD//zIBAAH/AAA="
}
```



***Responses:***


Status: send scp | Code: 200



```js
"Ok"
```



### 2. Get anomaly interval


Endpoint protetto che permette di ottenere gli intervalli delle anomalie, TODO: definire formato


***Endpoint:***

```bash
Method: GET
Type: 
URL: https://localhost:5000/interval
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| md5 | 7340ae4c5b30ad3cf13b36ec0e1136f9 |  |



***Responses:***


Status: Get interval of measure | Code: 200



```js
{
    "test0": 0,
    "test1": 1,
    "test2": 2,
    "test3": 3,
    "test4": 4
}
```



## TEST
API endpoints per i test



### 1. [TEST] Get resource


Endpoint protetto accessibile con il token di accesso, test di validità token


***Endpoint:***

```bash
Method: GET
Type: RAW
URL: https://localhost:5000/secret
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Body:***

```js        
{
	"username" : "test",
	"password" : "test_psw"
}
```



***Responses:***


Status: Get resource | Code: 200



```js
{
    "answer": 42
}
```



### 2. [TEST] User registration


Endpoint non protetto che permette di aggiungere un utenza


***Endpoint:***

```bash
Method: POST
Type: RAW
URL: https://localhost:5000/registration
```


***Headers:***

| Key | Value | Description |
| --- | ------|-------------|
| Content-Type | application/json |  |



***Query params:***

| Key | Value | Description |
| --- | ------|-------------|
| file |  |  |



***Body:***

```js        
{
	"username" : "test",
	"password" : "prova"
}
```



***Responses:***


Status: [TEST] User registration | Code: 200



```js
{
    "message": "User test was created",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDg0NTMsIm5iZiI6MTU2OTQwODQ1MywianRpIjoiYzViN2UxODEtMWMwNy00MTQ1LWE3M2EtNjNlYWIxYjM4YTkyIiwiZXhwIjoxNTY5NDA5MzUzLCJpZGVudGl0eSI6InRlc3QiLCJmcmVzaCI6dHJ1ZSwidHlwZSI6ImFjY2VzcyJ9.fuk2jZ_q55Pq1Ur_6Uk5DQ7Qz8QaMqiQQ_j_4bQ7VbM",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDg0NTMsIm5iZiI6MTU2OTQwODQ1MywianRpIjoiYWUyNWRlNmItZDY3Mi00NmU2LWE1ZGQtZThkMTdiYTZkMWIwIiwiZXhwIjoxNTcyMDAwNDUzLCJpZGVudGl0eSI6InRlc3QiLCJ0eXBlIjoicmVmcmVzaCJ9.iFirxmW1uO3BSK33SdTc5qdYzQlLLlf-DXTf9xNjbJc"
}
```



## Token management
API endpoints protetti con OAuth JWT per la gestione dei token



### 1. Logout


Endpoint protetto accessibile con il token di accesso, effettua il logout dell'utenza


***Endpoint:***

```bash
Method: GET
Type: 
URL: https://localhost:5000/logout
```



***Responses:***


Status: Revoke access token | Code: 200



```js
{
    "message": "Access token has been revoked"
}
```



### 2. Login


Endpoint protetto accessibile solo con le credenziali, permette di ottenere sia il token di accesso che quello di rinnovo, momentaneamente il periodo di validità di un token è di 300 secondi.


***Endpoint:***

```bash
Method: GET
Type: 
URL: https://localhost:5000/login
```



***Responses:***


Status: Login | Code: 200



```js
{
    "message": "Logged in as test",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDgwNjQsIm5iZiI6MTU2OTQwODA2NCwianRpIjoiYjM5MzMzODgtNmQ0MC00MTQxLWJlZmEtZGM4ZmFlNjY3ZWRjIiwiZXhwIjoxNTY5NDA4OTY0LCJpZGVudGl0eSI6InRlc3QiLCJmcmVzaCI6dHJ1ZSwidHlwZSI6ImFjY2VzcyJ9.lHrZN_5lAGHe90yCx-YLFN0ay0fz4h8seP4ARjUjGnY",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDgwNjQsIm5iZiI6MTU2OTQwODA2NCwianRpIjoiMTk3MDU1NWQtZTc0Ni00MzU3LTlhN2MtOTBlYjE2OGRkYmM4IiwiZXhwIjoxNTcyMDAwMDY0LCJpZGVudGl0eSI6InRlc3QiLCJ0eXBlIjoicmVmcmVzaCJ9.dT2Jp8Y8jewsm2qAQXwILfTCWZnFHAxjSOxfAJ2zfvw"
}
```



### 3. Refresh token


Endpoint protetto accessibile con il token di rinnovo, rinnova sia il token di accesso che il token di rinnovo


***Endpoint:***

```bash
Method: GET
Type: 
URL: https://localhost:5000/token/refresh
```



***Responses:***


Status: Refresh token | Code: 200



```js
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDAxODUsIm5iZiI6MTU2OTQwMDE4NSwianRpIjoiOGY3YmQyMGUtMzA5OC00YWYwLWI2MDItY2ZiZjk4MzZkYTE0IiwiZXhwIjoxNTY5NDAxMDg1LCJpZGVudGl0eSI6InRlc3QiLCJmcmVzaCI6dHJ1ZSwidHlwZSI6ImFjY2VzcyJ9.9t-Rv04tAil_Aj8sn33GbYKsrbc72WdIYjJApiDGzqY",
    "refresh_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1Njk0MDAxODUsIm5iZiI6MTU2OTQwMDE4NSwianRpIjoiNjIxYTc4NDktZTVjNi00Yzk5LWEyOTctODg4YTRmNTRlMWI4IiwiZXhwIjoxNTcxOTkyMTg1LCJpZGVudGl0eSI6InRlc3QiLCJ0eXBlIjoicmVmcmVzaCJ9.TwFrvUIt6NxPxRfOsn8rRjWRIRA998-b8ajeoGyhkrQ"
}
```



### 4. Renew service tokens and keys


Api endpoint che rinnova la coppia di chiavi per la crittografia asimmetrica e aggiorna i token di accesso


***Endpoint:***

```bash
Method: GET
Type: 
URL: https://localhost:5000/renew
```



---
[Back to top](#api-token-server)
