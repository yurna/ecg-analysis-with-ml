from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from ..db_models import db, Config_db
import os

# server initialization
app = Flask(__name__)
# endpoint plugin
api = Api(app)
# token manager
jwt = JWTManager(app)

class Config(Config_db):
    # cookie encryption
    SECRET_KEY = os.urandom(32)
    # jwt encryption
    JWT_SECRET_KEY = os.urandom(32)
    # blacklist toggle
    JWT_BLACKLIST_ENABLED = True
    # both on access and refresh
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    PROPAGATE_EXCEPTIONS = True


