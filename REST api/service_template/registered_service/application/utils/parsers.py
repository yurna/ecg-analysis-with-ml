from flask_restful import reqparse

parser_key = reqparse.RequestParser()
parser_key.add_argument('public_key', help='This field cannot be blank', required=True)
