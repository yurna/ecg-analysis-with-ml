from flask_restful import Resource
from flask_jwt_extended import jwt_required

from ..utils import parser_scp
from ..db_models import BlobModel


class Compute(Resource):
    """
        Resource class available only through JWT, it computes the anomaly interval from a given measure_id

        ...
        Models
        -------
        BlobModel -> update interval

        Methods
        -------
        post()
            The method parse the header looking for the token and then update the received
            measure with the computed anomaly interval
    """

    @jwt_required
    def get(self):
        """It handles the computation behind the anomaly detection algorithm

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                interval -> to be defined
            }
        """


        return a
