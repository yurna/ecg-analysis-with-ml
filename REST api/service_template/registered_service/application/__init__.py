from .app import app, Config, SERVICE_PORT, api
from .db_models import db
from .app import job, jwt, initialize
from .registration import register
from .resources import Refresh
from apscheduler.schedulers.background import BackgroundScheduler
from sqlalchemy_utils import database_exists, create_database

def create_app():
    app.config.from_object(Config)
    db.init_app(app)

    with app.app_context():
        # creation of db tables, if not already there
        if not database_exists(db.engine.url):
            create_database(db.engine.url)
        db.create_all()

    register()

    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        token_type = decrypted_token['type']
        jti = decrypted_token['jti']

        # cached
        JTI_access, JTI_refresh = initialize(app)

        if token_type == 'access':
            return jti != JTI_access
        if token_type == 'refresh':
            return jti != JTI_refresh

    scheduler = BackgroundScheduler()
    scheduled_job = scheduler.add_job(job, 'interval', seconds = 600)
    scheduler.start()

    api.add_resource(Refresh, '/refresh')

    return app