from . import db


class DiscoveryRegistryModel(db.Model):
    """
        Data model used to pair service name to its address

        ...

        Attributes
        ----------
        id : integer
            growing index
        address : str
            net address pf the resource e.g. ip
        port : str
            net port to which the service is reachable
        token : str
            service toke todo needed?
        service : str
            service name


        Methods
        -------
        save_to_db()
            Saves the current object to db
        return_all()
            Returns a json-formatted output of the current state
        find_by_service()
            Returns the pointer to the searched service
        delete_by_service()
            Delete the searched service


    """
    __tablename__ = 'discover_registry'
    __table_args__ = {'extend_existing': True}
    # __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    address = db.Column(db.String(255), nullable=False)
    port = db.Column(db.String(255), nullable=False)
    token = db.Column(db.String(255), nullable=False)
    service = db.Column(db.String(255), unique=True, nullable=False)

    def save_to_db(self):
        """Commit a new row to db

        Raises
        ------
        ErrorName
            When registering a new user with the same username
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def update_service(cls, address, port, token, service):
        """Updates service token

        Parameters
        ----------
        service : str
            Microservice identification e.g. computation
        token : byte
            JWT access token with RSA encryption
        """
        row = cls.query.filter_by(service=service).first()
        row.address = address
        row.port = port
        row.token = token
        row.service = service
        db.session.commit()

    @classmethod
    def return_all(cls):
        """Returns a json-formatted output of the current state

        Returns
        -------
        dict
            with tokens key which contains a list of username-access-refresh dictionaries
        """
        def to_json(x):
            return {
                'address': x.address,
                'port': x.port,
                'token': x.token,
                'service': x.service
            }

        return {'services': list(map(lambda x: to_json(x), DiscoveryRegistryModel.query.all()))}

    @classmethod
    def find_by_service(cls, service):
        """Returns the pointer to the searched service

        Parameters
        ----------
        service : str
            Microservice identification e.g. computation

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        return cls.query.filter_by(service=service).first()

    @classmethod
    def delete_by_service(cls, service):
        """Returns the pointer to the searched user

        Parameters
        ----------
        service : str
            Parameter to be searched

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        cls.query.filter_by(service=service).delete()
        db.session.commit()



