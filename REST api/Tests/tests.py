import unittest
from pathlib import Path
from request_methods import register, login, refresh_token
from request_methods import send_scp, get_interval
from ast import literal_eval
from hashlib import md5
from time import sleep

USER = 'test12'
PASSWORD = 'prova'


class User(unittest.TestCase):
    def register(self):
        answer1, answer2 = register(USER, PASSWORD)
        self.assertEqual(answer1, 'User {} was created'.format(USER))
        self.assertEqual(answer2, 'User {} already exists'.format(USER))


    def login(self):
        answer = login(USER, PASSWORD)
        self.assertEqual(42, answer)

    def refresh_token(self):
        answer = refresh_token(USER, PASSWORD)
        self.assertEqual(42, answer)

class Resources(unittest.TestCase):
    pairs = {}
    def post_scp(self):
        download_dir = Path('.') / 'scp_files'
        for scp in download_dir.glob('*.scp'):
            response = send_scp(USER, PASSWORD, scp.read_bytes())
            response = literal_eval(response.text)
            computed_md5 = md5(scp.read_bytes()).hexdigest()
            self.assertEqual(response['MD5'], computed_md5)
            self.pairs[response['MD5']] = response['intervals']

    def get_interval(self):
        for hash in self.pairs.keys():
            response = get_interval(USER, PASSWORD, hash)
            response = literal_eval(response.text)
            self.assertEqual(response, self.pairs[hash])


def suite(iterable):
    suite = unittest.TestSuite()
    suite.addTests(iterable)
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner(failfast=True)
    print('User management tests')
    runner.run(suite([User('register'), User('login'), User('refresh_token')]))
    sleep(0.5)
    print('Resources test')
    runner.run(suite([Resources('post_scp'), Resources('get_interval')]))

