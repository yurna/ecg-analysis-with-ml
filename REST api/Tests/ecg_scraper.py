from TicuroTestDatabase import TicuroTestDB
from tqdm import tqdm
from pathlib import Path

if __name__ == '__main__':
    download_dir = Path('.') / 'scp_files'
    if not download_dir.exists():
        download_dir.mkdir(parents=True, exist_ok=True)

    db = TicuroTestDB()
    measures = db.get_measures()
    files = db.get_all()

    for file, measure in zip(files, measures):
        with open(download_dir / '{}.scp'.format(measure), 'wb') as f:
            f.write(file)

