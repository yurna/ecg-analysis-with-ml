import requests
from ast import literal_eval
import base64
import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


def get_secret(token):
    head = {'Authorization': 'Bearer ' + token}
    url = 'https://localhost:5000/secret'
    response = requests.get(url, headers=head, verify=False)
    return literal_eval(response.text)


def register(user, password):
    data = {'username': user, 'password': password}
    url = 'https://localhost:5000/registration'
    response = requests.post(url, json=data, verify=False)
    parsed_response_1 = literal_eval(response.text)
    response = requests.post(url, json=data, verify=False)
    parsed_response_2 = literal_eval(response.text)
    return parsed_response_1['message'], parsed_response_2['message']


def login(user, password, return_token=False):
    string = '{}:{}'.format(user, password)
    b64string = base64.b64encode(string.encode()).decode()
    head = {'Authorization': 'Basic {}'.format(b64string)}
    url = 'https://localhost:5000/login'
    response = requests.get(url, headers=head, verify=False)
    parsed_response = literal_eval(response.text)
    secret = get_secret(parsed_response['access_token'])
    if return_token:
        return secret['answer'], parsed_response
    else:
        return secret['answer']


def refresh_token(user, password, return_token=False):
    _, parsed_response = login(user, password, return_token = True)
    token = parsed_response['refresh_token']
    url = 'https://localhost:5000/token/refresh'
    head = {'Authorization': 'Bearer ' + token}
    response = requests.get(url, headers=head, verify=False)
    parsed_response = literal_eval(response.text)
    secret = get_secret(parsed_response['access_token'])
    if return_token:
        return secret['answer'], parsed_response
    else:
        return secret['answer']
