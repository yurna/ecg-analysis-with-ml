from . import login
import base64
import requests


def send_scp(user, password, scp):
    _, parsed_response = login(user, password, return_token=True)
    token = parsed_response['access_token']
    encoded_scp = base64.b64encode(scp).decode()
    body = {'payload': encoded_scp}
    head = {'Authorization': 'Bearer ' + token}
    url = 'https://localhost:5000/send_scp'
    response = requests.post(url, json=body, headers=head, verify=False)
    return response


def get_interval(user, password, md5):
    _, parsed_response = login(user, password, return_token=True)
    token = parsed_response['access_token']
    head = {'Authorization': 'Bearer ' + token}
    url = 'https://localhost:5000/interval?md5={}'.format(md5)
    response = requests.get(url, headers=head, verify=False)
    return response
