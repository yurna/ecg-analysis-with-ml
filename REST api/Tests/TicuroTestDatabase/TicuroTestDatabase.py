import paramiko
from sshtunnel import SSHTunnelForwarder
from getpass import getpass

from tqdm import tqdm
from .secrets import psw_ticuro_test
from pathlib import Path
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

import cx_Oracle
import ast


class TicuroTestDB:
    """
    Classe per interfacciarsi con TICURO_TEST

    ...

    Attributes
    ----------
    mypkey : RSAkey
        chiave privata

    Methods
    -------
    get_row(row)
        Ritorna un byte array dell'oggetto scp della riga row nella tabella TLM_MEASURE
    """

    def __init__(self, key = Path(dir_path) / 'private.key'):
        """
        Parameters
        ----------
        key : str
            Posizione della chiave privata, richiederà la password per decriptarla
        """
        password = getpass()
        self.mypkey = paramiko.RSAKey.from_private_key_file(key, password=password)  # convert putty key to openssh

        with SSHTunnelForwarder(('88.99.188.191', 22), ssh_username='root', ssh_pkey=self.mypkey,
                                remote_bind_address=('localhost', 1521)) as tunnel:  # tunnel ssh
            dsn_tns = cx_Oracle.makedsn('localhost', tunnel.local_bind_port, sid='xe') # database
            conn = cx_Oracle.connect(user='TICURO_TEST', password=psw_ticuro_test, dsn=dsn_tns) # connection
            c = conn.cursor()
            c.execute('select * from tlm_measure where code = \'SUB_ECG\'')
            self.measures = list(c)
            conn.close()

    def get_measures(self):
        return self.measures

    def get_row(self, row):
        """
        Parameters
        ----------
        row : int
            Riga della tabella

        """

        with SSHTunnelForwarder(('88.99.188.191', 22), ssh_username='root', ssh_pkey=self.mypkey,
                                remote_bind_address=('localhost', 1521)) as tunnel:  # tunnel ssh
            dsn_tns = cx_Oracle.makedsn('localhost', tunnel.local_bind_port, sid='xe') # database
            conn = cx_Oracle.connect(user='TICURO_TEST', password=psw_ticuro_test, dsn=dsn_tns) # connection
            c = conn.cursor()
            n_measure = self.measures[row][0]
            c.execute('select * from tlm_measure_detail where measure_id = {}'.format(n_measure))
            id = ast.literal_eval(list(c)[0][3])['id']
            c.execute('select * from tlm_measure_file where id = {}'.format(id))
            scp = list(c)[0][1].read()
            conn.close()
        return scp

    def get_all(self):
        files = []
        with SSHTunnelForwarder(('88.99.188.191', 22), ssh_username='root', ssh_pkey=self.mypkey,
                                remote_bind_address=('localhost', 1521)) as tunnel:  # tunnel ssh
            dsn_tns = cx_Oracle.makedsn('localhost', tunnel.local_bind_port, sid='xe') # database
            conn = cx_Oracle.connect(user='TICURO_TEST', password=psw_ticuro_test, dsn=dsn_tns) # connection
        for measure in tqdm(self.measures):
            c = conn.cursor()
            n_measure = measure[0]
            print('1')
            c.execute('select * from tlm_measure_detail where measure_id = {}'.format(n_measure))
            id = ast.literal_eval(list(c)[0][3])['id']
            print('2')
            c.execute('select * from tlm_measure_file where id = {}'.format(id))
            scp = list(c)[0][1].read()
            files.append(scp)
        conn.close()
        return files

