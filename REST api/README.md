# ECG analysis 

RESTful API per l'identificazione di anomalie nel tratto ECG


## Indice

- [Introduzione](#introduzione)
- [Features](#features)
- [Requirements](#requirements)
- [Quick Start](#quick-start)
- [Testing](#testing)
- [To do](#To-do)




## Introduzione

RESTful API per l'analisi multivariata del segnale ECG e la valutazione di eventuali battiti anomali.
Il sistema possiede un architettura a stella con un servizio di interfaccia centrale e servizi satelliti

## Features

- Autenticazione tramite _bearer JSON Web Tokens_ divisi in token di refresh e token di accesso
- Database che compie il ruolo sia di storage condiviso dei micro-servizi che di _discovery registry_
- Chiavi asimmetriche per la cifratura dei token di accesso ai vari servizi satelliti
- Implementazione con docker compose



## Requirements
Applicativi sviluppati interamente in [Python3.7](https://www.python.org/downloads/release/python-374/), i moduli 
necessari sono nel file requirements:
```bash
$ pip install -r requirements.txt
```
o
```bash
$ python -m pip install -r requirements.txt
```
Nel caso si vogliano far girare direttamente sulla macchina in uso.
### Add-ons

- [Python](https://docs.python.org/3/using/index.html)
- [Docker](https://www.docker.com/get-docker)
- [PyTorch](https://pytorch.org/get-started/locally/)


## Quick Start


Nella cartella di root del progetto digitare
```bash
$ docker-compose build
```
quindi
```bash
$ docker-compose up
```

La porta vincolata nella macchina di host è la 5000, porta di default di un Flask web server, che permetterà l'accesso agli endpoint del server.


### Documentazione

- [Docs](Doc/API%20doc.md)


## Testing
È presente una suite di test delle RESTful API nella cartella Tests.

### Requirements
Per l'utilizzo vanno inseriti _private.key_ con una chiave privata con i permessi di accesso al database Ticuro e un file _secrets.py_ con 
```python
psw_ticuro_test = '<password>'
```
Va quindi avviato il file _ecg_scraper.py_ che si occupa della creazione della cartella scp_files e del download di file scp di test

### Test
I test sono implementati con _unittest_ e riguardano:

- Il servizio di gestione del _client_: registrazione, log-in, rinnovo del token
- Il servizio che si occupa dell'analisi dell'ecg: invio dell'scp, recupero dell'intervallo

Output:
```
User management tests
...
----------------------------------------------------------------------
Ran 3 tests in 0.754s

OK
Resources test
..
----------------------------------------------------------------------
Ran 2 tests in 17.860s

OK
```

## To do
- [x] Service discovery automatico senza ricorrere a chiavi asimmetriche preimpostate
- [x] Aggiunta del modulo relativo alle reti neurali
- [x] Rimozione dell'ambiente di debug per la produzione e modifica delle password preimpostate per la generazione dei token, chiavi RSA e accesso al database
- [ ] Completare commenti e documentazione
- [ ] Modifica tipo di accesso al db
- [ ] Aggiunta di certificato SSL firmato, ora utilizza un certificato self-signed
- [ ] Formato in cui viene inviato l'intervallo