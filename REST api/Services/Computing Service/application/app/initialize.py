from sqlalchemy.exc import IntegrityError
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from flask_jwt_extended import create_access_token, create_refresh_token, get_jti
from ..db_models import db, ServiceTokenModel
from .service_param import SERVICE_NAME
from flask_caching import Cache


cache = Cache(config={'CACHE_TYPE': 'simple'})


def encrypt(public_key, string):
    """Encrypt the fiven string with a public key

    Params
    -------
    public_key : RSAPublicKey
        Pubic key used for encryption
    string : str
        string to be encrypted

    Returns
    -------
    bytearray
        Encoded string
    """
    return public_key.encrypt(
        string.encode(),
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )


# creation of access/refresh token in order to have the JTI values always available
@cache.memoize()
def initialize(app):
    """Creates and uploads to db the access and refresh token

    Params
    -------
    Flask
        Flask instance of a running microservice with discovery registry

    Returns
    -------
    str, str
        Token identifiers for blacklisting function
    """
    # open app context for token creation
    with app.app_context():
        # creation of db tables, if not already there
        db.create_all()

        # tokens creation
        access_token = create_access_token(identity='default', fresh=True)
        refresh_token = create_refresh_token(identity='default')

        # open saved public key
        # with open('public_key.pem', "rb") as key_file:
        #     public_key = serialization.load_pem_public_key(
        #         key_file.read(),
        #         backend=default_backend()
        #     )
        public_key = serialization.load_pem_public_key(
            cache.get('public_key'),
            backend=default_backend()
        )


        # encrypt the two tokens
        encrypted_at = encrypt(public_key, access_token)
        encrypted_rt = encrypt(public_key, refresh_token)

        # saves token to db or updates them
        try:
            entry = ServiceTokenModel(service=SERVICE_NAME, token=encrypted_at, refresh=encrypted_rt)
            entry.save_to_db()
        except IntegrityError as e:
            db.session().rollback()
            ServiceTokenModel.update_token(SERVICE_NAME, encrypted_at, encrypted_rt)

        # extract identifiers from tokens
        JTI_access = get_jti(access_token)
        JTI_refresh = get_jti(refresh_token)

    return JTI_access, JTI_refresh


