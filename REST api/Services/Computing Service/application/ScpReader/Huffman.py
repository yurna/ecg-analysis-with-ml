# TODO completare i commenti
class HuffmanTable:
    def __init__(self, numberOfCodeStructuresInTable=19,
                 numberOfBitsInPrefix=[1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 10, 10],
                 numberOfBitsInEntireCode=[1, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9, 10, 10, 18, 26],
                 tableModeSwitch=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                 baseValueRepresentedByBaseCode=[0, 1, -1, 2, -2, 3, -3, 4, -4, 5, -5, 6, -6, 7, -7, 8, -8, 0, 0],
                 baseCode=[0, 1, 5, 3, 11, 7, 23, 15, 47, 31, 95, 63, 191, 127, 383, 255, 767, 511, 1023]):
        self.numberOfCodeStructuresInTable = numberOfCodeStructuresInTable
        self.numberOfBitsInPrefix = numberOfBitsInPrefix
        self.numberOfBitsInEntireCode = numberOfBitsInEntireCode

        self.tableModeSwitch = tableModeSwitch
        self.baseValueRepresentedByBaseCode = baseValueRepresentedByBaseCode
        self.baseCode = baseCode

    def getNumberOfCodeStructuresInTable(self):
        return self.numberOfCodeStructuresInTable

    def getNumberOfBitsInPrefix(self):
        return self.numberOfBitsInPrefix

    def getNumberOfBitsInEntireCode(self):
        return self.numberOfBitsInEntireCode

    def getTableModeSwitch(self):
        return self.tableModeSwitch

    def getBaseValueRepresentedByBaseCode(self):
        return self.baseValueRepresentedByBaseCode

    def getBaseCode(self):
        return self.baseCode


def reverse_bits(src, n):
    dst = 0
    while (n > 0):
        dst = (dst << 1) | (src & 0x1)
        src = src >> 1
        n -= 1
    return dst


def swap_supplied_huffman_table_base_codes(reversed_huffman_prefix_codes, bits_per_prefix):
    n = len(reversed_huffman_prefix_codes)
    corrected_huffman_prefix_codes = []
    for i in range(n):
        corrected_huffman_prefix_codes.append(reverse_bits(reversed_huffman_prefix_codes[i], bits_per_prefix[i]))

    return corrected_huffman_prefix_codes


class Huffman():
    def __init__(self, bytesToDecompress, differenceDataUsed, multiplier, huffmanTablesList):
        self.extract_bit_mask = [0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01]
        self.sign_detect_mask = [0x0000000000000000,
                                 0x0000000000000001, 0x0000000000000002, 0x0000000000000004, 0x0000000000000008,
                                 0x0000000000000010, 0x0000000000000020, 0x0000000000000040, 0x0000000000000080,
                                 0x0000000000000100, 0x0000000000000200, 0x0000000000000400, 0x0000000000000800,
                                 0x0000000000001000, 0x0000000000002000, 0x0000000000004000, 0x0000000000008000]

        self.signed_extend_mask = [0xfffffffffffff000,
                                   0xffffffffffffffff, 0xfffffffffffffffe, 0xfffffffffffffffc, 0xfffffffffffffff8,
                                   0xfffffffffffffff0, 0xffffffffffffffe0, 0xffffffffffffffc0, 0xffffffffffffff80,
                                   0xffffffffffffff00, 0xfffffffffffffe00, 0xfffffffffffffc00, 0xfffffffffffff800,
                                   0xfffffffffffff000, 0xffffffffffffe000, 0xffffffffffffc000, 0xffffffffffff8000]
        self.difference_data_used = differenceDataUsed
        self.multiplier = multiplier
        self.bytes_to_decompress = bytesToDecompress
        self.available_bytes = len(bytesToDecompress)
        self.decompressed_value_count = 0
        self.byte_index = 0
        self.bit_index = 8
        self.have_bits = 0
        self.current_bits = 0
        self.current_byte = 0
        self.last_value = 0
        self.second_last_value = 0

        #self.cast16 =  lambda x: struct.unpack('h', struct.pack('h', x & 0xffff))[0]

        self.huffman_tables_list = huffmanTablesList
        self.use_default = len(huffmanTablesList) == 0
        self.huffman_table_length = len(huffmanTablesList)
        self.load_huffman_table_in_use(1)
    # https://graphics.stanford.edu/~seander/bithacks.html#VariableSignExtend
    def cast16(self, n):
        n = n & 0xffff
        return (n ^ 0x8000) - 0x8000

    def get_enough_bits(self, wantBits):
        while self.have_bits < wantBits:
            if self.bit_index > 7:
                if self.byte_index < self.available_bytes:
                    self.current_byte = self.bytes_to_decompress[self.byte_index]
                    self.byte_index += 1
                    self.bit_index = 0
                else:
                    raise Exception("No more bits (having decompressed {} dec bytes)".format(self.byte_index))
            if (self.current_byte & self.extract_bit_mask[self.bit_index]) == 0:
                new_bit = 0
            else:
                new_bit = 1

            self.bit_index += 1
            self.current_bits = (self.current_bits << 1) + new_bit
            self.have_bits += 1

    def load_huffman_table_in_use(self, number):
        if self.use_default:
            useHuffmanTable = HuffmanTable()
        else:
            useHuffmanTable = self.huffman_tables_list[number]

        self.huffman_table_length = useHuffmanTable.getNumberOfCodeStructuresInTable()
        self.bitsPerPrefix = useHuffmanTable.getNumberOfBitsInPrefix()
        self.bitsPerEntireCode = useHuffmanTable.getNumberOfBitsInEntireCode()
        self.tableModeSwitch = useHuffmanTable.getTableModeSwitch()
        self.valuesRepresentedByCodes = useHuffmanTable.getBaseValueRepresentedByBaseCode()
        self.huffmanPrefixCodes = swap_supplied_huffman_table_base_codes(useHuffmanTable.getBaseCode(), self.bitsPerPrefix)

    def decode(self):
        value = 0
        gotValue = False
        while not gotValue:
            tableIndex = 0
            while tableIndex <= self.huffman_table_length:
                wantPrefixBits = self.bitsPerPrefix[tableIndex]

                self.get_enough_bits(wantPrefixBits)

                if self.current_bits == self.huffmanPrefixCodes[tableIndex]:
                    break

                tableIndex += 1

            if tableIndex >= self.huffman_table_length:
                raise Exception("Code prefix not in table")

            if self.tableModeSwitch[tableIndex] == 0:
                self.newTableNumber = self.valuesRepresentedByCodes[tableIndex]
                self.load_huffman_table_in_use(self.newTableNumber)
                continue
            elif self.bitsPerPrefix[tableIndex] == self.bitsPerEntireCode[tableIndex]:
                value = self.cast16(self.valuesRepresentedByCodes[tableIndex])

            else:
                numberOfOriginalBits = self.bitsPerEntireCode[tableIndex] - self.bitsPerPrefix[tableIndex]
                self.current_bits = 0
                self.have_bits = 0
                self.get_enough_bits(numberOfOriginalBits)
                if (self.current_bits & self.sign_detect_mask[numberOfOriginalBits]) != 0:
                    self.current_bits |= self.signed_extend_mask[numberOfOriginalBits]

                value = self.cast16(self.current_bits)
            if self.difference_data_used == 1:
                if self.decompressed_value_count > 0:
                    value = self.cast16(value + self.last_value)

            elif self.difference_data_used == 2:
                if self.decompressed_value_count > 1:
                    value = self.cast16(value + 2 * self.last_value - self.second_last_value)
            elif self.difference_data_used != 0:
                raise Exception("Unrecognized difference encoding method {}".format(self.difference_data_used))

            self.second_last_value = self.last_value
            self.last_value = value
            self.current_bits = 0
            self.have_bits = 0
            self.decompressed_value_count += 1
            gotValue = True

        return value * self.multiplier

    def decodeN(self, nValuesWanted):
        values = []
        for count in range(nValuesWanted):
            values.append(self.decode())
        return values


if __name__ == '__main__':
    test = [0xFF, 0x83, 0x7F, 0xE0, 0xE6, 0xF1, 0x53, 0x65, 0x59, 0xB6, 0x5B, 0x96, 0x4B, 0x96, 0x00]
    print(test)
    result_no_mult = [13, 14, 15, 14, 16, 18, 19, 20, 22, 22, 23, 23, 23, 22, 22, 20, 17, 15, 12, 8, 6, 3, 1, 0, -2, -2,
                      -3, -3]
    result_mult = [63, 70, 74, 71, 79, 89, 96, 102, 108, 112, 114, 116, 116, 112, 110, 100, 87, 74, 59, 42, 28, 13, 5,
                   -1, -8, -11, -13, -17]

    dec = Huffman(test, 2, 1, [])
    result_no_mult_1 = dec.decodeN(28)
    print([x - y for x, y in zip(result_no_mult_1, result_no_mult)])
    dec = Huffman(test, 2, 5, [])
    result_mult_1 = dec.decodeN(28)
    print([x - y for x, y in zip(result_mult_1, result_mult)])
