from pathlib import Path
import csv
from datetime import datetime
from .Huffman import Huffman
import numpy as np


class ScpReader:
    """
    Classe che fa il parsing del file scp

    ...

    Attributes
    ----------
    sec1_tag : dict
        dizionario con i dati della sezione1 dove sono contenuti i puntatori alle sezioni presenti

    # TODO completare i commenti degli attributi

    Methods
    -------
    _get_header()
        Metodo che ritorna il crc dell'intero file e la sua lunghezza complessiva

    # TODO completare i commenti dei metodi

    """
    def __init__(self, file):
        base_path = Path(__file__).parent
        sec1_tag = base_path / 'resources' / 'sec1_table.csv'
        with open(sec1_tag, 'r') as f:
            reader = csv.reader(f)
            self.sec1_tag = {}
            for row in reader:
                key, value = row
                if key == 'TAG':
                    continue
                self.sec1_tag[int(key)] = value

        leads_name = base_path / 'resources' / 'leads.csv'
        with open(leads_name, 'r') as f:
            reader = csv.reader(f)
            self.leads_name = {}
            for row in reader:
                key, value = row
                self.leads_name[int(key)] = value

        if type(file) == str:
            with open(file, "rb") as f:
                self.data = f.read()
        elif type(file) == bytes:
            self.data = file

        self.file_header = self._get_header

        self.count = 6  # header crc+file_length

        self.sections = self._section0()
        to_keep = [key for key in self.sections.keys() if self.sections[key]['length'] > 0]
        self.sections = {key: self.sections[key] for key in to_keep}

        for key in self.sections:
            if key == 1:
                self.patient_data = self._section1()
            elif key == 2:
                self.tables = self._section2()
            elif key == 3:
                self.leads, self.flags = self._section3()
            elif key == 4:
                self.qrs = self._section4()
            elif key == 5:
                self.refs_type0 = self._section5_6(len(self.leads))
            elif key == 6:
                self.ref_res = self._section5_6(len(self.leads))
            elif key == 7:
                self.measurements, self.spikes, self.ref_beats_qrs_complexes = self._section7()
            elif key == 8:
                self.statements_8, self.date_8 = self._section8()
            elif key == 9:
                self.manufactorer_data = self._section9()
            elif key == 10:
                self.to_do = self._section10()
            elif key == 11:
                self.statements_11, self.date_11 = self._section8()

    def __str__(self):
        string = ''
        for key in self.sections:
            if key == 1:
                string += 'Section {}:\n{}\n'.format(key,self.patient_data)
            elif key == 2:
                string += 'Section {}:\n{}\n'.format(key,self.tables)
            elif key == 3:
                string += 'Section {}:\n{}\n{}\n'.format(key, self.leads, self.flags)
            elif key == 4:
                string += 'Section {}:\n{}\n'.format(key,self.qrs)
            elif key == 5:
                string += 'Section {}:\n{}\n'.format(key,self.refs_type0)
            elif key == 6:
                string += 'Section {}:\n{}\n'.format(key,self.ref_res)
            elif key == 7:
                string += 'Section {}:\n{}\n{}\n{}\n'.format(key,self.measurements, self.spikes, self.ref_beats_qrs_complexes)
            elif key == 8:
                string += 'Section {}:\n{}\n'.format(key,self.statements_8, self.date_8)
            elif key == 9:
                string += 'Section {}:\n{}\n'.format(key,self.manufactorer_data)
            elif key == 10:
                string += 'Section {}:\n{}\n'.format(key,self.to_do)
            elif key == 11:
                string += 'Section {}:\n{}\n'.format(key,self.statements_11, self.date_11)
        return string
    def _get_header(self):
        # TODO completare i commenti del metodo
        header = self.data[:6]
        crc = header[:2]
        record_length = int.from_bytes(header[2:], byteorder='little')
        return crc, record_length

    def _get_section_header(self):
        # TODO completare i commenti del metodo
        sec_length = 16
        header = self.data[self.count:self.count + sec_length]
        crc = header[:2]
        sec_id = header[2:4]
        sec_length = int.from_bytes(header[4:8], byteorder='little')
        sec_version = header[9]
        prot_version = header[10]
        reserved = header[10:]
        return {"crc": crc, "sec_id": sec_id, "sec_length": sec_length, "sec_version": sec_version,
                "prot_version": prot_version, "reserved": reserved}

    def _get_section_data(self, sec_length):
        # TODO completare i commenti del metodo
        section = self.data[self.count + 16:self.count + sec_length]
        self.count = self.count + sec_length
        return section

    def _section0(self):
        # TODO completare i commenti del metodo
        # pointers to each of present sections
        header = self._get_section_header()
        assert b'SCPECG' == header["reserved"]

        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)

        sections = {}
        count = 0
        for sec in range(int(len(section_data) / 10)):
            count += 10
            pointer_field = section_data[sec * 10:sec * 10 + 10]
            sec_id = pointer_field[:2][0]
            length = int.from_bytes(pointer_field[2:6], byteorder='little')
            pointer = pointer_field[6:][0]
            section = {'length': length, 'pointer': pointer}
            sections[sec_id] = section

        return sections

    def _section1(self):
        # TODO completare i commenti del metodo
        # patient demographic and ECG administrative data
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        count = 0
        section_tag = {}
        while 1:
            tag = section_data[count]
            count += 1
            length = int.from_bytes(section_data[count:count + 2], byteorder='little')
            count += 2
            if tag == 255:
                break
            value = section_data[count:count + length]
            count += length
            if tag in self.sec1_tag:
                section_tag[self.sec1_tag[tag]] = value
            else:
                section_tag[tag] = value

        return section_tag

    def _section2(self):
        # TODO completare i commenti del metodo
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        n_huffman_tables = int.from_bytes(section_data[:2], byteorder='little')
        count = 2
        tables = []
        if n_huffman_tables != 19999:
            for table in range(n_huffman_tables):
                n_code_structures = int.from_bytes(section_data[2:4], byteorder='little')
                count += 2

                huffman = section_data[count:count + 9]
                print(huffman)
                count += 9

                prefix = huffman[0]
                n_bits = huffman[1]
                table_mode = huffman[2]

                base_value = huffman[3:5]
                base_code = huffman[5:]
                table = {'n_code_structures': n_code_structures, 'prefix': prefix, 'n_bits': n_bits,
                         'table_mode': table_mode, 'base_value': base_value, 'base_code': base_code}
                tables.append(table)
        return tables

    def _section3(self):
        # TODO completare i commenti del metodo
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        n_leads = section_data[0]
        flag = bin(section_data[1])[2:]
        flags = {'reference_beat': flag[0], 'reserved': flag[1], 'all_leads_sim': flag[2:4],
                 'n_sim': int(flag[4:], base=2)}

        count = 2
        leads = []
        for lead in range(n_leads):
            lead_details = section_data[count:count + 9]
            count += 9
            start = int.from_bytes(lead_details[:4], byteorder='little')
            end = int.from_bytes(lead_details[4:8], byteorder='little')
            lead_id = lead_details[-1]
            if lead_id in self.leads_name:
                lead_id = self.leads_name[lead_id]
            lead = {'start': start, 'end': end, 'lead_id': lead_id}
            leads.append(lead)
        return leads, flags

    def _section4(self):
        # TODO completare i commenti del metodo
        # QRS location
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        ms_ref_length = int.from_bytes(section_data[:2], byteorder='little')
        n_sample = int.from_bytes(section_data[2:4], byteorder='little')
        n_QRS = int.from_bytes(section_data[4:6], byteorder='little')
        count = 6
        all_qrs = []

        for n_qrs in range(n_QRS):
            beat_type = section_data[count:count + 2]
            count += 2
            n_subadd_start = int.from_bytes(section_data[count:count + 3], byteorder='little')
            count += 3
            res_samples_loc = int.from_bytes(section_data[count:count + 3], byteorder='little')
            count += 3
            res_samples_subadd = int.from_bytes(section_data[count:count + 4], byteorder='little')
            count += 4
            qrs = {'beat_type': beat_type, 'n_subadd_start': n_subadd_start, 'res_sample_loc': res_samples_loc,
                   'res_samples_subadd': res_samples_subadd}
            all_qrs.append(qrs)

        for n_qrs in range(n_QRS):
            start = int.from_bytes(section_data[count:count + 4], byteorder='little')
            count += 4
            end = int.from_bytes(section_data[count:count + 4], byteorder='little')
            count += 4
            all_qrs[n_qrs]['start'] = start
            all_qrs[n_qrs]['end'] = end
        return {'ms_ref_length':ms_ref_length, 'n_sample': n_sample, 'n_qrs': n_QRS, 'all_qrs': all_qrs}

    def _section5_6(self, n_leads):
        # TODO completare i commenti del metodo
        # 5 -> Ref beat type0
        # 6 -> entire ECG rythm data or residual signal after ref beats subtraction
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        avm = int.from_bytes(section_data[:2], byteorder='little')  # nanovolt
        fs = 10e5 / int.from_bytes(section_data[2:4], byteorder='little')
        encoding = section_data[4]  # 0 real, 1 first difference, 2 second difference pg.40
        reserved = section_data[5]
        count = 6
        lengths = []
        for i in range(n_leads):
            lengths.append(int.from_bytes(section_data[count:count + 2], byteorder='little'))
            count += 2

        encoded_ref = []  # codificato con hufmann di section 2
        for length in lengths:
            encoded_ref.append(section_data[count:count + length])
            count += length

        return {'avm': avm, 'fs': fs, 'reserved' : reserved, 'encoding': encoding, 'lengths' : lengths, 'encoded_ref': encoded_ref}

    def _section7(self):
        # TODO completare i commenti del metodo
        # global measurements for each beat type
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        n_ref = section_data[0]  # numero di reference o numero di qrs+1
        n_pacemaker_spikes = section_data[1]
        average_RR = int.from_bytes(section_data[2:4], byteorder='little')
        average_PP = int.from_bytes(section_data[4:6], byteorder='little')
        count = 6
        measurements = []
        for ref in range(n_ref):
            measurements.append(section_data[count:count + 16])
            count += 16

        spikes = []
        for spike in range(n_pacemaker_spikes):
            duration = int.from_bytes(section_data[count:count + 2], byteorder='little')  # milliseconds
            count += 2
            amplitude = int.from_bytes(section_data[count:count + 2], byteorder='little')  # microvolt
            count += 2
            spikes.append({'duration': duration, 'amplitude': amplitude})

        for spike in range(n_pacemaker_spikes):
            spike_type = section_data[count]
            count += 1
            source = section_data[count]
            count += 1
            qrs_index = section_data[count:count + 2]
            count += 2
            pulse_duration = section_data[count:count + 2]
            count += 2
            spikes[spike]['spike_type'] = spike_type
            spikes[spike]['source'] = source
            spikes[spike]['qrs_index'] = qrs_index
            spikes[spike]['pulse_duration'] = pulse_duration

        n_qrs_complexes = section_data[count:count + 2]
        count += 2
        ref_beats_qrs_complexes = []
        for qrs_complex in n_qrs_complexes:
            ref_beats_qrs_complexes.append(section_data[count])
            count += 1

        ventricular_rate = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        atrial_rate = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        qt_corrected = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        hr_correction = section_data[count]
        count += 1
        length = int.from_bytes(section_data[count:count + 2], byteorder='little')  # dispersion measurements
        count += length + 2

        p_onset = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        p_offset = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        qrs_onset = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        qrs_offset = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        t_offset = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        p_axis = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        qrs_axis = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        t_axis = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2

        return measurements, spikes, ref_beats_qrs_complexes

    def _section8(self):
        # TODO completare i commenti del metodo
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        count = 0
        confirmed = section_data[count]
        count += 1
        year = int.from_bytes(section_data[count:count + 2], byteorder='little')
        count += 2
        month = section_data[count]
        count += 1
        day = section_data[count]
        count += 1
        hours = section_data[count]
        count += 1
        minutes = section_data[count]
        count += 1
        seconds = section_data[count]
        count += 1

        try:
            date = datetime(year, month, day, hours, minutes, seconds)
        except Exception:
            date = None
        n_statements = section_data[count]
        count += 1

        statements = []
        pre_count = count
        try:
            for statement in range(n_statements):
                seq_number = section_data[count]
                count += 1
                length = int.from_bytes(section_data[count:count + 2], byteorder='little')
                count += 2
                statements.append(section_data[count:count + length])
                count += length
        except Exception:
            statements = [section_data[pre_count:]]

        return statements, date

    def _section9(self):
        # TODO completare i commenti del metodo
        header = self._get_section_header()
        sec_length = header['sec_length']
        section_data = self._get_section_data(sec_length)
        return section_data  # manufacturer dependant

    def _section10(self): # TODO completare la sezione 10
        return

    def decode(self):
        # TODO completare i commenti del metodo
        n_samples = []
        for lead in self.leads:
            n_samples.append(lead['end']-lead['start']+1)
        mult = self.ref_res['avm']/1000
        if '5' in self.sections:
            assert self.ref_res['fs'] == self.refs_type0['fs'] # same fs, need to be handled with decimation
        # TODO aggiungere qrs complex ref e decimation

        decoded = []
        for count, n in enumerate(n_samples):
            huffman = Huffman(self.ref_res['encoded_ref'][count], self.ref_res['encoding'], mult, self.tables)
            decoded.append(huffman.decodeN(n))
        return np.array(decoded)


if __name__ == '__main__':
    file = Path('522.scp')
    scp = ScpReader(file.read_bytes())
    scp = scp.decode()
    print(scp.shape)









