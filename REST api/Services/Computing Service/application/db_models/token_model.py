from . import db
from ..utils import verify_hash, generate_hash


class TokenModel(db.Model):
    """
        Data model used to pair username with hashed access and refresh token, needed for blacklisting functions

        ...

        Attributes
        ----------
        id : integer
            growing index
        username : str
            user unique identifier
        access : str
            hashed jti of access token
        refresh: str
            hashed jti of refresh token

        Methods
        -------
        save_to_db()
            Saves the current object to db
        find_by_username(username=None, access=None, refresh=None)
            Returns the pointer to the searched user
        return_all()
            Returns a json-formatted output of the current state
        delete_by_username(username)
            Logout user by revoking its token with db row deletion
        update(username, token=None, refresh=None)
            Updates user hashed tokens
        return_all()
            Returns a json-formatted output of the current state
        is_jti_blacklisted(user, jti, token_type)
            Checks whether the jti corresponds to current token


    """
    __tablename__ = 'tokens'
    __table_args__ = {'extend_existing': True}
    # __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    access = db.Column(db.String(255), nullable=False)
    refresh = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        """Commit a new row to db

        Raises
        ------
        ErrorName
            When registering a new user with the same username
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by(cls, username=None, access=None, refresh=None):
        """Returns the pointer to the searched user

        The parameters to be searched could be username or access or refresh and
        are checked in this order

        Parameters
        ----------
        username : str, optional
            Parameter to be searched
        access : str, optional
            Parameter to be searched
        refresh : str, optional
            Parameter to be searched

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        if username:
            return cls.query.filter_by(username=username).first()
        elif access:
            return cls.query.filter_by(access=access).first()
        elif refresh:
            return cls.query.filter_by(refresh=refresh).first()

    @classmethod
    def delete_by_username(cls, username):
        """Deletes selected entry of data-model

        Parameters
        ----------
        username : str
            Parameter to be deleted
        TODO try except

        """
        user = cls.query.filter_by(username=username).first()
        db.session.delete(user)
        db.session.commit()

    @classmethod
    def update(cls, username, token=None, refresh=None):
        """Updates user hashed tokens

        Parameters
        ----------
        username : str
            Row identifier to be updated
        token : str, optional
            Parameter to be updated
        refresh : str, optional
            Parameter to be updated

        """
        user = cls.query.filter_by(username=username).first()
        if token:
            user.access = token
        if refresh:
            user.refresh = refresh
        db.session.commit()

    @classmethod
    def return_all(cls):
        """Returns a json-formatted output of the current state

        Returns
        -------
        dict
            with tokens key which contains a list of username-access-refresh dictionaries
        """
        def to_json(x):
            return {
                'user': x.username,
                'access': x.access,
                'refresh': x.refresh
            }

        return {'tokens': list(map(lambda x: to_json(x), TokenModel.query.all()))}

    @classmethod
    def is_jti_blacklisted(cls, user, jti, token_type):
        """Returns a json-formatted output of the current state

        Parameters
        ----------
        user : str
            Row identifier to be checked
        jti : str
            Received jti
        token_type : str
            Jti type, could be access or refresh

        Returns
        -------
        bool
            indicating whether the jti is revoked, true, or not, false
        """
        query_user = cls.query.filter_by(username=user).first()
        if query_user:
            if token_type == 'access':
                current_jti = query_user.access
            elif token_type == 'refresh':
                current_jti = query_user.refresh
        else:
            return True
        # error will be throw if the hash was set to 0 -> revoking procedure
        try:
            verified = verify_hash(jti, current_jti)
        except ValueError:
            verified = False
        return not verified

