import os

def get_env_variable(name):
    """Utility for the extraction of environmental variables

    Parameters
    ----------
    name : str
        name of the ENV variable

    Returns
    -------
    str
        value of the variable

    Raise
    -------
    Exception
        When the ENV variable is not found
    """
    try:
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)