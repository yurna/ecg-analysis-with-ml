from flask_restful import reqparse

parser_scp = reqparse.RequestParser()
parser_scp.add_argument('id', help='This field cannot be blank', required=True)

parser_key = reqparse.RequestParser()
parser_key.add_argument('public_key', help='This field cannot be blank', required=True)

