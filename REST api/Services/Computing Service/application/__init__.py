from .app import app, api, Config, initialize, jwt, cache, SERVICE_PORT
from .resources import *
from .db_models import db, url, reset_db
from .computation import init_net
from .registration import register
from sqlalchemy_utils import create_database, database_exists


def create_app():
    app.config.from_object(Config)
    db.init_app(app)
    cache.init_app(app)

    with app.app_context():
        # creation of db tables, if not already there
        if not database_exists(db.engine.url):
            create_database(db.engine.url)
        db.create_all()

    fresh_start = False
    if fresh_start:
        reset_db(url)

    register()
    init_net()

    @jwt.token_in_blacklist_loader
    def check_if_token_in_blacklist(decrypted_token):
        token_type = decrypted_token['type']
        jti = decrypted_token['jti']

        # cached
        JTI_access, JTI_refresh = initialize(app)

        if token_type == 'access':
            return jti != JTI_access
        if token_type == 'refresh':
            return jti != JTI_refresh

    api.add_resource(Compute, '/compute')
    api.add_resource(Refresh, '/refresh')

    api.add_resource(Health, '/health')

    return app
