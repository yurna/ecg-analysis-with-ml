from flask_restful import Resource
from flask_jwt_extended import jwt_required

from ..utils import parser_scp
from ..db_models import BlobModel
from ..ScpReader import ScpReader
from ..computation import get_anomalies



class Compute(Resource):
    """
        Resource class available only through JWT, it computes the anomaly interval from a given measure_id

        ...
        Models
        -------
        BlobModel -> update interval

        Methods
        -------
        post()
            The method parse the header looking for the token and then update the received
            measure with the computed anomaly interval
    """

    @jwt_required
    def post(self):
        """It handles the computation behind the anomaly detection algorithm

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                interval -> to be defined
            }
        """
        data = parser_scp.parse_args()
        measure_id = data['id']
        measure = BlobModel.find_by_identifier(measure_id)
        scp = ScpReader(measure.scp)
        scp = scp.decode()
        a = get_anomalies(scp)

        BlobModel.update_interval(measure_id, a)

        return a
