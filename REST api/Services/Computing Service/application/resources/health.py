from flask_restful import Resource


class Health(Resource):
    """
        Resource class not protected that is used to check the server status

        ...
        Methods
        -------
        get()
            method returns 204 if server is up
    """

    def get(self):
        """Used to check server status

        Returns
        -------
        json
            status code : 204
        """

        return {}, 204
