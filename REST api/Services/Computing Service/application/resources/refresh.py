from flask_restful import Resource
from ..db_models import DiscoveryRegistryModel
from flask import request
import socket

from ..utils import parser_key
from ..app import app, initialize, cache
import base64


class Refresh(Resource):
    """
        Resource class available only through refresh JWT, it refresh the previous tokens and encrypt them again with a
        new public key

        ...
        Models
        -------
        ServiceTokenModel -> update access_token:refresh_token

        Methods
        -------
        post()
            The method parse the header looking for the token and then decode the public key with which encodes a new
            pair of access and refresh token
    """

    def post(self):
        """It handles the generation of new tokens which will be encoded with the new public key

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                "message" : "ok"
            }
        """

        remote_address = request.remote_addr
        services = DiscoveryRegistryModel.return_all()['services']
        passed = False
        for service in services:
            # gethostbyname used within docker network
            if socket.gethostbyname(service['address']) == remote_address:
                passed = True
                break

        if passed:
            data = parser_key.parse_args()
            public_key_pem = data['public_key']
            public_key_pem = base64.b64decode(public_key_pem)

            cache.set('public_key', public_key_pem)

            cache.delete_memoized(initialize, app)
            _, _ = initialize(app)

            return {'message': 'ok'}

        else:
            return 'Unauthorized access', 401
