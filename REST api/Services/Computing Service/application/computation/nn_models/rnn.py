# -*- coding: utf-8 -*-
import torch.nn as nn
import torch


class RNN_all_lstm(torch.nn.Module):
    """
        Neural network class that extends torch.nn.Module and implement a fully LSTM network

        Methods
        -------
        froward()
            Overrides the super class methods and represent the prediction pass of the network callable with <Module>(tensor)
    """

    def __init__(self, input_size, hidden_units, layers_num, output_size, dropout_prob=0):
        """Neural network architecture

        Layers
        -------
        input
            tensor [n_examples x seq_len x n_leads]
        first lstm
            output [n_examples x seq_len x hidde_units]
        second lstm
            output [n_examples x seq_len x n_leads]
        """
        # Call the parent init function (required!)
        super().__init__()
        # Define recurrent layer
        self.rnn1 = torch.nn.LSTM(input_size=input_size,
                                 hidden_size=hidden_units,
                                 num_layers=layers_num - 1,
                                 dropout=dropout_prob,
                                 batch_first=True,
                                 bidirectional=False)

        self.rnn2 = torch.nn.LSTM(input_size=hidden_units,
                                 hidden_size=output_size,
                                 num_layers=1,
                                 dropout=0,
                                 batch_first=True,
                                 bidirectional=False)

    def forward(self, x, state=None):
        """Forward pass of neural network

        Params
        -------
        x: Tensor
            Input tensor
        state: Tensor
            Initial LSTM state
        """
        # LSTM
        self.rnn1.flatten_parameters()
        x, rnn_state = self.rnn1(x, state)
        self.rnn2.flatten_parameters()
        # need (1,5,8) hidden
        x, rnn_state = self.rnn2(x, state)
        return x, rnn_state