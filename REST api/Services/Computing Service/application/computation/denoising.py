import numpy as np
import pywt
from scipy.signal import butter, filtfilt, resample


def butter_bandpass(lowcut, highcut, fs, order=5):
    """Creates the a pass-band butterworth filter with the given prerequisites

    Parameters
    ----------
    lowcut : integer
        lowcut of the pass-band
    highcut : integer
        highcut of the pass-band
    fs : integer
        sampling frequency of the signal
    order : int, optional
        filter order

    Returns
    -------
    b : integer
        numerator polynomial of the IIR filter
    a : integer
        denominator polynomial of the IIR filter

    """
    nyq = 0.5 * fs
    low = lowcut / nyq
    high = highcut / nyq
    b, a = butter(order, [low, high], btype='band')
    return b, a


def butter_bandpass_filter(data, lowcut, highcut, fs, order=5):
    """Creates the a pass-band butterworth filter and filter the given signal

    Parameters
    ----------
    data : numpy.array
        array with the signal
    lowcut : integer
        lowcut of the pass-band
    highcut : integer
        highcut of the pass-band
    fs : integer
        sampling frequency of the signal
    order : int, optional
        filter order

    Returns
    -------
    y : numpy.array
        represent the filtered data

    """
    b, a = butter_bandpass(lowcut, highcut, fs, order=order)
    y = filtfilt(b, a, data)
    return y


# thresholding
def get_th(coeff, raw, scale=0.5):
    """rig-sure thresholding

    Parameters
    ----------
    coeff : numpy.array
        array with the coefficient to threshold
    raw : numpy.array
        raw signal for std computation
    scale : integer, optional
        thresholding scaling

    Returns
    -------
    th : integer
        threshold value

    """
    # squared
    squared = coeff ** 2
    # sort from little to big
    squared.sort()
    # sum value
    s = np.sum(squared)
    risk = []
    # risk computation
    for count, value in enumerate(squared):
        r = len(squared) - 2 * (count + 1) + (len(squared) - (count + 1)) * value + s
        r /= len(squared)
        risk.append(r)
    # standard deviation
    std = np.std(raw)
    # select the coefficient with less risk
    selected_coeff = np.sqrt(squared[np.argmin(risk)])
    # threshold
    th = scale * std * selected_coeff
    return th


def heursure(x):
    """heur-sure thresholding

    Parameters
    ----------
    x : numpy.array
        array with the coefficient to threshold

    Returns
    -------
    th : integer
        threshold value

    """
    l = len(x)
    hth = np.sqrt(2 * np.log(l))

    # get the norm of x
    normsqr = np.dot(x, x)
    eta = 1.0 * (normsqr - l) / l
    crit = (np.math.log(l, 2) ** 1.5) / np.sqrt(l)

    if eta < crit:
        th = hth
    else:
        sx2 = x ** 2
        sx2.sort()
        cumsumsx2 = np.sum(sx2)
        risks = []
        for i in np.xrange(0, l):
            risks.append((l - 2 * (i + 1) + (cumsumsx2[i] + (l - 1 - i) * sx2[i])) / l)
        mini = np.argmin(risks)

        rth = np.sqrt(sx2[mini])
        th = min(hth, rth)

    return th


def denoise_single_signal(raw, wavelet_type='sym6', level=6, scale=1, range_freq=[1, 60], fs=500, heur=False,
                          discarded=2):
    """Denoising procedure using both passband filter and wavelet thresholding

    Parameters
    ----------
    raw : array
        signal to denoise
    wavelet_type : str, optional
        wavelet type used for decomposition
    level : integer, optional
        number of wavelets
    scale : integer, optional
        threshold scaling
    range_freq : list, optional
        pass_band filtering
    fs : integer, optional
        sampling frequency
    heur : bool, optional
        heuristic thresholding usage
    discarded : integer, optional
        number of wavelets to discard

    Returns
    -------
    numpy.array
        representing the filtered signal

    """
    # passband filtering
    filtered = butter_bandpass_filter(raw, range_freq[0], range_freq[1], fs, order=6)

    # wavelet decomposition
    w = pywt.Wavelet(wavelet_type)
    coeffs = pywt.wavedec(filtered, w, level=level)

    th_coeffs = []
    # wavelet coefficients thresholding
    for count, coeff in enumerate(coeffs):
        if scale > 0:
            if heur:
                th = heursure(coeff)
            else:
                th = get_th(coeff, raw, scale=scale)
            thresholded = pywt.threshold_firm(coeff, th, 2 * th)
        else:
            thresholded = coeff
        th_coeffs.append(thresholded)

    # wavelet coefficient recomposing
    if discarded > 0:
        th_coeffs = th_coeffs[:-discarded] + discarded * [None]
    denoised = pywt.waverec(th_coeffs, w)

    return np.array(denoised, dtype='float32')


def denoise(record):
    """Denoising procedure using both passband filter and wavelet thresholding for a multivariate signal

    Parameters
    ----------
    raw : array
        multivariate signal to denoise

    Returns
    -------
    numpy.array
        representing the filtered multivariate signal

    """
    # 8 * 5000
    fs = 500
    fs_target = 250
    to_samples = int(record.shape[1] / fs * fs_target)
    record = resample(record.T, to_samples).T
    denoised = []
    scale = 0.0
    for column in record:
        data = denoise_single_signal(column, wavelet_type='sym5', level=5, discarded=0, scale=scale, range_freq=[1, 50],
                                     fs=250)
        denoised.append(data)
    return np.array(denoised).T
