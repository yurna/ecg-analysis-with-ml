from . import denoise
from .nn_models import RNN_all_lstm
from pathlib import Path
import numpy as np
import torch
import os
from . import get_th

def rolling_window(signal, window):
    """Windowing utility, some numpy magic

    Parameters
    ----------
    signal : numpy.array
        signal from which sample windows
    window : integer
        window length

    Returns
    -------
    numpy.array
        all the windows

    """
    shape = signal.shape[:-1] + (signal.shape[-1] - window + 1, window)
    strides = signal.strides + (signal.strides[-1],)
    return np.lib.stride_tricks.as_strided(signal, shape=shape, strides=strides)


def load_net():
    """Net loader function

    Returns
    -------
    torch.nn.Module
        the loaded network with initialized weights

    """
    # read all weights
    dir_path = os.path.dirname(os.path.realpath(__file__))
    weights = list((Path(dir_path) / 'weights').glob('*.weights'))

    in_out = 8
    middle_layer_size = 64
    n_layers = 4
    lookback = 5

    # name format
    name = 'lstm_in{}_mid{}x{}_lb{}'.format(in_out, middle_layer_size, n_layers, lookback)

    # initialize net
    net = RNN_all_lstm(in_out, middle_layer_size, n_layers, in_out, dropout_prob=0.3)
    # needed because the net was trained in a multi-gpu environment
    net = torch.nn.DataParallel(net, dim=1)

    # weights loading
    found = np.array([file.stem == name for file in weights])
    net.load_state_dict(torch.load(weights[found.argmax()], map_location=torch.device('cpu')))
    return net, lookback

def init_net():
    """Net initialization to speedup the first request, it loads the network and makes a mock prediction

   """
    net, lookback = load_net()
    net.eval()
    mock_input = torch.rand((1, 2500, 8))
    with torch.no_grad():
        net(mock_input)

def obtain_anomaly_measure(predicted, real, window=60, scale=2):
    """Utility for the generation of a vector containing the anomaly spikes

    Parameters
    ----------
    predicted : numpy.array
        signal generated from the network
    real : numpy.array
        real signal
    window : integer
        window used for the error smoothing
    scale : integer
        scaling of the dynamic threshold


    Returns
    -------
    numpy.array
        With the anomaly scores

    """
    # mean square error
    diff_mse = ((predicted - real) ** 2).mean(axis=1)
    # normalization
    diff_mse /= np.max(diff_mse)
    # anomaly smoothing
    sums = [np.trapz(window) for window in rolling_window(diff_mse, window)]
    # padding
    results = np.pad(sums, int(window / 2), 'constant', constant_values=(0,))
    # hard thresholding
    results[results < get_th(results, results, scale)] = 0

    return results


def get_anomalies(signal):
    """Function that gather all procedures for anomaly extraction

    Parameters
    ----------
    signal : numpy.array
        signal to process


    Returns
    -------
    dict
        Containing punctual anomaly position
    """
    # signal 8x5000
    # denoising
    denoised = denoise(signal)
    # normalization
    denoised /= denoised.max()

    # net loading
    net, lookback = load_net()

    # real input and output
    denoised = torch.FloatTensor(denoised)
    net_input = denoised[:-lookback].unsqueeze(0)
    net_output = denoised[lookback:].numpy()

    # forward pass
    net.eval()
    with torch.no_grad():
        output = net(net_input)[0].squeeze().cpu().numpy()

    # anomaly scoring
    results = obtain_anomaly_measure(output, net_output)

    # extract anomalies position
    points_of_anomaly = np.where(results)[0]

    return {'anomalies': points_of_anomaly.tolist()}
