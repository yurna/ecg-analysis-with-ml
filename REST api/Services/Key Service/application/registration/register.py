from ..app import SERVICE_NAME, SERVICE_PORT, UUID, app
from ..db_models import DiscoveryRegistryModel

from ..utils import get_env_variable


def register():
    """Register a service to the service registry


    """
    # register the uuid, todo needed?
    token = UUID
    # test on server to get real ip and port
    ip = get_env_variable('ADDRESS')
    port = SERVICE_PORT

    with app.app_context():
        # register the service or update the present with the same name
        findings = DiscoveryRegistryModel.find_by_service(service = SERVICE_NAME)

        if findings:
            DiscoveryRegistryModel.update_service(address=ip, port=port, token=token, service=SERVICE_NAME)
        else:
            entry = DiscoveryRegistryModel(address=ip, port=port, token=token, service=SERVICE_NAME)
            entry.save_to_db()

