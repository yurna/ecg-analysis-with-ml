from flask_restful import Resource
from ..db_models import DiscoveryRegistryModel
from flask import request
import socket

from ..app import app
import base64



class GetKey(Resource):
    """
        Resource class available only through JWT, it give the current secret_key

        ...
        Methods
        -------
        get()
            The method return the current
    """

    def get(self):
        """It handles the computation behind the anomaly detection algorithm

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                interval -> to be defined
            }
        """

        remote_address = request.remote_addr
        services = DiscoveryRegistryModel.return_all()['services']
        passed = False
        for service in services:
            # gethostbyname used within docker network
            if socket.gethostbyname(service['address']) == remote_address:
                passed = True
                break
        if passed:
            with app.app_context():
                message = base64.b64encode(app.config['JWT_SECRET_KEY']).decode()
                return {'message': message}
        else:
            return {'message': 'permission denied'}
