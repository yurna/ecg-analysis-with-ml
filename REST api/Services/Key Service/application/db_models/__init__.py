from flask_sqlalchemy import SQLAlchemy
from ..utils import get_env_variable

MYSQL_URL = get_env_variable("MYSQL_URL")
MYSQL_USER = get_env_variable("MYSQL_USER")
MYSQL_PASSWORD = get_env_variable("MYSQL_PASSWORD")
MYSQL_DATABASE = get_env_variable("MYSQL_DATABASE")
url = 'mysql://{0}:{1}@{2}/{3}'.format(MYSQL_USER, MYSQL_PASSWORD, MYSQL_URL, MYSQL_DATABASE)

class Config_db(object):
    SQLALCHEMY_DATABASE_URI = url
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_ECHO = True


db = SQLAlchemy()


from .blob_model import BlobModel
from .service_token_model import ServiceTokenModel
from .token_model import TokenModel
from .user_model import UserModel
from .discovery_registry_model import DiscoveryRegistryModel
from .db_utils import reset_db