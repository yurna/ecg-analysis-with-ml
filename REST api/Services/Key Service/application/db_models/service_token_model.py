from . import db


class ServiceTokenModel(db.Model):
    """
        Data model used to save SCP blobs and their measurements

        ...
        Attributes
        ----------
        id : integer
            growing index
        service : str
            microservice identification e.g. computation
        token : byte
            JWT access token with RSA encryption
        TODO add also server address?

        Methods
        -------
        save_to_db()
            Saves the current object to db
        update_token(service, token)
            Updates service token
        find_by_service(service)
            Returns the pointer to the searched service token
    """
    __tablename__ = 'service_tokens'
    __table_args__ = {'extend_existing': True}
    # __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    service = db.Column(db.String(120), unique=True, nullable=False)
    token = db.Column(db.BLOB, nullable=False)
    refresh = db.Column(db.BLOB, nullable=False)

    def save_to_db(self):
        """Commit a new row to db

        Raises
        ------
        ErrorName
            When registering a new token with the same identifier
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def update_token(cls, service, token, refresh):
        """Updates service token

        Parameters
        ----------
        service : str
            Microservice identification e.g. computation
        token : byte
            JWT access token with RSA encryption
        """
        row = cls.query.filter_by(service=service).first()
        row.token = token
        row.refresh = refresh
        db.session.commit()

    @classmethod
    def find_by_service(cls, service):
        """Returns the pointer to the searched token

        Parameters
        ----------
        service : str
            Microservice identification e.g. computation

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        return cls.query.filter_by(service=service).first()
