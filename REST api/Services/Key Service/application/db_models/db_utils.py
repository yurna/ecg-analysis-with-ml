from sqlalchemy_utils import create_database, database_exists, drop_database
def reset_db(url):
    """Utility for reset the database

    Parameters
    ----------
    url : str
        database address
    """
    if database_exists(url):
        drop_database(url)
    create_database(url)