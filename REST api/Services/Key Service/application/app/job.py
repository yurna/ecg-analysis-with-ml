from . import app
import os


def job():
    with app.app_context():
        app.config['JWT_SECRET_KEY'] = os.urandom(32)
