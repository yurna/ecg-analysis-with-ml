from application import create_app, SERVICE_PORT

application = create_app()

if __name__ == "__main__":
    application.run(host='0.0.0.0', port=SERVICE_PORT, debug=True, ssl_context='adhoc')
