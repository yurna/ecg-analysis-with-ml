from .app import app, api, Config, SERVICE_PORT
from .resources import *
from .db_models import db
from .app import cache
from .registration import register, initialize
from sqlalchemy_utils import create_database, database_exists


def create_app():
    app.config.from_object(Config)
    db.init_app(app)
    cache.init_app(app)

    with app.app_context():
        # creation of db tables, if not already there
        if not database_exists(db.engine.url):
            create_database(db.engine.url)
        db.create_all()

    register()
    initialize()

    api.add_resource(UserRegistration, '/registration')
    api.add_resource(UserLogin, '/login')
    api.add_resource(UserLogout, '/logout')
    api.add_resource(TokenRefresh, '/token/refresh')

    api.add_resource(SecretResource, '/secret')
    api.add_resource(ScpReceiver, '/scp')

    api.add_resource(RenewKey, '/renew')

    api.add_resource(Health, '/health')

    api.add_resource(GetKey, '/key')

    return app
