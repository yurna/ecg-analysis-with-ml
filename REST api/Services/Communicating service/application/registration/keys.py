from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.asymmetric import rsa
from ..app import cache, UUID

from ..db_models import DiscoveryRegistryModel, ServiceTokenModel
import base64
import requests
from ..db_models import db


@cache.memoize()
def create_keys(psw):
    """Utility for key creation

    Parameters
    ----------
    psw : str
        password for the new private key


    Returns
    -------
    private_key_pem : RSAPrivateKey
        serialized private key
    public_key_pem : RSAPublicKey
        serialized public key

    """
    # key couple creation
    private_key = rsa.generate_private_key(
        public_exponent=65537,
        key_size=3072,
        backend=default_backend()
    )
    public_key = private_key.public_key()

    # serialization of private key
    private_key_pem = private_key.private_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PrivateFormat.PKCS8,
        encryption_algorithm=serialization.BestAvailableEncryption(psw.encode())
    )

    # serialization of public key
    public_key_pem = public_key.public_bytes(
        encoding=serialization.Encoding.PEM,
        format=serialization.PublicFormat.SubjectPublicKeyInfo
    )

    return private_key_pem, public_key_pem


def decrypt(private_key, message):
    """Decrypt a given message with the private key

    Params
    -------
    private_key : RSAPrivateKey
        Pubic key used for encryption
    message : bytearray
        encrypted message


    Returns
    -------
    bytearray
        decoded string
    """
    return private_key.decrypt(
        message,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )


def service_pairing(service_name, token_type='access'):
    """Utility for service authentication

    Parameters
    ----------
    service_name : str
        service name to which authenticate
    token_type : str, optional
        type of the token to extract


    Returns
    -------
    str
        JWt string of the requested token

    """

    # Query in the service registry and in the token registry for the needed service
    result = DiscoveryRegistryModel.find_by_service(service_name)
    tokens = ServiceTokenModel.find_by_service(service_name)

    # key generation/extraction
    private_key, public_key = create_keys(psw=UUID)
    private_key = serialization.load_pem_private_key(
        private_key,
        password=UUID.encode(),
        backend=default_backend()
    )

    # if registered and previously authenticated
    if result and tokens:
        try:
            # trying the decryption
            if token_type == 'access':
                decrypted_token = decrypt(private_key, tokens.token)
            elif token_type == 'refresh':
                decrypted_token = decrypt(private_key, tokens.refresh)
            return decrypted_token.decode()
        except ValueError:
            # if fails that means the public key with which the token was encrypted is different from the current
            # public key
            pass

    # if the service only registered or registered and authenticated but previous 'if' fails
    if result:
        # encrypt public key and send to the resource
        public_key_b64 = base64.b64encode(public_key).decode()
        data = {'public_key': public_key_b64}
        url = 'https://{}:{}/refresh'.format(result.address, result.port)
        try:
            # send to the refresh endpoint on needed service
            requests.post(url, json=data, verify=False)
            # update db infos
            db.session.commit()

            # query for the new encrypted tokens
            tokens = ServiceTokenModel.find_by_service(service_name)
            if token_type == 'access':
                decrypted_token = decrypt(private_key, tokens.token)
            elif token_type == 'refresh':
                decrypted_token = decrypt(private_key, tokens.refresh)
            return decrypted_token.decode()
        except requests.exceptions.ConnectionError:
            pass

    # None is returned when the service is not available
    return None
