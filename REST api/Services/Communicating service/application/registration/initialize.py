from .keys import create_keys
from ..app import cache, UUID


def initialize():
    """Delete the cached values of the serialized pair of keys and re-initialize two new keys

    """
    # save in cache
    cache.delete_memoized(create_keys)
    _, _ = create_keys(psw=UUID)
