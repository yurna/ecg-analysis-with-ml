from flask_restful import Resource
from flask_jwt_extended import create_access_token, create_refresh_token, jwt_refresh_token_required, get_jwt_identity, get_jti


from ..utils import verify_hash, generate_hash
from ..db_models import TokenModel

def new_valid_token(token_type, old_token, current_user):
    """Updates service token

    Parameters
    ----------
    token_type : str
        Accept only 'access' or 'refresh', it determines the type of the token generated
    old_token : str
        Encrypted JTI of the previous token assigned to the user
    current_user: str
        name of the user to which the token will be created

    Returns
    ----------
    str
        hashed jti of the current token
    str
        generated JWT

    """
    tokens = {'access': create_access_token, 'refresh': create_refresh_token}
    token_fn = tokens[token_type]
    # new access and refresh not already blacklisted
    token = token_fn(identity=current_user)
    if old_token != '0':
        while verify_hash(get_jti(token), old_token):
            token = token_fn(identity=current_user)
    hashed_token = generate_hash(get_jti(token))
    return hashed_token, token


class TokenRefresh(Resource):
    """
        Resource class available only through JWT, it refresh the user token blacklisting the previous without caring of
        their validity

        ...
        Models
        -------
        TokenModel -> username:hashed_access_jti:hashed_refresh_token

        Methods
        -------
        get()
            The method parse the header looking for the token and reset the access and refresh with new ones in the
            database
    """

    @jwt_refresh_token_required
    def get(self):
        """It handles the log-out procedure for a given user, it replace the valid tokens with 0 in the Database Model

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                'access_token': <access_token>,
                'refresh_token': <refresh_token>
            }
        """
        current_user = get_jwt_identity()

        # blacklist old tokens
        old_tokens = TokenModel.find_by(username=current_user)
        old_hashed_token = old_tokens.access
        old_hashed_refresh = old_tokens.refresh

        # new access and refresh not already blacklisted
        hashed_token, access_token = new_valid_token('access', old_hashed_token, current_user)
        hashed_refresh, refresh_token = new_valid_token('refresh', old_hashed_refresh, current_user)

        TokenModel.update(current_user, access=hashed_token, refresh=hashed_refresh)

        return {
            'access_token': access_token,
            'refresh_token': refresh_token
        }

