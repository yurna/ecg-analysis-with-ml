from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity

from ..db_models import TokenModel


class UserLogout(Resource):
    """
        Resource class available only through JWT, it removes the access to the current user

        ...
        Models
        -------
        TokenModel -> username:hashed_access_jti:hashed_refresh_token

        Methods
        -------
        get()
            The method parse the header looking for the token and reset to 0 the access and refresh in the database
    """

    @jwt_required
    def get(self):
        """It handles the log-out procedure for a given user, it replace the valid tokens with 0 in the Database Model

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                "message": "<Name of the user logged out">,
            }
        """
        current_user = get_jwt_identity()
        TokenModel.update(current_user, access="0", refresh="0")
        return {'message': '{} logged_out'.format(current_user)}
