from flask_restful import Resource
from flask_jwt_extended import jwt_required, get_jwt_identity
import hashlib

import base64
import requests
from sqlalchemy.exc import IntegrityError
from ast import literal_eval

from ..registration import service_pairing

from ..utils import parser_scp, parser_measure
from ..db_models import BlobModel, db, DiscoveryRegistryModel
from ..app import app



class ScpReceiver(Resource):
    """
        Resource class available only through JWT, it handles the scp uploading

        ...
        Models
        -------
        BlobModel -> identifier:current_user:scp:empty_interval

        Methods
        -------
        post()
            The method handles the upload of the scp file in base64, first it decodes the received payload and save it with an
            hashed index in the BlobModel
        get()
            The method parse the header looking for the token and get the interval from the database using the md5 hash
            as key
    """

    @jwt_required
    def post(self):
        """It handles the upload of the scp file in base64, first it decodes the received payload and save it with an
         hashed index in the BlobModel

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                "message": "Ok",
                "MD5": <identifier>
            }
        """
        current_user = get_jwt_identity()
        data = parser_scp.parse_args()

        encoded_scp = data['payload']
        encoded_scp = base64.b64decode(encoded_scp)

        identifier = hashlib.md5(encoded_scp).hexdigest()
        app.logger.info('scp MD5{}'.format(identifier))
        try:
            blob = BlobModel(identifier=identifier, user=current_user, scp=encoded_scp, interval={})
            blob.save_to_db()
            json = {'message': 'New file'}
        except IntegrityError:
            db.session().rollback()
            json =  {'message': 'File already uploaded'}

        token = service_pairing('computation')
        if token:
            address = DiscoveryRegistryModel.find_by_service(service = 'computation')
            url = 'https://{}:{}/compute'.format(address.address, address.port)

            head = {'Authorization': 'Bearer ' + token}
            data = {'id': identifier, 'user': current_user}

            response = requests.post(url, json=data, headers=head, verify=False)
            response_parsed = literal_eval(response.text)

            json['MD5'] = identifier
            json['intervals'] = response_parsed
        else:
            json['error'] = 'Computation server unreachable'

        return json

    @jwt_required
    def get(self):
        """It handles the upload of the scp file in base64, first it decodes the received payload and save it with an
                 hashed index in the BlobModel

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                interval -> to be defined
            }
        """
        data = parser_measure.parse_args()
        measure_id = data['md5']
        measure = BlobModel.find_by_identifier(measure_id)
        if measure:
            return measure.interval
        else:
            return {'message': "No measurement found"}

