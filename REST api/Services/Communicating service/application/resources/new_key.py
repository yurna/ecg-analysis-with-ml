from flask_jwt_extended import jwt_required, get_jwt_identity

import requests

from ..registration import service_pairing

from flask_restful import Resource
from ..db_models import DiscoveryRegistryModel

class GetKey(Resource):
    """
        Resource class available only through JWT, it handles the scp uploading

        ...
        Models
        -------
        BlobModel -> identifier:current_user:scp:empty_interval

        Methods
        -------
        post()
            The method handles the upload of the scp file in base64, first it decodes the received payload and save it with an
            hashed index in the BlobModel
        get()
            The method parse the header looking for the token and get the interval from the database using the md5 hash
            as key
    """

    @jwt_required
    def get(self):
        """It handles the upload of the scp file in base64, first it decodes the received payload and save it with an
         hashed index in the BlobModel

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                "message": "Ok",
                "MD5": <identifier>
            }
        """
        json = {}

        token = service_pairing('key')
        if token:
            address = DiscoveryRegistryModel.find_by_service(service='key')
            url = 'https://{}:{}/key'.format(address.address, address.port)

            head = {'Authorization': 'Bearer ' + token}

            response = requests.get(url, headers=head, verify=False)

            json['message'] = response.text
        else:
            json['error'] = 'Computation server unreachable'

        return json
