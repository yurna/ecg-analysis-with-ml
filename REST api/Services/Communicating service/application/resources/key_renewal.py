from flask_restful import Resource
from flask_jwt_extended import jwt_required

from ..registration import initialize


class RenewKey(Resource):
    """
        Resource class available only through refresh JWT, it updates both the tokens and the keys

        Methods
        -------
        get()
            The method parse the header looking for the token and send the newly generated public key
    """

    @jwt_required
    def get(self):
        """It handles the generation of a new pair of keys and the upload of public key, base64 encoded, to the service

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                "message" : "ok"
            }
        """
        initialize()

        return {'message': 'ok'}
