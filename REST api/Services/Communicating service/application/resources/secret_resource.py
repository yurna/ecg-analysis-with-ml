from flask_restful import Resource
from flask_jwt_extended import jwt_required


class SecretResource(Resource):
    """
        Resource class available only through JWT and only in test mode, it returns the meaning of life

        ...
        Methods
        -------
        get()
            The method parse the header looking for the token and makes magic
    """

    @jwt_required
    def get(self):
        """Only for testing purposes

        Auth Type
        -------
        Bearer Token
            header = Authorization: Bearer <JWT>

        Returns
        -------
        json
            {
                'answer': <meaning_of_life>,
            }
        """
        return {
            'answer': 42
        }
