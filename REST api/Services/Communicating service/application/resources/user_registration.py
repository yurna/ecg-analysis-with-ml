from flask_restful import Resource

from ..utils import parser_login, generate_hash
from ..db_models import UserModel


class UserRegistration(Resource):
    """
        Resource class available only for test purpose that provide an endpoint for user registration

        ...
        Models
        -------
        UserModel -> pairs username:password

        Methods
        -------
        post()
            The method parse the json looking for useraname and password fields
    """

    def post(self):
        """Parse the sent json-formatted body to get username:password pair and post it to UserModel,
            if the username is already there it returns the error

        Json format
        ----------
            {
                "username" : "<username>",
                "password" : "<password>"
            }

        Returns
        -------
        json
            {
                "message": "<Name of the user created">,
            }
        """
        data = parser_login.parse_args()

        if UserModel.find_by_username(data['username']):
            return {'message': 'User {} already exists'.format(data['username'])}
        else:
            new_user = UserModel(
                username=data['username'],
                password=generate_hash(data['password'])
            )
            new_user.save_to_db()

            return {'message': 'User {} was created'.format(data['username'])}
