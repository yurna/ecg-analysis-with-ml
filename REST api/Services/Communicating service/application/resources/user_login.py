from flask_restful import Resource
from flask_jwt_extended import (create_access_token, create_refresh_token, get_jti)
from flask_httpauth import HTTPBasicAuth

from ..utils import verify_hash, generate_hash
from ..db_models import UserModel, TokenModel

auth = HTTPBasicAuth()


@auth.verify_password
def verify_password(username, password):
    """Returns a boolean value representing the password match

    Parameters
    ----------
    username : str
        Name of the user attempting the log-in
    password : str
        Password inserted by the user

    Returns
    -------
    bool
        representing the match between the inserted password and the one stored in the database
    """
    # first try to authenticate by token
    user = UserModel.find_by_username(username)
    if not user:
        return False
    return verify_hash(password, user.password)


def new_valid_token(token_type, old_token, current_user):
    """Updates service token

    Parameters
    ----------
    token_type : str
        Accept only 'access' or 'refresh', it determines the type of the token generated
    old_token : str
        Encrypted JTI of the previous token assigned to the user
    current_user: str
        name of the user to which the token will be created

    Returns
    ----------
    str
        hashed jti of the current token
    str
        generated JWT

    """
    tokens = {'access': create_access_token, 'refresh': create_refresh_token}
    token_fn = tokens[token_type]
    # new access and refresh not already blacklisted
    token = token_fn(identity=current_user)
    if old_token != '0':
        while verify_hash(get_jti(token), old_token):
            token = token_fn(identity=current_user)
    hashed_token = generate_hash(get_jti(token))
    return hashed_token, token


class UserLogin(Resource):
    """
        Resource class available only through username and password, it generates the tokens

        ...
        Models
        -------
        TokenModel -> username:hashed_access_jti:hashed_refresh_token

        Methods
        -------
        get()
            The method parse the header looking for username and password fields
    """

    @auth.login_required
    def get(self):
        """It handles the token request from a registered user

        Auth Type
        -------
        Basic Auth
            header = Authorization: Basic <base64("username:password")>

        Returns
        -------
        json
            {
                "message": "<Name of the user logged in">,
                "access_token": <access_token>,
                "refresh_token": <refresh_token>
            }
        """
        current_user = auth.username()

        # blacklist old tokens
        old_tokens = TokenModel.find_by(username=current_user)
        if old_tokens:
            old_hashed_token = old_tokens.access
            old_hashed_refresh = old_tokens.refresh
            # new access and refresh not already blacklisted
            hashed_token, access_token = new_valid_token('access', old_hashed_token, current_user)
            hashed_refresh, refresh_token = new_valid_token('refresh', old_hashed_refresh, current_user)
            TokenModel.update(current_user, access=hashed_token, refresh=hashed_refresh)
        else:
            # new access and refresh not already blacklisted
            hashed_token, access_token = new_valid_token('access', '0', current_user)
            hashed_refresh, refresh_token = new_valid_token('refresh', '0', current_user)
            entry = TokenModel(username = current_user, access=hashed_token, refresh=hashed_refresh)
            entry.save_to_db()



        return {
            'message': 'Logged in as {}'.format(auth.username()),
            'access_token': access_token,
            'refresh_token': refresh_token
        }
