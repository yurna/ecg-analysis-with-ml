from flask_restful import reqparse

parser_login = reqparse.RequestParser()
parser_login.add_argument('username', help='This field cannot be blank', required=True)
parser_login.add_argument('password', help='This field cannot be blank', required=True)

parser_scp = reqparse.RequestParser()
parser_scp.add_argument('payload', help='This field cannot be blank', required=True)

parser_measure = reqparse.RequestParser()
parser_measure.add_argument('md5', help='This field cannot be blank', required=True, location='args')
