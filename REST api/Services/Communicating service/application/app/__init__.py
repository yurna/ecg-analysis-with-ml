from .app import app, api, Config
from .service_param import SERVICE_NAME, SERVICE_PORT, UUID
from .cache import cache