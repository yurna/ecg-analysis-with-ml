from . import app
import os
from ..registration import service_pairing


def job():
    token = service_pairing('key')
    with app.app_context():
        app.config['JWT_SECRET_KEY'] = os.urandom(32)
