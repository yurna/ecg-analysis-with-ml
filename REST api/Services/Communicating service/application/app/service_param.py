from ..utils import get_env_variable
import uuid

# service name in db table
SERVICE_NAME = get_env_variable('SERVICE')
# service port
SERVICE_PORT = get_env_variable('PORT')
# service
UUID = str(uuid.uuid5(uuid.NAMESPACE_DNS, SERVICE_NAME))