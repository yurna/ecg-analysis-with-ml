from . import db
from sqlalchemy.dialects import mysql


class BlobModel(db.Model):
    """
        Data model used to save SCP blobs and their measurements

        ...
        Attributes
        ----------
        id : integer
            growing index
        identifier : integer
            measure unique identifier e.g. MD5
        user : str
            client name e.g. HETZNER
        scp : byte
            scp file
        interval : dict
            json-formatted interval
            TODO decidere formattazione

        Methods
        -------
        save_to_db()
            Saves the current object to db
        find_by_identifier(ids):
            Returns the pointer to the searched measure id
        update_interval(ids, interval)
            Updates the anomaly interval
    """
    __tablename__ = 'blobs'
    __table_args__ = {'extend_existing': True}
    # __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    identifier = db.Column(db.String(255), unique=True, nullable=False)
    user = db.Column(db.String(120), nullable=False)
    scp = db.Column(mysql.MEDIUMBLOB, nullable=False)
    interval = db.Column(db.JSON, nullable=False)

    def save_to_db(self):
        """Commit a new row to db

        Raises
        ------
        ErrorName
            When registering a new measure with the same identifier
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_identifier(cls, ids):
        """Returns the pointer to the searched measure

        Parameters
        ----------
        ids : integer
            Measure id to be searched

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        return cls.query.filter_by(identifier=ids).first()

    @classmethod
    def update_interval(cls, ids, interval):
        """Updates measure anomaly intervals

        Parameters
        ----------
        ids : integer
            Measure identifier to be updated
        interval : dict
            Parameter to be updated
        """
        measure = cls.query.filter_by(identifier=ids).first()
        measure.interval = interval
        db.session.commit()


