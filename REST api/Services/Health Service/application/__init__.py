from .app import app, Config, SERVICE_PORT
from .db_models import db
from .app import job
from .registration import register
from apscheduler.schedulers.background import BackgroundScheduler
from sqlalchemy_utils import database_exists, create_database

def create_app():
    app.config.from_object(Config)
    db.init_app(app)

    with app.app_context():
        # creation of db tables, if not already there
        if not database_exists(db.engine.url):
            create_database(db.engine.url)
        db.create_all()

    register()

    scheduler = BackgroundScheduler()
    scheduled_job = scheduler.add_job(job, 'interval', seconds = 60)
    scheduler.start()
    return app