from . import db


class UserModel(db.Model):
    """
        Data model used to pair username with hashed password

        ...

        Attributes
        ----------
        id : integer
            growing index
        username : str
            user unique identifier
        password : str
            hashed password

        Methods
        -------
        save_to_db()
            Saves the current object to db
        find_by_username(username)
            Returns the pointer to the searched user
        return_all()
            Returns a json-formatted output of the current state
        delete_all()
            Deletes all entries of data-model
    """
    # table name
    __tablename__ = 'users'
    __table_args__ = {'extend_existing': True}
    # __abstract__ = True
    # columns names
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)

    def save_to_db(self):
        """Commit a new row to db

        Raises
        ------
        ErrorName
            When registering a new user with the same username
        """
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_username(cls, username):
        """Returns the pointer to the searched user

        Parameters
        ----------
        username : str
            Parameter to be searched

        Returns
        -------
        Query
            first result of the Query or None if the result doesn’t contain any row
        """
        return cls.query.filter_by(username=username).first()

    @classmethod
    def return_all(cls):
        """Returns a json-formatted output of the current state

        Returns
        -------
        dict
            with users key which contains a list of username-password dictionaries
        """
        def to_json(x):
            return {
                'username': x.username,
                'password': x.password
            }

        return {'users': list(map(lambda x: to_json(x), UserModel.query.all()))}

    @classmethod
    def delete_all(cls):
        """Deletes all entries of data-model

        Returns
        -------
        dict
            with number of rows deleted, if completed, or with standard message otherwise
        """
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
