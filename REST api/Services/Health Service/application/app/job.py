from ..db_models import DiscoveryRegistryModel
from . import app
from .service_param import SERVICE_NAME
import requests


def job():
    with app.app_context():
        rows = DiscoveryRegistryModel.return_all()
    service_not_available = []
    for row in rows['services']:
        if row['service'] != SERVICE_NAME:
            url = 'https://{}:{}/health'.format(row['address'], row['port'])
            try:
                response = requests.get(url, verify=False)
                if response.status_code != 204:
                    service_not_available.append(row['service'])
            except requests.exceptions.ConnectionError:
                service_not_available.append(row['service'])

    for service in service_not_available:
        with app.app_context():
            DiscoveryRegistryModel.delete_by_service(service)
