from passlib.hash import pbkdf2_sha256 as sha256


def generate_hash(password):
    """Utility for the generation of the pbkdf2 hash

    Parameters
    ----------
    password : str
        string to hash


    Returns
    -------
    str
        hash of the input

    """
    return sha256.hash(password)


def verify_hash(password, hash):
    """Utility for the verification of the pbkdf2 hash

    Parameters
    ----------
    password : str
        string to control
    hash : str
        stored hash

    Returns
    -------
    bool
        result of the verification

    """
    return sha256.verify(password, hash)